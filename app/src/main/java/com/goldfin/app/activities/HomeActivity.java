package com.goldfin.app.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.widget.RelativeLayout;

import com.goldfin.R;
import com.goldfin.app.fragments.TutorialFragment;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;


import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.managers.SharedPreferenceManager;
import com.goldfin.app.models.User.UserModel;


public class HomeActivity extends BaseActivity {


    RelativeLayout contParentActivityLayout;
    private UserModel userModel;
    private SharedPreferenceManager sharedPreferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contParentActivityLayout = findViewById(R.id.contParentActivityLayout);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        userModel = sharedPreferenceManager.getCurrentUser();

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        UserModel userModel = SharedPreferenceManager.getInstance(this).getObject(AppConstants.KEY_CURRENT_USER_MODEL, UserModel.class);
        initFragments(/*UserTypeEnum.fromCanonicalForm(userModel.getCssausermodel().getRole())*/);
    }

    public RelativeLayout getContParentActivityLayout() {
        return contParentActivityLayout;
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected int getTitlebarLayoutId() {
        return R.id.titlebar;
    }


    @Override
    protected int getDockableFragmentId() {
        return R.id.contMain;
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    // FIXME: 26/07/2018 Use Enum UserType instead of String
    private void initFragments() {
            addDockableFragment(TutorialFragment.newInstance(), false);

    }


    @Override
    public void onBackPressed() {
        if (isInSelectingState) {
            if (genericClickableInterface != null) {
                genericClickableInterface.click();
                isInSelectingState = false;
            } else {
                UIHelper.showToast(getBaseContext(), "No call back selected.");
            }
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            closeApp();
        }
    }


}