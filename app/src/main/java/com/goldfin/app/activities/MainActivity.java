
package com.goldfin.app.activities;


import android.os.Bundle;
import androidx.annotation.Nullable;

import com.goldfin.R;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

//        RunTimePermissions.verifyStoragePermissions(this);
        initFragments();
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getTitlebarLayoutId() {
        return R.id.titlebar;
    }



    @Override
    protected int getDockableFragmentId() {
        return R.id.contMain;
    }


    private void initFragments() {
    }


    @Override
    public void onBackPressed() {

//         * Show Close app popup if no or single fragment is in stack. otherwise check if drawer is open. Close it..
//         */

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            closeApp();
        }

    }
}