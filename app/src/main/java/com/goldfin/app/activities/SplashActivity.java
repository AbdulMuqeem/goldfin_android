package com.goldfin.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.managers.SharedPreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends Activity {
    private final int ANIMATIONS_DELAY = 1000;

    private final int SPLASH_TIME_OUT = 2000;
    SharedPreferenceManager sharedPreferenceManager;
    boolean isUserLoggedIn = false;
    @BindView(R.id.splash_iv_logo)
    ImageView splashIvLogo;
    @BindView(R.id.splash_iv_desc)
    TextView splashIvDesc;
//    @BindView(R.id.contParentLayout)
//    LinearLayout contParentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashprev);
        ButterKnife.bind(this);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        isUserLoggedIn = sharedPreferenceManager.getCurrentUser() != null;

        // fixing portrait mode problem for SDK 26 if using windowIsTranslucent = true
        if (Build.VERSION.SDK_INT == 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animateLayout(SPLASH_TIME_OUT);
            }
        }, ANIMATIONS_DELAY);


    }

    private void animateLayout(int SPLASH_TIME_OUT) {
        Intent i;
        i = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(i);
        // close this activity
        finish();
    }


}
