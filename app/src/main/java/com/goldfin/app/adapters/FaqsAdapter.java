package com.goldfin.app.adapters;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.goldfin.R;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.models.ContactsModel;
import com.goldfin.app.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FaqsAdapter extends RecyclerView.Adapter<FaqsAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;


    private ArrayList<ContactsModel> filteredData = new ArrayList<>();
    private Activity activity;
    private ArrayList<ContactsModel> arrayList;

    public FaqsAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_faq, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ContactsModel ContactsModel = arrayList.get(holder.getAdapterPosition());
        holder.question.setText(ContactsModel.getQuestions()); ////????
        holder.txtAnswer.setText(ContactsModel.getAnswer()); ////????

        holder.txtAnswer.setVisibility(View.GONE);
        holder.question.setTextColor(activity.getResources().getColor(R.color.txt_color));
        holder.contQuest.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        setListener(holder, ContactsModel);

    }

    private void setListener(ViewHolder holder, ContactsModel model) {


        holder.btnPlusMinus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.txtAnswer.setVisibility(View.VISIBLE);
//                            holder.question.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
                    holder.contQuest.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
                    holder.question.setTextColor(activity.getResources().getColor(R.color.c_white));
                } else {
                    holder.txtAnswer.setVisibility(View.GONE);
                    holder.question.setTextColor(activity.getResources().getColor(R.color.txt_color));
//                            holder.question.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
                    holder.contQuest.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.question)
        AnyTextView question;
        @BindView(R.id.btnPlusMinus)
        ToggleButton btnPlusMinus;
        @BindView(R.id.txtAnswer)
        AnyTextView txtAnswer;
        @BindView(R.id.contQuest)
        LinearLayout contQuest;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
