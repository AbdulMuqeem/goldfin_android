package com.goldfin.app.adapters;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.goldfin.R;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.models.ContactsModel;
import com.goldfin.app.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;


    private ArrayList<ContactsModel> filteredData = new ArrayList<>();
    private Activity activity;
    private ArrayList<ContactsModel> arrayList;

    public PhoneAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_contact, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ContactsModel ContactsModel = arrayList.get(holder.getAdapterPosition());
        holder.txtName.setText(ContactsModel.getName() + ""); ////????
        holder.txtNumber.setText(ContactsModel.getPhoneNumber() + ""); ////????
        setListener(holder, ContactsModel);

    }

    private void setListener(ViewHolder holder, ContactsModel model) {
        holder.contCont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), model);
                onItemClick.onItemClick(holder.getAdapterPosition(), model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtName)
        AnyTextView txtName;
        @BindView(R.id.txtNumber)
        AnyTextView txtNumber;
        @BindView(R.id.contCont)
        LinearLayout contCont;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
