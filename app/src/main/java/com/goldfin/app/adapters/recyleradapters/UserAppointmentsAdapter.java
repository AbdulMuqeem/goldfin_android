package com.goldfin.app.adapters.recyleradapters;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.goldfin.R;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.widget.AnyTextView;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserAppointmentsAdapter extends RecyclerView.Adapter<UserAppointmentsAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;



    private ArrayList<UserAppointmentsModel> filteredData = new ArrayList<>();
    private Activity activity;
    private ArrayList<UserAppointmentsModel> arrayList;

    public UserAppointmentsAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_userdetail, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final UserAppointmentsModel userAppointmentsModel = arrayList.get(holder.getAdapterPosition());
        holder.txtDTTM.setText(userAppointmentsModel.getCreatedAt()); ////????
        holder.txtAmount.setText(/*userAppointmentsModel.getLoanAmountDesired()*/""); ////????
        holder.txtCNIC.setText(userAppointmentsModel.getCnic()); ////????
        holder.txtName.setText(userAppointmentsModel.getUserName()); ////????
        holder.txtStatus.setText(userAppointmentsModel.getLoanStatus()); ////????

//        holder.txtName.setTextColor(activity.getResources().getColor(R.color.txt_color));
//        holder.txtStatus.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        setListener(holder, userAppointmentsModel);

    }

    private void setListener(ViewHolder holder, UserAppointmentsModel model) {

        holder.btnViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtDTTM)
        AnyTextView txtDTTM;
        @BindView(R.id.txtStatus)
        AnyTextView txtStatus;
        @BindView(R.id.txtName)
        AnyTextView txtName;
        @BindView(R.id.txtCNIC)
        AnyTextView txtCNIC;
        @BindView(R.id.txtAmount)
        AnyTextView txtAmount;
        @BindView(R.id.btnViewDetails)
        RoundKornerLinearLayout btnViewDetails;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
