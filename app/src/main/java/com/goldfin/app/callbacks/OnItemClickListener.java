package com.goldfin.app.callbacks;

public interface OnItemClickListener {
    void onItemClick(int position, Object object);

}
