package com.goldfin.app.callbacks;

/**
 * Created by muhammadmuzammil on 4/24/2017.
 */

public interface OnItemSelectListner
{
    void onItemSelect(Object data);
}
