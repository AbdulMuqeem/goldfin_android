package com.goldfin.app.callbacks;

public interface OnNewPacketReceivedListener
{
    void onNewPacket(int event, Object data);
}
