package com.goldfin.app.callbacks;

public interface RatingBarDataInterface {

    void getRatings(float ratingValue1, float ratingValue2);
}
