package com.goldfin.app.constatnts;

import android.os.Environment;

import com.goldfin.BaseApplication;


public class AppConstants {

    public static final String INPUT_DATE_FORMAT = "yyyy-dd-MM hh:mm:ss";
    public static final String INPUT_DATE_FORMAT_AM_PM = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_DATE_FORMAT = "EEEE dd,yyyy";
    public static final String INPUT_TIME_FORMAT = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_TIME_FORMAT = "hh:mm a";
    public static String DEVICE_OS_ANDROID = "ANDROID";
    public static final String ROOT_MEDIA_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/" + BaseApplication.getApplicationName() + "/Media";

    /*******************Preferences KEYS******************/
    public static final String USER_NOTIFICATION_DATA = "USER_NOTIFICATION_DATA";

    public static String PROFILE_REGISTRATION = "profile_registration";
    public static String IS_FIRST_TIME = "launch_first_time";
    public static String FORCED_RESTART = "forced_restart";

    //    API SERVICES
    public static final String KEY_ONE_TIME_TOKEN = "";
    public static final String CNIC_MASK = "99999-9999999-9";
    public static final String MOBILE_MASK = "+99-999-9999999";


    /*******************Preferences KEYS******************/
    public static final String KEY_CURRENT_USER_MODEL = "KEY_CURRENT_USER_MODEL";
    public static final String KEY_CUSTOMER_DATA = "KEY_CUSTOMER_DATA";
    public static final String KEY_FORM_STATUS_MODEL = "keyformstatus";


    public static final String ACCESS_LOG_DATE_SHOW = "dd-MM-yyyy";
    public static final String ACCESS_LOG_DATE_SEND = "dd-MM-yyyy";


    public static final String ATTACHMENT_CNIC_FRONT = "CNIC-FRONT-S";
    public static final String ATTACHMENT_CNIC_BACK = "CNIC-BACK-S";
    public static final String ATTACHMENT_UTILITY_BILL = "UTILITY_BILL";

    public static final String ATTACHMENT_PICTURE_1 = "ATTACHMENT_PICTURE_1";
    public static final String ATTACHMENT_PICTURE_2 = "ATTACHMENT_PICTURE_2";

    public static final String ATTACHMENT_CURRENT_SLIP = "salaryslip";
    public static final String ATTACHMENT_JOB_CERT = "ATTACHMENT_JOB_CERT";
    public static final String ATTACHMENT_EXP_LET = "ATTACHMENT_EXP_LET";


    public static final String ATTACHMENT_PROO_OF_BUSINESS = "proof_business";

    public static final String ATTACHMENT_RESI_BUSI_BILL = "residenceBusinessBills";
    public static final String ATTACHMENT_PROOF_BUSI_INCOME = "proofOfBusinessIncome";
    public static final String ATTACHMENT_SECP = "SECP";
    public static final String ATTACHMENT_NTN = "NTN";
}
