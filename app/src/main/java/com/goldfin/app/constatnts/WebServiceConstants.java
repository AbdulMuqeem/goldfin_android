package com.goldfin.app.constatnts;


public class WebServiceConstants {


    public static String BASE_URL = "http://134.209.144.186:3000/";


    public static final String PARAMS_REQUEST_METHOD = "RequestMethod";
    public static final String WS_KEY_GET_REQUESTOR = "";
    public static final String WS_KEY_GET_TOKEN = "";
    public static final String WS_KEY_AUTHENTICATE_USER = "";
    public static final String PARAMS_REQUEST_DATA = "RequestData";


    public static final String METHOD_LOGIN = "auth/login";
    public static final String METHOD_SIGNUP = "auth/signup";

    public static final String METHOD_GET_OTP = "auth/forget-password"; //OTP
    public static final String METHOD_ENTER_OTP_CODE = "auth/verifyAccount2"; // enter OTP
    public static final String METHOD_ENTER_SIGNUP_OTP_CODE = "auth/verifyAccount"; // enter OTP
    public static final String METHOD_SET_NEW_PASSWORD = "auth/setPassword";
    public static final String METHOD_CMO_ADD_USER = "auth/signupCMO";

    public static final String METHOD_RUPEE = "rates/goldratemoney/";
    public static final String METHOD_GOLDTOLAGRAM = "rates/goldrateqty/";
    public static final String METHOD_VISITPLAN = "planVisit/addNewVisit/";
    public static final String METHOD_PERSONALINFO = "salariedLaf/lafForm1/";
    public static final String METHOD_PROESSIONALINFO = "salariedLaf/lafForm2/";
    public static final String METHOD_FANANCIALINFO = "salariedLaf/lafForm3/";
    public static final String METHOD_FORM_STATUS = "salariedLaf/getUserForms/";

    public static final String METHOD_BUSINESS_PERSONALINFO = "businessLaf/lafForm1/";
    public static final String METHOD_BUSINESS_PROESSIONALINFO = "businessLaf/lafForm2/";
    public static final String METHOD_BUSINESS_FANANCIALINFO = "businessLaf/lafForm3/";

    public static final String METHOD_ADDTOINVITE = "recommend/addToInvite/";
    public static final String METHOD_LOAN_STATUS = "user/getLoanStatus/";

    public static final String METHOD_ADD_DOCUMENTS = "salariedLaf/addDocuments/";
    public static final String METHOD_ADD_DOCUMENTS_BUSINESS = "businessLaf/addDocuments/";
    public static final String METHOD_CMO = "cmo/cafForm/";
    public static final String METHOD_VOFORM = "vo/voForm/";
    public static final String METHOD_LOANDETAILSUMMARY = "loanSummary/addSummary/";
    public static final String METHOD_GET_CNIC_CHECK = "/user/cnicCheck/";


    public static final String METHOD_GET_FILLED_DATA_CNIC = "user/getFilledForm/";
    public static final String METHOD_GET_FORM_BO = "user/getFormsBO/";
    public static final String METHOD_GET_FORM_VO = "user/getFormsVO/";
    public static final String METHOD_GET_FORM_CMO_CSO = "user/getUsersForms/";
    public static final String METHOD_GET_CMO_FORM = "cmo/getCMO/";
    public static final String METHOD_GET_DOCS = "user/getDocuments/";
    public static final String METHOD_GET_VO_FORM = "vo/getVO/";
    public static final String METHOD_GET_FILLED_ID = "user/getFilledFormbyId/";
    public static final String METHOD_GET_FORWARD_BO = "user/assignBO/";
    public static final String METHOD_GET_FORWARD_VO = "user/assignVO/";
    public static final String METHOD_GET_LOAN_STATUS = "user/loanStatus/";
    public static final String METHOD_GET_LAF1 = "user/getLAF1/";
    public static final String METHOD_GET_LAF2 = "user/getLAF2/";
    public static final String METHOD_GET_LAF3 = "user/getLAF3/";
    public static final String METHOD_GET_TC = "term/";


}
