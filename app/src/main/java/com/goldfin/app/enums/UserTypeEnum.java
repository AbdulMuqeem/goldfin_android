package com.goldfin.app.enums;

public enum UserTypeEnum {
    CUSTOMER,
    CMO,
    VO,
    CSO,
    BO;


    public String canonicalForm() {
        return this.name().toLowerCase();
    }

    public static UserTypeEnum fromCanonicalForm(String canonical) {
        return (UserTypeEnum) valueOf(UserTypeEnum.class, canonical.toUpperCase());
    }
}