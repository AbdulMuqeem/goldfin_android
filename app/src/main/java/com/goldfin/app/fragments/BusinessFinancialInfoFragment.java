package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BusinessFinancialInfoFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtMonthlyTurnOver)
    AnyEditTextView edtMonthlyTurnOver;
    @BindView(R.id.edtMonthlyExpense)
    AnyEditTextView edtMonthlyExpense;
    @BindView(R.id.edtOtherIncomeMonthly)
    AnyEditTextView edtOtherIncomeMonthly;
    @BindView(R.id.edtSourceIncom)
    AnyEditTextView edtSourceIncom;
    @BindView(R.id.edtSourceOLoan)
    AnyEditTextView edtSourceOLoan;
    @BindView(R.id.edtLoanAmount)
    AnyEditTextView edtLoanAmount;
    @BindView(R.id.edtHouseRent)
    AnyEditTextView edtHouseRent;
    @BindView(R.id.edtExpEducation)
    AnyEditTextView edtExpEducation;
    @BindView(R.id.edtExpComitteeSavings)
    AnyEditTextView edtExpComitteeSavings;
    @BindView(R.id.edtExpMisc)
    AnyEditTextView edtExpMisc;
    @BindView(R.id.btn_next)
    Button btnNext;

    @BindView(R.id.spAvgMonthlyHousIncome)
    Spinner spAvgMonthlyHousIncome;
    @BindView(R.id.edtHouseHold)
    AnyEditTextView edtHouseHold;
    @BindView(R.id.edtUtility)
    AnyEditTextView edtUtility;
    @BindView(R.id.edtLoaninstallment)
    AnyEditTextView edtLoaninstallment;
    @BindView(R.id.edtExistingLoan)
    AnyEditTextView edtExistingLoan;
    @BindView(R.id.edtMedical)
    AnyEditTextView edtMedical;
    @BindView(R.id.checkbox1)
    CheckBox checkbox1;
    @BindView(R.id.checkbox2)
    CheckBox checkbox2;
    @BindView(R.id.txtTotal)
    AnyTextView txtTotal;
    @BindView(R.id.spExistingLoan)
    Spinner spExistingLoan;
    @BindView(R.id.contExLoanAmount)
    RelativeLayout contExLoanAmount;
    private int result, expHouseHold, expRent, expBills, expEduc, expLoan, expSavings, expMedical, expMisc;
    private UserTypeEnum userTypeEnum;
    private boolean isFromSearch;
    private UserAppointmentsModel userAppointmentsModel;
    private String monthyAccount, spexLoanYesNo;

    //
    public static BusinessFinancialInfoFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        BusinessFinancialInfoFragment fragment = new BusinessFinancialInfoFragment();
        fragment.setArguments(args);
        fragment.isFromSearch = isFromSearch;
        fragment.userAppointmentsModel = userAppointmentsModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_financial_info_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        googlePlaceHelper = new GooglePlaceHelper(getBaseActivity(), 0, this, ProfessionalInfoFragment.this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFromSearch) {
//            btnNext.setText("Back");
            serviceCallGetData();
        } else {
            if (getCurrentUser() != null && getCurrentUser().getFinancialInformation().equalsIgnoreCase("true")) {
                serviceCallGetData();
            }
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

        spExistingLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spexLoanYesNo = (String) adapterView.getSelectedItem();
                if (spexLoanYesNo.equalsIgnoreCase("Yes")) {
                    contExLoanAmount.setVisibility(View.VISIBLE);

                } else contExLoanAmount.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkbox2.setChecked(false);
                    monthyAccount = "yes";
                }
            }
        });
        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkbox1.setChecked(false);
                    monthyAccount = "no";
                }
            }
        });


        edtHouseHold.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtHouseHold != null && !edtHouseRent.getStringTrimmed().equalsIgnoreCase("")) {
                    expHouseHold = Integer.parseInt(edtHouseRent.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtExpComitteeSavings.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpComitteeSavings != null && !edtExpComitteeSavings.getStringTrimmed().equalsIgnoreCase("")) {
                    expSavings = Integer.parseInt(edtExpComitteeSavings.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });


        edtExpMisc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpMisc != null && !edtExpMisc.getStringTrimmed().equalsIgnoreCase("")) {
                    expMisc = Integer.parseInt(edtExpMisc.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtMedical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtMedical != null && !edtMedical.getStringTrimmed().equalsIgnoreCase("")) {
                    expMedical = Integer.parseInt(edtMedical.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtLoaninstallment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtLoaninstallment != null && !edtLoaninstallment.getStringTrimmed().equalsIgnoreCase("")) {
                    expLoan = Integer.parseInt(edtLoaninstallment.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });


        edtExpEducation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpEducation != null && !edtExpEducation.getStringTrimmed().equalsIgnoreCase("")) {
                    expEduc = Integer.parseInt(edtExpEducation.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtUtility.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtUtility != null && !edtUtility.getStringTrimmed().equalsIgnoreCase("")) {
                    expBills = Integer.parseInt(edtUtility.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });
        edtHouseRent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtHouseRent != null && !edtHouseRent.getStringTrimmed().equalsIgnoreCase("")) {
                    expRent = Integer.parseInt(edtHouseRent.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        FormSendingDataModel formSendingDataModel = new FormSendingDataModel();
        formSendingDataModel.setId(getCurrentUser().getId());
        formSendingDataModel.setAvgmonthturnover(edtMonthlyTurnOver.getStringTrimmed());
        formSendingDataModel.setAveragemonthbusinessexp(edtMonthlyExpense.getStringTrimmed());
        formSendingDataModel.setMonthlyincome(edtOtherIncomeMonthly.getStringTrimmed());
        formSendingDataModel.setOtherincome(edtSourceIncom.getStringTrimmed());
        formSendingDataModel.setMonthlyaccounts(monthyAccount);
        formSendingDataModel.setOutstandingloan(spexLoanYesNo);
        formSendingDataModel.setOutstandingloanamount(edtExistingLoan.getStringTrimmed());
        formSendingDataModel.setLoanAmountDesired(edtLoanAmount.getStringTrimmed());
        formSendingDataModel.setSourceofloan(edtSourceOLoan.getStringTrimmed());


        formSendingDataModel.setTotalexp(txtTotal.getStringTrimmed());
        formSendingDataModel.setHousehold1(edtHouseHold.getStringTrimmed());
        formSendingDataModel.setHouserent1(edtHouseRent.getStringTrimmed());
        formSendingDataModel.setUtilitybills1(edtUtility.getStringTrimmed());
        formSendingDataModel.setChildeducation1(edtExpEducation.getStringTrimmed());
        formSendingDataModel.setMedical1(edtMedical.getStringTrimmed());
        formSendingDataModel.setCommittee1(edtExpComitteeSavings.getStringTrimmed());
        formSendingDataModel.setLoaninstallment1(edtLoaninstallment.getStringTrimmed());
        formSendingDataModel.setOther3(edtExpMisc.getStringTrimmed());


        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectForm(formSendingDataModel, WebServiceConstants.METHOD_BUSINESS_FANANCIALINFO,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserTypeEnum userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
                                switch (userTypeEnum) {

                                    case CUSTOMER:
                                    case BO:
                                        getBaseActivity().addDockableFragment(GeneralInfoFragment.newInstance(true), false);
                                        break;
                                    case CMO:
                                        getBaseActivity().addDockableFragment(CollateralInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                        break;
                                    case VO:
                                        getBaseActivity().addDockableFragment(VerificationFormFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                        break;
                                    case CSO:
                                        getBaseActivity().addDockableFragment(DocumentUploadingFragment.newInstance(false, true, null), false);
                                        break;


                                }
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    private void bindData(FormSendingDataModel customerData) {
//        21323-4567890-7

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
                enableInputs();
                break;
            case CSO:
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false")) {
                    enableInputs();
                } else {
                    if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Deprecated"))
                        enableInputs();
                    else
                        disableInputs();
                }
                break;
        }

        if (customerData != null) {
            if (customerData.getLoanAmountDesired() != null)
                edtLoanAmount.setText(customerData.getLoanAmountDesired());

            if (customerData.getSourceofloan2() != null)
                edtSourceOLoan.setText(customerData.getSourceofloan2());

            if (customerData.getAveragemonthbusinessexp() != null)
                edtMonthlyExpense.setText(customerData.getAveragemonthbusinessexp());

            if (customerData.getAvgmonthturnover() != null)
                edtMonthlyTurnOver.setText(customerData.getAvgmonthturnover());

            if (customerData.getMonthlyincome() != null)
                edtOtherIncomeMonthly.setText(customerData.getMonthlyincome());

            if (customerData.getSourceofloan() != null)
                edtSourceOLoan.setText(customerData.getSourceofloan());

            if (customerData.getOtherincome() != null)
                edtSourceIncom.setText(customerData.getOtherincome());

            if (customerData.getTotalexp() != null)
                txtTotal.setText(customerData.getTotalexp());

            if (customerData.getHousehold1() != null)
                edtHouseHold.setText(customerData.getHousehold1());

            if (customerData.getHouserent1() != null)
                edtHouseRent.setText(customerData.getHouserent1());

            if (customerData.getUtilitybills1() != null)
                edtUtility.setText(customerData.getUtilitybills1());

            if (customerData.getChildeducation1() != null)
                edtExpEducation.setText(customerData.getChildeducation1());

            if (customerData.getMedical1() != null)
                edtMedical.setText(customerData.getMedical1());

            if (customerData.getCommittee1() != null)
                edtExpComitteeSavings.setText(customerData.getCommittee1());

            if (customerData.getLoaninstallment1() != null)
                edtLoaninstallment.setText(customerData.getLoaninstallment1());

            if (customerData.getOutstandingloan() != null) {
                spexLoanYesNo = (customerData.getOutstandingloan());
                spExistingLoan.setSelection(((ArrayAdapter) spExistingLoan.getAdapter()).getPosition(spexLoanYesNo));

            }
            if (spexLoanYesNo.equalsIgnoreCase("yes")) {
                edtExistingLoan.setVisibility(View.VISIBLE);
            }
            if (customerData.getOther3() != null)
                edtExpMisc.setText(customerData.getOther3());
            if (customerData.getOutstandingloanamount() != null)
                edtExistingLoan.setText(customerData.getOutstandingloanamount());


        }
    }

    private void enableInputs() {
        edtExpComitteeSavings.setEnabled(true);
        edtMonthlyTurnOver.setEnabled(true);
        edtExpMisc.setEnabled(true);
        edtHouseHold.setEnabled(true);
        edtHouseRent.setEnabled(true);
        edtLoanAmount.setEnabled(true);
        edtLoaninstallment.setEnabled(true);
        edtMedical.setEnabled(true);
        edtExpEducation.setEnabled(true);
        edtSourceOLoan.setEnabled(true);
        edtUtility.setEnabled(true);
        edtOtherIncomeMonthly.setEnabled(true);
        edtMonthlyExpense.setEnabled(true);
        edtSourceIncom.setEnabled(true);
        edtExistingLoan.setEnabled(true);
    }

    private void disableInputs() {
        edtMonthlyTurnOver.setEnabled(false);
        edtExpComitteeSavings.setEnabled(false);
        edtHouseHold.setEnabled(false);
        edtHouseRent.setEnabled(false);
        edtLoanAmount.setEnabled(false);
        edtLoaninstallment.setEnabled(false);
        edtMedical.setEnabled(false);
        edtExpMisc.setEnabled(false);
        edtExpEducation.setEnabled(false);
        edtSourceOLoan.setEnabled(false);
        edtUtility.setEnabled(false);
        edtOtherIncomeMonthly.setEnabled(false);
        edtMonthlyExpense.setEnabled(false);
        edtSourceIncom.setEnabled(false);
        edtExistingLoan.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (sharedPreferenceManager.getCustomerData().getUserId() != null)
                id = sharedPreferenceManager.getCustomerData().getUserId();
            else id = sharedPreferenceManager.getCustomerData().getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_LAF3 + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    @OnClick({R.id.txtTotal, R.id.btn_next, R.id.btnCalculate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtTotal:

                result = expHouseHold + expRent + expBills + expEduc + expLoan + expSavings + expMedical + expMisc;
                txtTotal.setText(result + "");
                break;
            case R.id.btnCalculate:
                getBaseActivity().addDockableFragment(CalculatorFragment.newInstance(true), false);

                break;
            case R.id.btn_next:
                if (isFromSearch) {
                    serviceCall();
//                    popBackStack();
//                    popBackStack();
//                    popBackStack();
                } else {
//                    if (edtMonthlyTurnOver.testValidity() &&
//                            edtMonthlyExpense.testValidity() &&
//                            edtOtherIncomeMonthly.testValidity() &&
//                            edtExistingLoan.testValidity() &&
//                            edtSourceIncom.testValidity() &&
//                            checkbox1.isChecked() || checkbox1.isChecked() &&
//                            edtSourceOLoan.testValidity() &&
//                            edtLoanAmount.testValidity() &&
//                            edtHouseHold.testValidity() &&
//                            edtHouseRent.testValidity() &&
//                            edtUtility.testValidity() &&
//                            edtExpEducation.testValidity() &&
//                            edtLoaninstallment.testValidity() &&
//                            edtExpComitteeSavings.testValidity() &&
//                            edtMedical.testValidity()
//                    ) {
                    serviceCall();
//                    } else UIHelper.showToast(getContext(), "Please fill all required fields");
                }
                break;
        }
    }

//    @OnClick({R.id.spExistingLoan, R.id.contExLoanAmount})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.spExistingLoan:
//                break;
//            case R.id.contExLoanAmount:
//                break;
//        }
//    }
}