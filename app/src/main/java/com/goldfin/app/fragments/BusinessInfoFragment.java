package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.DateHelper;
import com.goldfin.app.helperclasses.GooglePlaceHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.DateManager;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BusinessInfoFragment extends BaseFragment
        implements GooglePlaceHelper.GooglePlaceDataInterface {


    Unbinder unbinder;

    GooglePlaceHelper googlePlaceHelper;
    @BindView(R.id.edtBusinessTitle)
    AnyEditTextView edtBusinessTitle;
    @BindView(R.id.spBusinessStatus)
    Spinner spBusinessStatus;
    @BindView(R.id.edtBusAddress)
    AnyEditTextView edtBusAddress;
    @BindView(R.id.edtBusLandline)
    AnyEditTextView edtBusLandline;
    @BindView(R.id.edtWebsite)
    AnyEditTextView edtWebsite;
    @BindView(R.id.edtStatus)
    AnyEditTextView edtStatus;
    @BindView(R.id.spNOB)
    Spinner spNOB;
    @BindView(R.id.edtCompanyName)
    AnyEditTextView edtCompanyName;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.edtEstSince)
    AnyEditTextView edtEstSince;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.SECPNumber)
    AnyEditTextView edtSECPNumber;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.edtNTNNo)
    AnyEditTextView edtNTNNo;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.edtNoofEmployee)
    AnyEditTextView edtNoofEmployee;
    @BindView(R.id.spWorking)
    Spinner spWorking;
    @BindView(R.id.edtOwnedLand)
    AnyEditTextView edtOwnedLand;
    @BindView(R.id.edtRentedLand)
    AnyEditTextView edtRentedLand;
    @BindView(R.id.edtCultivatingSince)
    AnyEditTextView edtCultivatingSince;
    @BindView(R.id.spCrops)
    Spinner spCrops;
    @BindView(R.id.edtAnimalCount)
    AnyEditTextView edtAnimalCount;
    @BindView(R.id.btnNext)
    Button btnNext;
    private UserTypeEnum userTypeEnum;
    private String businessStatu = "", natureofbusiness = "", working = "", maincrop = "";
    private boolean isFromSearch;
    private UserAppointmentsModel userAppointmentsModel;


    //
    public static BusinessInfoFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        BusinessInfoFragment fragment = new BusinessInfoFragment();
        fragment.setArguments(args);
        fragment.isFromSearch = isFromSearch;
        fragment.userAppointmentsModel = userAppointmentsModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_business_info_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googlePlaceHelper = new GooglePlaceHelper(getBaseActivity(), 0, this, BusinessInfoFragment.this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFromSearch) serviceCallGetData();
        else if (getCurrentUser() != null && getCurrentUser().getProfessionalInformation().equalsIgnoreCase("true")) {
            serviceCallGetData();
        } else txtDate.setText(DateHelper.getCurrentDate());

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

        spBusinessStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                businessStatu = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spCrops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maincrop = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spWorking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                working = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spNOB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                natureofbusiness = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        FormSendingDataModel formSendingDataModel = new FormSendingDataModel();
        formSendingDataModel.setId(getCurrentUser().getId());
        formSendingDataModel.setAnimals(edtAnimalCount.getStringTrimmed());
        formSendingDataModel.setBussinessaddress(edtBusAddress.getStringTrimmed());
        formSendingDataModel.setBussinessname(edtBusinessTitle.getStringTrimmed());
        formSendingDataModel.setBussinesslandline(edtBusLandline.getStringTrimmed());
        formSendingDataModel.setBusinessStatus(businessStatu);
        formSendingDataModel.setCompanyname(edtCompanyName.getStringTrimmed());
        formSendingDataModel.setCultivating(edtCultivatingSince.getStringTrimmed());
        formSendingDataModel.setEstablishsince(edtEstSince.getStringTrimmed());
        formSendingDataModel.setMaincrops(maincrop);
        formSendingDataModel.setNbrofemp(edtNoofEmployee.getStringTrimmed());
        formSendingDataModel.setNatureofbusiness(natureofbusiness);
        formSendingDataModel.setNtn(edtNTNNo.getStringTrimmed());
        formSendingDataModel.setBussinesswebsite(edtWebsite.getStringTrimmed());
        formSendingDataModel.setRentedland(edtRentedLand.getStringTrimmed());
        formSendingDataModel.setOwnland(edtOwnedLand.getStringTrimmed());
        formSendingDataModel.setSecp(edtSECPNumber.getStringTrimmed());
        formSendingDataModel.setDateofreg(txtDate.getStringTrimmed());
//        formSendingDataModel.setAddressapp(edtBusAddress.getStringTrimmed());


        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectForm(formSendingDataModel, WebServiceConstants.METHOD_BUSINESS_PROESSIONALINFO,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(BusinessFinancialInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
//                                UIHelper.showToast(getContext(), "Will be implemented in future");
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void setDate() {
        DateManager.showDatePicker(getContext(), txtDate, AppConstants.ACCESS_LOG_DATE_SHOW, (datePicker, i, i1, i2) -> {
            final Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, i);
            myCalendar.set(Calendar.MONTH, i1);
            myCalendar.set(Calendar.DAY_OF_MONTH, i2);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            String myFormat = AppConstants.ACCESS_LOG_DATE_SEND; // In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        }, false, null);
    }


    private void bindData(FormSendingDataModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
                break;
            case CSO:
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false")) {
                    enableInputs();
                } else {
                    if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Deprecated"))
                        enableInputs();
                    else
                        disableInputs();
                }
        }


        if (customerData != null) {
            if (customerData.getDateofreg() != null)
                txtDate.setText(customerData.getDateofreg());

            if (customerData.getBussinesswebsite() != null)
                edtWebsite.setText(customerData.getBussinesswebsite());


            if (customerData.getAnimals() != null)
                edtAnimalCount.setText(customerData.getAnimals());
            if (customerData.getBussinessaddress() != null)
                edtBusAddress.setText(customerData.getBussinessaddress());
            if (customerData.getBussinessname() != null)
                edtBusinessTitle.setText(customerData.getBussinessname());

            if (customerData.getBussinesslandline() != null)
                edtBusLandline.setText(customerData.getBussinesslandline());


            if (customerData.getBusinessStatus() != null) {
                businessStatu = (customerData.getBusinessStatus());
                spBusinessStatus.setSelection(((ArrayAdapter) spBusinessStatus.getAdapter()).getPosition(businessStatu));
            }
            if (customerData.getNatureofbusiness() != null) {
                natureofbusiness = (customerData.getNatureofbusiness());
                spNOB.setSelection(((ArrayAdapter) spNOB.getAdapter()).getPosition(natureofbusiness));
            }
            if (customerData.getMaincrops() != null) {
                maincrop = (customerData.getMaincrops());
                spCrops.setSelection(((ArrayAdapter) spCrops.getAdapter()).getPosition(maincrop));
            }

            if (customerData.getCompanyname() != null)
                edtCompanyName.setText(customerData.getCompanyname());

            if (customerData.getCultivating() != null)
                edtCultivatingSince.setText(customerData.getCultivating());


            if (customerData.getNbrofemp() != null)
                edtNoofEmployee.setText(customerData.getNbrofemp());

            if (customerData.getNtn() != null)
                edtNTNNo.setText(customerData.getNtn());
            if (customerData.getRentedland() != null)
                edtRentedLand.setText(customerData.getRentedland());
            if (customerData.getOwnland() != null)
                edtOwnedLand.setText(customerData.getOwnland());
            if (customerData.getSecp() != null)
                edtSECPNumber.setText(customerData.getSecp());
            if (customerData.getEstablishsince() != null)
                edtEstSince.setText(customerData.getEstablishsince());
            if (customerData.getDateofreg() != null)
                txtDate.setText(customerData.getDateofreg());


        }
    }

    private void enableInputs() {
        edtAnimalCount.setEnabled(true);
        edtBusAddress.setEnabled(true);
        edtBusinessTitle.setEnabled(true);
        edtBusLandline.setEnabled(true);
        txtDate.setEnabled(true);
        edtWebsite.setEnabled(true);
        spCrops.setEnabled(true);
        edtCompanyName.setEnabled(true);
        edtCultivatingSince.setEnabled(true);

        edtEstSince.setEnabled(true);
        spNOB.setEnabled(true);
        edtNoofEmployee.setEnabled(true);
        spBusinessStatus.setEnabled(true);

        edtNTNNo.setEnabled(true);

        edtOwnedLand.setEnabled(true);
        edtRentedLand.setEnabled(true);
        edtStatus.setEnabled(true);
        spWorking.setEnabled(true);
        edtSECPNumber.setEnabled(true);
    }

    private void disableInputs() {
        edtAnimalCount.setEnabled(false);
        edtBusAddress.setEnabled(false);
        edtBusinessTitle.setEnabled(false);
        edtBusLandline.setEnabled(false);
        txtDate.setEnabled(false);
        edtWebsite.setEnabled(false);
        spBusinessStatus.setEnabled(false);
        edtCompanyName.setEnabled(false);
        edtCultivatingSince.setEnabled(false);

        edtEstSince.setEnabled(false);
        spWorking.setEnabled(false);
        edtNoofEmployee.setEnabled(false);
        spNOB.setEnabled(false);

        edtNTNNo.setEnabled(false);

        edtOwnedLand.setEnabled(false);
        edtRentedLand.setEnabled(false);
        edtStatus.setEnabled(false);
        spCrops.setEnabled(false);
        edtSECPNumber.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (sharedPreferenceManager.getCustomerData().getUserId() != null)
                id = sharedPreferenceManager.getCustomerData().getUserId();
            else id = sharedPreferenceManager.getCustomerData().getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_LAF2 + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }


    @Override
    public void onPlaceActivityResult(double longitude, double latitude, String locationName) {

    }

    @Override
    public void onError(String error) {

    }

    @OnClick({R.id.txtDate, R.id.btnNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                setDate();
                break;
            case R.id.btnNext:
                if (isFromSearch) {
                    serviceCall();
                } else {
                    /*if (!edtBusinessTitle.testValidity() &&
                            !businessStatu.equalsIgnoreCase("") &&
                            edtBusAddress.testValidity() &&
                            edtBusLandline.testValidity() && edtWebsite.testValidity() && edtStatus.testValidity() &&
                            !natureofbusiness.equalsIgnoreCase("") &&
                            edtCompanyName.testValidity() &&
                            edtEstSince.testValidity() &&
                            edtSECPNumber.testValidity() &&
                            edtNTNNo.testValidity() &&
                            edtNoofEmployee.testValidity() &&
                            !working.equalsIgnoreCase("") &&
                            edtOwnedLand.testValidity() &&
                            edtRentedLand.testValidity() &&
                            edtCultivatingSince.testValidity() &&
                            !maincrop.equalsIgnoreCase("") &&
                            edtAnimalCount.testValidity()
                    ) {*/
                    serviceCall();
//                    } else UIHelper.showToast(getContext(), "Please fill all required fields");
                }
                break;
        }
    }
}