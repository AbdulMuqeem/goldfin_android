package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.CalculatedValuesModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CalculatorFragment extends BaseFragment implements AdapterView.OnItemSelectedListener {


    Unbinder unbinder;
    @BindView(R.id.edtAmount)
    AnyEditTextView edtAmount;
    @BindView(R.id.edtGold)
    AnyEditTextView edtGold;
    @BindView(R.id.contCalculate)
    RoundKornerLinearLayout contCalculate;
    @BindView(R.id.contApplyNow)
    RoundKornerLinearLayout contApplyNow;
    @BindView(R.id.txtResult)
    AnyTextView txtResult;
    @BindView(R.id.txtType)
    AnyTextView txtType;
    @BindView(R.id.spUnitType)
    Spinner spUnitType;
    private String type;
    ArrayList<String> arrType;
    private CalculatedValuesModel calculatedValuesModel;
    private String gold;
    private String Amount;
    private boolean isFromFinan;

    //
    public static CalculatorFragment newInstance(boolean isFromFinan) {
        Bundle args = new Bundle();
        CalculatorFragment fragment = new CalculatorFragment();
        fragment.setArguments(args);
        fragment.isFromFinan = isFromFinan;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_calc;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrType = new ArrayList<>();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner();
        edtGold.setText("");
        edtAmount.setText("");
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }

    @Override
    public void setListeners() {
        spUnitType.setOnItemSelectedListener(this);


        edtAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    //this if condition is true when edittext lost focus...
                    //check here for number is larger than 10 or not
                    if (edtAmount != null && !edtAmount.getStringTrimmed().equalsIgnoreCase("")) {
                        edtAmount.setText("");
                        spUnitType.setEnabled(true);
                    }
                } else edtGold.setEnabled(false);
            }
        });
        edtGold.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    //this if condition is true when edittext lost focus...
                    //check here for number is larger than 10 or not
                    if (edtGold != null && !edtGold.getStringTrimmed().equalsIgnoreCase("")) {
                        edtGold.setText("");
                        spUnitType.setEnabled(false);
                    }
                } else edtAmount.setEnabled(false);
            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.contCalculate, R.id.contApplyNow, R.id.txtType, R.id.edtAmount, R.id.edtGold})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.contCalculate:
                calc(true);

                break;
            case R.id.contApplyNow:
                if (isFromFinan) {
                    popBackStack();
                } else
                    getBaseActivity().addDockableFragment(SignInUpFragment.newInstance(), false);
                break;
            case R.id.txtType:
                spUnitType.performClick();
                break;
            case R.id.edtAmount:
//                if (edtAmount.getStringTrimmed().equalsIgnoreCase("")) {
//                } else edtAmount.setText("");
                break;
            case R.id.edtGold:
//                if (edtGold.getStringTrimmed().equalsIgnoreCase("")) {
//                } else edtGold.setText("");
                break;
        }
    }

    private void calc(boolean b) {

        if (edtAmount.getStringTrimmed().equalsIgnoreCase("") && edtGold.getStringTrimmed().equalsIgnoreCase("")) {
            if (b) UIHelper.showToast(getContext(), "Please Enter Value of Gold or Amount");
        } else if (!edtAmount.getStringTrimmed().equalsIgnoreCase("") && !edtGold.getStringTrimmed().equalsIgnoreCase("")) {

            edtAmount.setText("");
            edtGold.setText("");
        } else {
            if (edtGold.getStringTrimmed().equalsIgnoreCase("")) {
                servicCall(WebServiceConstants.METHOD_RUPEE, edtAmount.getStringTrimmed(), "");
            } else {
                servicCall(WebServiceConstants.METHOD_GOLDTOLAGRAM, edtGold.getStringTrimmed(), type);
            }
        }

    }


    private void servicCall(String methodname, String value, String type) {
        String url;
        if (type.equalsIgnoreCase("")) {
            url = methodname + value + "/";
        } else url = methodname + type.toLowerCase() + "/" + value + "/";
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + url, true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                calculatedValuesModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), CalculatedValuesModel.class);
                                if (!type.equalsIgnoreCase("")) {
                                    edtAmount.setText(calculatedValuesModel.getRequiredAmount());
                                } else {
                                    if (type.equalsIgnoreCase("Tola"))
                                        edtGold.setText(calculatedValuesModel.getTolaRequired().replaceAll("[^\\d.]", ""));
                                    else
                                        edtGold.setText(calculatedValuesModel.getGramRequired().replaceAll("[^\\d.]", ""));
                                }

                                edtGold.setEnabled(true);
                                edtAmount.setEnabled(true);
                                txtType.setEnabled(true);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    private void spinner() {
        arrType.clear();
        arrType.add("Gram");
        arrType.add("Tola");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getBaseActivity(),
                R.layout.item_spinner, arrType);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUnitType.setAdapter(adapter);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        type = (String) parent.getSelectedItem();
        txtType.setText(type);

        if (calculatedValuesModel != null &&
                calculatedValuesModel.getTolaRequired() != null &&
                calculatedValuesModel.getGramRequired() != null) {
            if (type.equalsIgnoreCase("Tola"))
                edtGold.setText(calculatedValuesModel.getTolaRequired().replaceAll("[^\\d.]", ""));
            else
                edtGold.setText(calculatedValuesModel.getGramRequired().replaceAll("[^\\d.]", ""));

        } else {
            edtAmount.setText("");
            calc(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}

