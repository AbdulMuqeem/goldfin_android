package com.goldfin.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.DateManager;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;
import com.google.common.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;


public class CollateralInfoFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtShroffName)
    AnyEditTextView edtShroffName;
    @BindView(R.id.edtShroffCertNo)
    AnyEditTextView edtShroffCertNo;
    @BindView(R.id.edtNatureofGold)
    AnyEditTextView edtNatureofGold;
    @BindView(R.id.edtGrossWeight)
    AnyEditTextView edtGrossWeight;
    @BindView(R.id.edtNetWeight)
    AnyEditTextView edtNetWeight;
    @BindView(R.id.edtMarketValue)
    AnyEditTextView edtMarketValue;
    @BindView(R.id.edtPaket1)
    AnyEditTextView edtPaket1;
    @BindView(R.id.edtGrossWeightCMO)
    AnyEditTextView edtGrossWeightCMO;
    @BindView(R.id.edtPaket2)
    AnyEditTextView edtPaket2;
    @BindView(R.id.edtBarCode)
    AnyEditTextView edtBarCode;
    @BindView(R.id.edtName)
    AnyEditTextView edtName;
    @BindView(R.id.edtUserCode)
    AnyEditTextView edtUserCode;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.edtNG1)
    AnyEditTextView edtNG1;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.edtNG2)
    AnyEditTextView edtNG2;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.edtNG3)
    AnyEditTextView edtNG3;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.edtNG4)
    AnyEditTextView edtNG4;
    @BindView(R.id.tv6)
    TextView tv6;
    @BindView(R.id.edtNG5)
    AnyEditTextView edtNG5;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.txtPack1)
    TextView txtPack1;
    @BindView(R.id.viewP1)
    View viewP1;
    @BindView(R.id.txtPack2)
    TextView txtPack2;
    @BindView(R.id.viewPack2)
    View viewPack2;
    private ArrayList<String> arrayGoldDetails;
    private File filePacket1;
    private File filePacket2;
    String attachmentType = "";
    private UserTypeEnum userTypeEnum;
    private boolean isFromSearch;
    private UserAppointmentsModel userAppointmentsModel;

    //
    public static CollateralInfoFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        CollateralInfoFragment fragment = new CollateralInfoFragment();
        fragment.userAppointmentsModel = userAppointmentsModel;
        fragment.isFromSearch = isFromSearch;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_collateral_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayGoldDetails = new ArrayList<>();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getCurrentUser().getType().equalsIgnoreCase("cmo")) {
            serviceCallGetData();
            isFromSearch = false;
        }
        if (isFromSearch) {
            btnNext.setText("Back");
            if (userAppointmentsModel != null && userAppointmentsModel.getCmoDone().equalsIgnoreCase("true")) {
                serviceCallGetData();
            }
        }

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {

        arrayGoldDetails.add(edtNG1.getStringTrimmed());
        arrayGoldDetails.add(edtNG2.getStringTrimmed());
        arrayGoldDetails.add(edtNG3.getStringTrimmed());
        arrayGoldDetails.add(edtNG4.getStringTrimmed());
        arrayGoldDetails.add(edtNG5.getStringTrimmed());


        CMOModel cmoModel = new CMOModel();

        cmoModel.setCustomerId(sharedPreferenceManager.getCustomerData().getUserId());
        cmoModel.setCMOId(getCurrentUser().getId());
        cmoModel.setFormId(sharedPreferenceManager.getCustomerData().getForm_id());
        cmoModel.setShroffName(edtShroffName.getStringTrimmed());
        cmoModel.setShroffCertificateNumber(edtShroffCertNo.getStringTrimmed());
        cmoModel.setNatureOfGold(edtNatureofGold.getStringTrimmed());
        cmoModel.setGrossWeight(edtGrossWeight.getStringTrimmed());
        cmoModel.setNetWeight(edtNetWeight.getStringTrimmed());
        cmoModel.setMarketValue(edtMarketValue.getStringTrimmed());
        cmoModel.setSecuredPacket1Number(edtPaket1.getStringTrimmed());
        cmoModel.setSecuredPacket2Number(edtPaket2.getStringTrimmed());
        cmoModel.setGrossWeightCMO(edtGrossWeightCMO.getStringTrimmed());
        cmoModel.setBarCodeNumber(edtBarCode.getStringTrimmed());
        cmoModel.setUserCode(edtUserCode.getStringTrimmed());
        cmoModel.setName(edtName.getStringTrimmed());
        cmoModel.setDate(txtDate.getStringTrimmed());
        cmoModel.setDetailsGold(arrayGoldDetails);


        if (filePacket1 != null)
            cmoModel.setPicture1(filePacket1.getPath());
        else {
            UIHelper.showToast(getContext(), "Please attach picture");
            return;
        }
        if (filePacket2 != null)
            cmoModel.setPicture2(filePacket2.getPath());
        else {
            UIHelper.showToast(getContext(), "Please attach picture");
            return;
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectCMO(cmoModel, WebServiceConstants.METHOD_CMO,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                servicCallFormSatus();
                                emptyBackStack();
                                UIHelper.showToast(getContext(), "Form Submitted");
                                getBaseActivity().addDockableFragment(SearchFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void setDate() {
        DateManager.showDatePicker(getContext(), txtDate, AppConstants.ACCESS_LOG_DATE_SHOW, (datePicker, i, i1, i2) -> {
            final Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, i);
            myCalendar.set(Calendar.MONTH, i1);
            myCalendar.set(Calendar.DAY_OF_MONTH, i2);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            String myFormat = AppConstants.ACCESS_LOG_DATE_SEND; // In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        }, false, null);
    }


    @OnClick({R.id.txtDate, R.id.btnNext, R.id.txtPack1, R.id.txtPack2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                setDate();
                break;
            case R.id.btnNext:
                if (isFromSearch) popBackStack();
                else {
                    if (edtShroffName.testValidity() && edtShroffCertNo.testValidity() && edtNatureofGold.testValidity()
                            && edtGrossWeight.testValidity() && edtGrossWeightCMO.testValidity() && edtMarketValue.testValidity() && edtPaket2.testValidity() &&
                            edtName.testValidity() && edtNetWeight.testValidity() && edtPaket1.testValidity() && edtBarCode.testValidity() &&
                            edtUserCode.testValidity())
                        serviceCall();
                }
                break;

            case R.id.txtPack1:
                attachmentType = AppConstants.ATTACHMENT_PICTURE_1;
                cropImagePicker();
                break;

            case R.id.txtPack2:
                attachmentType = AppConstants.ATTACHMENT_PICTURE_2;
                cropImagePicker();
                break;
        }
    }


    private void cropImagePicker() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMinCropWindowSize(192, 192)
                .setMinCropResultSize(192, 192)
                .setMultiTouchEnabled(false)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                // FIXME: 15-Jul-17 Fix Quality if required
                .setRequestedSize(640, 640, CropImageView.RequestSizeOptions.RESIZE_FIT)
                .setOutputCompressQuality(80)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                switch (attachmentType) {

                    case AppConstants.ATTACHMENT_PICTURE_1:
                        filePacket1 = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_PICTURE_2:
                        filePacket2 = new File(result.getUri().getPath());
                        break;


                    default:
                        UIHelper.showShortToastInCenter(getContext(), "Attachment type undefined");

                }

                setImageAfterResult(result.getUri().toString());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }


    private void setAndUploadImage(String uploadFilePath) throws IOException {
        switch (attachmentType) {
            case AppConstants.ATTACHMENT_PICTURE_1:
                viewP1.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;
            case AppConstants.ATTACHMENT_PICTURE_2:
                viewPack2.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                break;


        }
    }

    private void setImageAfterResult(final String uploadFilePath) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    setAndUploadImage(uploadFilePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /*getServiceCall*/

    private void bindData(CMOModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case VO:
            case BO:
            case CSO:
                disableInputs();
                break;
            case CMO:
                enableInputs();
                break;
        }

        if (customerData != null) {

            if (customerData.getDate() != null)
                txtDate.setText(customerData.getDate());
            if (customerData.getShroffName() != null)
                edtShroffName.setText(customerData.getShroffName());
            if (customerData.getUserCode() != null) edtUserCode.setText(customerData.getUserCode());
            if (customerData.getShroffCertificateNumber() != null)
                edtShroffCertNo.setText(customerData.getShroffCertificateNumber());
            if (customerData.getSecuredPacket2Number() != null)
                edtPaket2.setText(customerData.getSecuredPacket2Number());
            if (customerData.getSecuredPacket1Number() != null)
                edtPaket1.setText(customerData.getSecuredPacket1Number());
            if (customerData.getNetWeight() != null)
                edtNetWeight.setText(customerData.getNetWeight());
            if (customerData.getNatureOfGold() != null)
                edtNatureofGold.setText(customerData.getNatureOfGold());
            if (customerData.getName() != null) edtName.setText(customerData.getName());
            if (customerData.getMarketValue() != null)
                edtMarketValue.setText(customerData.getMarketValue());
            if (customerData.getGrossWeight() != null)
                edtGrossWeight.setText(customerData.getGrossWeight());
            if (customerData.getBarCodeNumber() != null)
                edtBarCode.setText(customerData.getBarCodeNumber());
            if (customerData.getUserCode() != null) edtUserCode.setText(customerData.getUserCode());
            if (customerData.getGrossWeightCMO() != null)
                edtGrossWeightCMO.setText(customerData.getGrossWeightCMO());
            if (customerData.getDetailsGold().size() > 0) {
                edtNG1.setText(customerData.getDetailsGold().get(0));
                edtNG2.setText(customerData.getDetailsGold().get(1));
                edtNG3.setText(customerData.getDetailsGold().get(2));
                edtNG4.setText(customerData.getDetailsGold().get(3));
                edtNG5.setText(customerData.getDetailsGold().get(4));
            }
            if (customerData.getPicture1() != null) {
                viewP1.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

            }
            if (customerData.getPicture2() != null) {
                viewPack2.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

            }
        }
    }

    private void enableInputs() {
        edtShroffName.setEnabled(true);
        edtUserCode.setEnabled(true);
        edtShroffCertNo.setEnabled(true);
        edtPaket2.setEnabled(true);
        edtPaket1.setEnabled(true);
        edtNetWeight.setEnabled(true);
        edtNatureofGold.setEnabled(true);
        edtName.setEnabled(true);
        edtMarketValue.setEnabled(true);
        edtGrossWeight.setEnabled(true);
        edtBarCode.setEnabled(true);
        edtUserCode.setEnabled(true);
        edtGrossWeightCMO.setEnabled(true);
        edtNG1.setEnabled(true);
        edtNG2.setEnabled(true);
        edtNG3.setEnabled(true);
        edtNG4.setEnabled(true);
        edtNG5.setEnabled(true);
    }

    private void disableInputs() {
        edtShroffName.setEnabled(false);
        edtUserCode.setEnabled(false);
        edtShroffCertNo.setEnabled(false);
        edtPaket2.setEnabled(false);
        edtPaket1.setEnabled(false);
        edtNetWeight.setEnabled(false);
        edtNatureofGold.setEnabled(false);
        edtName.setEnabled(false);
        edtMarketValue.setEnabled(false);
        edtGrossWeight.setEnabled(false);
        edtBarCode.setEnabled(false);
        edtUserCode.setEnabled(false);
        edtGrossWeightCMO.setEnabled(false);
        edtNG1.setEnabled(false);
        edtNG2.setEnabled(false);
        edtNG3.setEnabled(false);
        edtNG4.setEnabled(false);
        edtNG5.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (userAppointmentsModel.getUserId() != null)
                id = userAppointmentsModel.getUserId();
            else id = userAppointmentsModel.getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_CMO_FORM +
                id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<CMOModel>>() {
                                }.getType();
                                ArrayList<CMOModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                if (arrayList1 != null && arrayList1.size() > 0) {
                                    CMOModel formSendingDataModel = arrayList1.get(0);
                                    bindData(formSendingDataModel);
                                }

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }
}