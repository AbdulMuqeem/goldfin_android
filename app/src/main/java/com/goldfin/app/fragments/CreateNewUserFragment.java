package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.validator.CnicValidation;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.NewUser;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.google.common.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.AppConstants.CNIC_MASK;


public class CreateNewUserFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtEmployerName)
    AnyEditTextView edtEmployerName;
    @BindView(R.id.edtCNIC)
    AnyEditTextView edtCNIC;
    @BindView(R.id.edtEmployerAdress)
    AnyEditTextView edtEmployerAdress;
    @BindView(R.id.btnFinish)
    Button btnFinish;
    UserAppointmentsModel userAppointmentsModel = new UserAppointmentsModel();

    public static CreateNewUserFragment newInstance() {
        Bundle args = new Bundle();
        CreateNewUserFragment fragment = new CreateNewUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_new_user;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtCNIC.addTextChangedListener(new MaskFormatter(CNIC_MASK, edtCNIC, '-'));
        edtCNIC.addValidator(new CnicValidation());

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void servicCall() {


        CMOModel cmoModel = new CMOModel();

        cmoModel.setFullname(edtEmployerName.getStringTrimmed());
        cmoModel.setAddress(edtEmployerAdress.getStringTrimmed());
        cmoModel.setCnic(edtCNIC.getStringTrimmed());
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectCMO(cmoModel, WebServiceConstants.METHOD_CMO_ADD_USER,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                NewUser newUser = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), NewUser.class);

                                userAppointmentsModel.setForm_id(newUser.getFormId());
                                userAppointmentsModel.setUserId(newUser.getId());
                                sharedPreferenceManager.putObject("KEY_CUSTOMER_DATA", userAppointmentsModel);


                                getBaseActivity().addDockableFragment(CollateralInfoFragment.newInstance(false,
                                        sharedPreferenceManager.getCustomerData()), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    @OnClick(R.id.btnFinish)
    public void onViewClicked() {
        if (edtEmployerName.testValidity() && edtCNIC.testValidity() && edtEmployerAdress.testValidity()) {

            servicCall();
        }
    }

//    private void servicCallCheckCNIC() {
//        new WebServices(getBaseActivity(),
//                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_CNIC_CHECK + edtCNIC.getStringTrimmed() + "/",
//                true)
//                .webServiceRequestAPIAnyObjectGet(
////
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                            }
//                        });


//    }

}