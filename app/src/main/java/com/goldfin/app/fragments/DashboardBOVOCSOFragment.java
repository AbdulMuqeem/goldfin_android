package com.goldfin.app.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.WebServiceConstants.METHOD_GET_FORWARD_BO;
import static com.goldfin.app.constatnts.WebServiceConstants.METHOD_GET_FORWARD_VO;
import static com.goldfin.app.constatnts.WebServiceConstants.METHOD_GET_LOAN_STATUS;


public class DashboardBOVOCSOFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.txtStatus)
    AnyTextView txtStatus;
    @BindView(R.id.txtName)
    AnyTextView txtName;
    @BindView(R.id.txtCNIC)
    AnyTextView txtCNIC;
    @BindView(R.id.btnLAF)
    LinearLayout btnLAF;
    @BindView(R.id.btnShroffCert)
    LinearLayout btnShroffCert;
    @BindView(R.id.btnVO)
    LinearLayout btnVO;
    @BindView(R.id.btnDocs)
    LinearLayout btnDocs;
    @BindView(R.id.btnForward)
    LinearLayout btnForward;
    @BindView(R.id.lableForwar)
    TextView lableForwar;
    @BindView(R.id.spLoan)
    Spinner spLoan;
    @BindView(R.id.contSpLoan)
    LinearLayout contSpLoan;

    private UserAppointmentsModel userAppointmentsModel;
    private boolean isFromSearch;
    private boolean isFromBO;

    public static DashboardBOVOCSOFragment newInstance(UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        DashboardBOVOCSOFragment fragment = new DashboardBOVOCSOFragment();
        fragment.setArguments(args);
        fragment.userAppointmentsModel = userAppointmentsModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_bovocso;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private UserTypeEnum userTypeEnum;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtCNIC.setText(userAppointmentsModel.getCnic());
        txtName.setText(userAppointmentsModel.getUserName());
        txtStatus.setText(userAppointmentsModel.getLoanStatus());
        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        if (userAppointmentsModel.getCmoDone() != null && userAppointmentsModel.getCmoDone().equalsIgnoreCase("true")) {
            btnShroffCert.setEnabled(true);
        } else btnShroffCert.setEnabled(false);
        if (userAppointmentsModel.getVoDone() != null && userAppointmentsModel.getVoDone().equalsIgnoreCase("true")) {
            btnVO.setEnabled(true);
        } else btnVO.setEnabled(false);
        if (userAppointmentsModel.getFormDone() != null && userAppointmentsModel.getFormDone().equalsIgnoreCase("true")) {
            btnDocs.setEnabled(true);
        } else btnDocs.setEnabled(false);

        switch (userTypeEnum) {

            case CUSTOMER:
                break;
            case CMO:
                btnLAF.setVisibility(View.GONE);
                btnVO.setVisibility(View.GONE);
                btnDocs.setVisibility(View.GONE);
                contSpLoan.setVisibility(View.GONE);
                btnForward.setVisibility(View.GONE);
                btnShroffCert.setVisibility(View.VISIBLE);
                isFromSearch = true;
                break;
            case VO:
                btnLAF.setVisibility(View.VISIBLE);
                btnShroffCert.setVisibility(View.GONE);
                btnDocs.setVisibility(View.GONE);
                contSpLoan.setVisibility(View.GONE);
                btnVO.setVisibility(View.VISIBLE);
                isFromSearch = true;

                break;
            case CSO:
                isFromBO = false;
                btnLAF.setVisibility(View.VISIBLE);
                btnShroffCert.setVisibility(View.GONE);
                btnDocs.setVisibility(View.GONE);
                contSpLoan.setVisibility(View.GONE);
                btnVO.setVisibility(View.GONE);
                lableForwar.setText("Forward to BO");
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false"))
                    btnForward.setVisibility(View.VISIBLE);
                else btnForward.setVisibility(View.GONE);
                isFromSearch = true;
                break;
            case BO:
                isFromBO = true;
                btnLAF.setVisibility(View.VISIBLE);
                btnVO.setVisibility(View.VISIBLE);
                contSpLoan.setVisibility(View.VISIBLE);
                btnDocs.setVisibility(View.VISIBLE);
                btnShroffCert.setVisibility(View.VISIBLE);
                lableForwar.setText("Forward to VO");
                if (userAppointmentsModel.getVo() != null && userAppointmentsModel.getVo().equalsIgnoreCase("false")) {
                    btnForward.setVisibility(View.VISIBLE);
                    btnForward.setEnabled(true);
                    btnForward.setBackground(getResources().getDrawable(R.drawable.leftcutbg));
                } else {
                    btnForward.setBackground(getResources().getDrawable(R.drawable.leftcut_round_grey));
                    btnForward.setEnabled(false);
                }
                isFromSearch = true;


                if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Decline")
                        || userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Approve")) {
                    spLoan.setEnabled(false);
                    contSpLoan.setEnabled(false);
                } else {
                    spLoan.setEnabled(true);
                    contSpLoan.setEnabled(true);
                }
                break;
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

        spLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String loanStatus = (String) adapterView.getSelectedItem();
                if (!loanStatus.equalsIgnoreCase("Loan Status")) {
                    UIHelper.showAlertDialog("Do you really wants to Change Loan Status.", "Alert", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            serviceCallLoanStatus(loanStatus);
                        }
                    }, getContext());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.btnLAF, R.id.btnShroffCert, R.id.btnVO, R.id.btnDocs, R.id.btnForward, R.id.contSpLoan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLAF:

                if (userAppointmentsModel.getProfession() != null && userAppointmentsModel.getProfession().equalsIgnoreCase("Salaried")) {
                    getBaseActivity().addDockableFragment(PersonalInfoFragment.newInstance(false, isFromSearch, userAppointmentsModel), false);
                } else {
                    getBaseActivity().addDockableFragment(PersonalInfoFragment.newInstance(true, isFromSearch, userAppointmentsModel), false);
                }
                break;
            case R.id.btnShroffCert:
//            case R.id.btnCMO:
                getBaseActivity().addDockableFragment(CollateralInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                break;

            case R.id.btnForward:
                UIHelper.showAlertDialog("Do you really wants to forward this.", "Alert", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (isFromBO) {
                            serviceCallforwardbo(METHOD_GET_FORWARD_VO);
                        } else {
                            serviceCallforwardbo(METHOD_GET_FORWARD_BO);
                        }
                    }
                }, getContext());
                break;
            case R.id.btnVO:
                getBaseActivity().addDockableFragment(VerificationFormFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                break;
            case R.id.contSpLoan:

                break;
            case R.id.btnDocs:
                if (userAppointmentsModel.getProfession() != null && userAppointmentsModel.getProfession().equalsIgnoreCase("Salaried")) {
                    getBaseActivity().addDockableFragment(DocumentUploadingFragment.newInstance(isFromSearch, false, userAppointmentsModel), false);
                } else {
                    getBaseActivity().addDockableFragment(DocumentUploadingFragment.newInstance(isFromSearch, true, userAppointmentsModel), false);
                }
                break;
        }
    }

    private void serviceCallforwardbo(String methodName) {
        String id;
        if (userAppointmentsModel.getUserId() != null)
            id = userAppointmentsModel.getUserId();
        else id = userAppointmentsModel.getForm_id();
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL +
                methodName + id + "/true/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                popBackStack();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void serviceCallLoanStatus(String loanStatus) {
        String id;
        if (userAppointmentsModel.getUserId() != null)
            id = userAppointmentsModel.getUserId();
        else id = userAppointmentsModel.getForm_id();
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL +
                METHOD_GET_LOAN_STATUS + id + "/" + loanStatus + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                popBackStack();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

}