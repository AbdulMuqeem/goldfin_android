package com.goldfin.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.fragments.dialogs.GenericPopupDialog;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.DocumentModel;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.AttachedDocumentModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;


public class DocumentUploadingFragment extends BaseFragment {


    Unbinder unbinder;

    @BindView(R.id.btnSkip)
    Button btnSkip;
    @BindView(R.id.btnNext)
    Button btnNext;
    boolean isBusinessUser;
    @BindView(R.id.txtCF)
    TextView txtCF;
    @BindView(R.id.viewCF)
    View viewCF;
    @BindView(R.id.txtCB)
    TextView txtCB;
    @BindView(R.id.viewCB)
    View viewCB;
    @BindView(R.id.txtutility)
    TextView txtutility;
    @BindView(R.id.viewCUB)
    View viewCUB;
    @BindView(R.id.txtSlip)
    TextView txtSlip;
    @BindView(R.id.viewCSS)
    View viewCSS;
    @BindView(R.id.txtJobCert)
    TextView txtJobCert;
    @BindView(R.id.viewJC)
    View viewJC;
    @BindView(R.id.contCSS)
    LinearLayout contCSS;
    @BindView(R.id.contJC)
    LinearLayout contJC;
    @BindView(R.id.txtresidenceBusinessBills)
    TextView txtresidenceBusinessBills;
    @BindView(R.id.viewresidenceBusinessBills)
    View viewresidenceBusinessBills;
    @BindView(R.id.contresidenceBusinessBills)
    LinearLayout contresidenceBusinessBills;
    @BindView(R.id.txtProofOfBusiness)
    TextView txtProofOfBusiness;
    @BindView(R.id.viewproofOfBusiness)
    View viewproofOfBusiness;
    @BindView(R.id.contProofOfBusiness)
    LinearLayout contProofOfBusiness;
    @BindView(R.id.txtProofOfBusinessIncome)
    TextView txtProofOfBusinessIncome;
    @BindView(R.id.viewProofOfBusinessIncome)
    View viewProofOfBusinessIncome;
    @BindView(R.id.contProofOfBusinessIncome)
    LinearLayout contProofOfBusinessIncome;
    @BindView(R.id.txtSECP)
    TextView txtSECP;
    @BindView(R.id.viewSECP)
    View viewSECP;
    @BindView(R.id.contSECP)
    LinearLayout contSECP;
    @BindView(R.id.txtNTN)
    TextView txtNTN;
    @BindView(R.id.viewNTN)
    View viewNTN;
    @BindView(R.id.contNTN)
    LinearLayout contNTN;
    @BindView(R.id.txtExpLetter)
    TextView txtExpLetter;
    @BindView(R.id.viewEL)
    View viewEL;
    @BindView(R.id.contExpLetter)
    LinearLayout contExpLetter;


    private File fileCNICFront;
    private File fileUtility;
    private File fileCNICBack;
    private File fileJobCert;
    private File fileSalarySlip;
    private File fileExpLetter;


    private File fileBusinessProof;
    private File fileBusinessProofIncome;
    private File fileNTN;
    private File fileSEC;
    private File fileResiBusiUtility;

    String attachmentType = "";
    private UserAppointmentsModel userAppointmentsModel;
    private boolean isFromSearch;

    //
    public static DocumentUploadingFragment newInstance(boolean isFromSearch, boolean isBusinessUser, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        DocumentUploadingFragment fragment = new DocumentUploadingFragment();
        fragment.setArguments(args);
        fragment.isBusinessUser = isBusinessUser;
        fragment.isFromSearch = isFromSearch;
        fragment.userAppointmentsModel = userAppointmentsModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_document_upload;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isBusinessUser) {
            contJC.setVisibility(View.GONE);
            contCSS.setVisibility(View.GONE);
            contExpLetter.setVisibility(View.GONE);
            contProofOfBusinessIncome.setVisibility(View.VISIBLE);
            contProofOfBusiness.setVisibility(View.VISIBLE);
            contSECP.setVisibility(View.VISIBLE);
            contNTN.setVisibility(View.VISIBLE);
            contresidenceBusinessBills.setVisibility(View.VISIBLE);
        } else {
            contJC.setVisibility(View.VISIBLE);
            contExpLetter.setVisibility(View.VISIBLE);
            contCSS.setVisibility(View.VISIBLE);
            contProofOfBusinessIncome.setVisibility(View.GONE);
            contProofOfBusiness.setVisibility(View.GONE);
            contSECP.setVisibility(View.GONE);
            contNTN.setVisibility(View.GONE);
            contresidenceBusinessBills.setVisibility(View.GONE);
        }
        if (isFromSearch) {
            if (userAppointmentsModel != null && userAppointmentsModel.getCmoDone().equalsIgnoreCase("true")) {
                serviceCallGetData();
                btnNext.setText("Back");
                btnSkip.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        AttachedDocumentModel attachedDocumentModel = new AttachedDocumentModel();
        attachedDocumentModel.setId(getCurrentUser().getId());
        if (fileCNICBack != null)
            attachedDocumentModel.setCnicBack(fileCNICBack.getPath());
        else attachedDocumentModel.setCnicBack("");
        if (fileCNICFront != null)
            attachedDocumentModel.setCnicFront(fileCNICFront.getPath());
        else attachedDocumentModel.setCnicFront("");
        if (fileUtility != null)
            attachedDocumentModel.setResidenceUtilityBills(fileUtility.getPath());
        else attachedDocumentModel.setResidenceUtilityBills("");
        if (fileSalarySlip != null)
            attachedDocumentModel.setCurrentSalarySlip(fileSalarySlip.getPath());
        else attachedDocumentModel.setCurrentSalarySlip("");
        if (fileJobCert != null)
            attachedDocumentModel.setJobCertificate(fileJobCert.getPath());
        else attachedDocumentModel.setJobCertificate("");
        if (fileResiBusiUtility != null)
            attachedDocumentModel.setResidenceBusinessBills(fileResiBusiUtility.getPath());
        else attachedDocumentModel.setResidenceBusinessBills("");
        if (fileBusinessProof != null)
            attachedDocumentModel.setProofOfBusiness(fileBusinessProof.getPath());
        else attachedDocumentModel.setProofOfBusiness("");
        if (fileBusinessProofIncome != null)
            attachedDocumentModel.setProofOfBusinessIncome(fileBusinessProofIncome.getPath());
        else attachedDocumentModel.setProofOfBusinessIncome("");
        if (fileSEC != null)
            attachedDocumentModel.setsECP(fileSEC.getPath());
        else attachedDocumentModel.setsECP("");
        if (fileNTN != null)
            attachedDocumentModel.setnTN(fileNTN.getPath());
        else attachedDocumentModel.setnTN("");
        if (fileExpLetter != null)
            attachedDocumentModel.setTotalexpense(fileExpLetter.getPath());
        else attachedDocumentModel.setTotalexpense("");

        String methodName;
        if (isBusinessUser)
            methodName = WebServiceConstants.METHOD_ADD_DOCUMENTS_BUSINESS;
        else methodName = WebServiceConstants.METHOD_ADD_DOCUMENTS;

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectDocument(attachedDocumentModel, methodName,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserTypeEnum userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
                                switch (userTypeEnum) {

                                    case CUSTOMER:
                                        getBaseActivity().addDockableFragment(LoanStatusFragment.newInstance(), false);
                                        break;
                                    case CMO:
                                        getBaseActivity().addDockableFragment(FinishThankyouFragment.newInstance(), false);
                                        break;
                                    case VO:
                                        getBaseActivity().addDockableFragment(VerificationFormFragment.newInstance(isFromSearch, sharedPreferenceManager.getCustomerData()), false);

                                        break;
                                    case CSO:
                                        popStackTill(6);
//                                        emptyBackStack();
//                                        getBaseActivity().addDockableFragment(SearchFragment.newInstance(), false);
                                        break;
                                    case BO:
                                        getBaseActivity().addDockableFragment(CollateralInfoFragment.newInstance(isFromSearch, sharedPreferenceManager.getCustomerData()), false);

                                        break;
                                }

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    private void cropImagePicker() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMinCropWindowSize(192, 192)
                .setMinCropResultSize(192, 192)
                .setMultiTouchEnabled(false)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                // FIXME: 15-Jul-17 Fix Quality if required
                .setRequestedSize(640, 640, CropImageView.RequestSizeOptions.RESIZE_FIT)
                .setOutputCompressQuality(80)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                switch (attachmentType) {

                    case AppConstants.ATTACHMENT_CNIC_FRONT:
                        fileCNICFront = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_CNIC_BACK:
                        fileCNICBack = new File(result.getUri().getPath());
                        break;

                    case AppConstants.ATTACHMENT_UTILITY_BILL:
                        fileUtility = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_EXP_LET:
                        fileExpLetter = new File(result.getUri().getPath());
                        break;

                    case AppConstants.ATTACHMENT_CURRENT_SLIP:
                        fileSalarySlip = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_JOB_CERT:
                        fileJobCert = new File(result.getUri().getPath());
                        break;

                    case AppConstants.ATTACHMENT_PROO_OF_BUSINESS:
                        fileBusinessProof = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_PROOF_BUSI_INCOME:
                        fileBusinessProofIncome = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_NTN:
                        fileNTN = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_RESI_BUSI_BILL:
                        fileResiBusiUtility = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_SECP:
                        fileSEC = new File(result.getUri().getPath());
                        break;

                    default:
                        UIHelper.showShortToastInCenter(getContext(), "Attachment type undefined");

                }

                setImageAfterResult(result.getUri().toString());
//                uploadImageFile(filePassport.getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }


    private void setAndUploadImage(String uploadFilePath) throws IOException {
        switch (attachmentType) {
            case AppConstants.ATTACHMENT_CNIC_FRONT:
                viewCF.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;
            case AppConstants.ATTACHMENT_CNIC_BACK:
                viewCB.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                break;
            case AppConstants.ATTACHMENT_UTILITY_BILL:
                viewCUB.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                break;
            case AppConstants.ATTACHMENT_CURRENT_SLIP:
                viewCSS.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                break;
            case AppConstants.ATTACHMENT_JOB_CERT:
                viewJC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;
            case AppConstants.ATTACHMENT_EXP_LET:
                viewEL.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;

            case AppConstants.ATTACHMENT_PROO_OF_BUSINESS:
                viewproofOfBusiness.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;
            case AppConstants.ATTACHMENT_PROOF_BUSI_INCOME:
                viewProofOfBusinessIncome.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;

            case AppConstants.ATTACHMENT_SECP:
                viewSECP.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;

            case AppConstants.ATTACHMENT_NTN:
                viewNTN.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;

            case AppConstants.ATTACHMENT_RESI_BUSI_BILL:
                viewresidenceBusinessBills.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;


        }
    }

    private void setImageAfterResult(final String uploadFilePath) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    setAndUploadImage(uploadFilePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick({R.id.txtCF, R.id.txtCB, R.id.txtutility, R.id.txtSlip, R.id.txtJobCert, R.id.btnSkip, R.id.btnNext, R.id.txtExpLetter,
            R.id.txtresidenceBusinessBills, R.id.txtProofOfBusiness, R.id.txtProofOfBusinessIncome, R.id.txtSECP, R.id.txtNTN})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCF:
                attachmentType = AppConstants.ATTACHMENT_CNIC_FRONT;
                cropImagePicker();
                break;
            case R.id.txtCB:
                attachmentType = AppConstants.ATTACHMENT_CNIC_BACK;
                cropImagePicker();
                break;
            case R.id.txtutility:
                attachmentType = AppConstants.ATTACHMENT_UTILITY_BILL;
                cropImagePicker();
                break;
            case R.id.txtExpLetter:
                attachmentType = AppConstants.ATTACHMENT_EXP_LET;
                cropImagePicker();
                break;
            case R.id.txtSlip:
                attachmentType = AppConstants.ATTACHMENT_CURRENT_SLIP;
                cropImagePicker();
                break;
            case R.id.txtJobCert:
                attachmentType = AppConstants.ATTACHMENT_JOB_CERT;
                cropImagePicker();
                break;


            case R.id.txtresidenceBusinessBills:
                attachmentType = AppConstants.ATTACHMENT_RESI_BUSI_BILL;
                cropImagePicker();
                break;
            case R.id.txtProofOfBusiness:
                attachmentType = AppConstants.ATTACHMENT_PROO_OF_BUSINESS;
                cropImagePicker();
                break;
            case R.id.txtProofOfBusinessIncome:
                attachmentType = AppConstants.ATTACHMENT_PROOF_BUSI_INCOME;
                cropImagePicker();
                break;
            case R.id.txtSECP:
                attachmentType = AppConstants.ATTACHMENT_SECP;
                cropImagePicker();
                break;
            case R.id.txtNTN:
                attachmentType = AppConstants.ATTACHMENT_NTN;
                cropImagePicker();
                break;


            case R.id.btnSkip:
                GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "", "Bring these document at the branch", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getBaseActivity().addDockableFragment(LoanStatusFragment.newInstance(), false);

                    }
                });
                genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);

                break;
            case R.id.btnNext:
                if (isFromSearch) popBackStack();
                else {
                    serviceCall();
                }
                break;
        }
    }


    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (userAppointmentsModel.getUserId() != null)
                id = userAppointmentsModel.getUserId();
            else id = userAppointmentsModel.getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_DOCS +
                id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                DocumentModel model = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), DocumentModel.class);

                                bindData(model);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void bindData(DocumentModel model) {

        if (model.getCnicFront() != null) {
            viewCF.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getCnicBack() != null) {
            viewCB.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getCurrentSalarySlip() != null) {
            viewCSS.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getExperienceLetter() != null) {
            viewEL.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getJobCertificate() != null) {
            viewJC.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getResidenceUtilityBills() != null) {
            viewCUB.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }


        if (model.getnTN() != null) {
            viewNTN.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getsECP() != null) {
            viewSECP.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getProofOfBusiness() != null) {
            viewProofOfBusinessIncome.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getProofOfBusinessIncome() != null) {
            viewProofOfBusinessIncome.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }
        if (model.getResidenceUtilityBills() != null) {
            viewresidenceBusinessBills.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
        }

    }

}