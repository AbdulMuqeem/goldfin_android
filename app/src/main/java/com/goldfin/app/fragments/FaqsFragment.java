package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.adapters.FaqsAdapter;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.models.ContactsModel;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FaqsFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.rvPhone)
    RecyclerView rvPhone;
    FaqsAdapter phoneAdapter;
    ArrayList<ContactsModel> arrayList;

    //
    public static FaqsFragment newInstance() {
        Bundle args = new Bundle();
        FaqsFragment fragment = new FaqsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_faqs;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        phoneAdapter = new FaqsAdapter(getBaseActivity(), arrayList, null);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        arrayList.addAll(arrQuestAnswer());
        bindView();

    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvPhone.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvPhone.getItemAnimator()).setSupportsChangeAnimations(false);
        rvPhone.setAdapter(phoneAdapter);
        phoneAdapter.notifyDataSetChanged();


    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public ArrayList<ContactsModel> arrQuestAnswer() {
        ArrayList<ContactsModel> arrayList = new ArrayList<>();
        arrayList.add(new ContactsModel("What is MeraSarmayaQarza (MSQ)?", getString(R.string.q1), ""));
        arrayList.add(new ContactsModel("What kind of gold needs to be pledged?", getString(R.string.q2), ""));
        arrayList.add(new ContactsModel("Who can get MSQ?", getString(R.string.q3), ""));
        arrayList.add(new ContactsModel("How the gold will be assessed?", getString(R.string.q4), ""));
        arrayList.add(new ContactsModel("How the Gold collateral will be secured?", getString(R.string.q5), ""));
        arrayList.add(new ContactsModel("How quickly is MSQ approved?", getString(R.string.q6), ""));
        arrayList.add(new ContactsModel("What documents are required to avail MSQ?", getString(R.string.q7), ""));
        arrayList.add(new ContactsModel("What is the maximum loan amount?", getString(R.string.q8), ""));
        arrayList.add(new ContactsModel("What is tenure of MSQ?", getString(R.string.q9), ""));
        arrayList.add(new ContactsModel("Can loan be paid before its maturity? And is there any charges on early repayment?", getString(R.string.q10), ""));
        arrayList.add(new ContactsModel("What will happen if Gold Loan is not paid?", getString(R.string.q11), ""));
        return arrayList;
    }
}