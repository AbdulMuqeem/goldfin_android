package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FinancialInfoFragment extends BaseFragment {


    Unbinder unbinder;

    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.edtGrossSalary)
    AnyEditTextView edtGrossSalary;
    @BindView(R.id.edtNetSalary)
    AnyEditTextView edtNetSalary;
    @BindView(R.id.edtOthers)
    AnyEditTextView edtOthers;
    @BindView(R.id.edtSourceoOtherIncom)
    AnyEditTextView edtSourceoOtherIncom;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.tv6)
    TextView tv6;
    @BindView(R.id.edtSourceOLoan)
    AnyEditTextView edtSourceOLoan;
    @BindView(R.id.edtLoanAmount)
    AnyEditTextView edtLoanAmount;
    @BindView(R.id.spAvgMonthlyHousIncome)
    Spinner spAvgMonthlyHousIncome;
    @BindView(R.id.edtHouseHold)
    AnyEditTextView edtHouseHold;
    @BindView(R.id.edtHouseRent)
    AnyEditTextView edtHouseRent;
    @BindView(R.id.edtUtility)
    AnyEditTextView edtUtility;
    @BindView(R.id.edtExpEducation)
    AnyEditTextView edtExpEducation;
    @BindView(R.id.edtLoaninstallment)
    AnyEditTextView edtLoaninstallment;
    @BindView(R.id.edtExpComitteeSavings)
    AnyEditTextView edtExpComitteeSavings;
    @BindView(R.id.edtExistingLoan)
    AnyEditTextView edtExistingLoan;
    @BindView(R.id.edtMedical)
    AnyEditTextView edtMedical;
    @BindView(R.id.edtExpMisc)
    AnyEditTextView edtExpMisc;
    @BindView(R.id.txtTotal)
    AnyTextView txtTotal;
    @BindView(R.id.checkbox1)
    CheckBox checkbox1;
    @BindView(R.id.checkbox2)
    CheckBox checkbox2;
    @BindView(R.id.spExistingLoan)
    Spinner spExistingLoan;
    @BindView(R.id.contExLoanAmount)
    RelativeLayout contExLoanAmount;
    @BindView(R.id.btnCalculate)
    Button btnCalculate;
    private int result, expHouseHold, expRent, expBills, expEduc, expLoan, expSavings, expMedical, expMisc;
    private UserTypeEnum userTypeEnum;
    private boolean isFromSearch;
    private UserAppointmentsModel userAppointmentsModel;
    private String spexLoanYesNo;

    //
    public static FinancialInfoFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        FinancialInfoFragment fragment = new FinancialInfoFragment();
        fragment.isFromSearch = isFromSearch;
        fragment.userAppointmentsModel = userAppointmentsModel;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_salaried_financial_info_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        googlePlaceHelper = new GooglePlaceHelper(getBaseActivity(), 0, this, ProfessionalInfoFragment.this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (isFromSearch) {
//            btnNext.setText("Back");
            serviceCallGetData();
        } else if (getCurrentUser() != null && getCurrentUser().getProfessionalInformation().equalsIgnoreCase("true")) {
            serviceCallGetData();
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

        spExistingLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spexLoanYesNo = (String) adapterView.getSelectedItem();
                if (spexLoanYesNo.equalsIgnoreCase("Yes")) {
                    contExLoanAmount.setVisibility(View.VISIBLE);

                } else contExLoanAmount.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkbox2.setChecked(false);
            }
        });
        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkbox1.setChecked(false);
            }
        });


        edtHouseHold.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtHouseHold != null && !edtHouseRent.getStringTrimmed().equalsIgnoreCase(""))
                    expHouseHold = Integer.parseInt(edtHouseRent.getStringTrimmed());
                txtTotal.performClick();

            }
        });
        edtExpComitteeSavings.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpComitteeSavings != null && edtExpComitteeSavings.getStringTrimmed() != null && !edtExpComitteeSavings.getStringTrimmed().equalsIgnoreCase(""))
                    expSavings = Integer.parseInt(edtExpComitteeSavings.getStringTrimmed());
                txtTotal.performClick();

            }
        });


        edtExpMisc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpMisc != null && !edtExpMisc.getStringTrimmed().equalsIgnoreCase("")) {
                    expMisc = Integer.parseInt(edtExpMisc.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtMedical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtMedical != null && !edtMedical.getStringTrimmed().equalsIgnoreCase("")) {
                    expMedical = Integer.parseInt(edtMedical.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });
        edtLoaninstallment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtLoaninstallment != null && !edtLoaninstallment.getStringTrimmed().equalsIgnoreCase("")) {
                    expLoan = Integer.parseInt(edtLoaninstallment.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });


        edtExpEducation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtExpEducation != null && !edtExpEducation.getStringTrimmed().equalsIgnoreCase("")) {
                    expEduc = Integer.parseInt(edtExpEducation.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });
        edtUtility.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtUtility != null && !edtUtility.getStringTrimmed().equalsIgnoreCase("")) {
                    expBills = Integer.parseInt(edtUtility.getStringTrimmed());
                    txtTotal.performClick();
                }

            }
        });
        edtHouseRent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (edtHouseRent != null && !edtHouseRent.getStringTrimmed().equalsIgnoreCase("")) {
                    expRent = Integer.parseInt(edtHouseRent.getStringTrimmed());
                    txtTotal.performClick();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        FormSendingDataModel formSendingDataModel = new FormSendingDataModel();
        formSendingDataModel.setId(getCurrentUser().getId());
        formSendingDataModel.setGrosssalary(edtGrossSalary.getStringTrimmed());
        formSendingDataModel.setGrosssalary(edtNetSalary.getStringTrimmed());
        formSendingDataModel.setOthersource(edtOthers.getStringTrimmed());
        formSendingDataModel.setNatureofincome(edtSourceoOtherIncom.getStringTrimmed());
        formSendingDataModel.setOutstandingloan(spexLoanYesNo);
        formSendingDataModel.setOutstandingloanamount(edtExistingLoan.getStringTrimmed());
        formSendingDataModel.setLoanamount(edtLoanAmount.getStringTrimmed());
        formSendingDataModel.setSourceofloan(edtSourceOLoan.getStringTrimmed());
        formSendingDataModel.setTotalexp(txtTotal.getStringTrimmed());
        formSendingDataModel.setHousehold(edtHouseHold.getStringTrimmed());
        formSendingDataModel.setHouserent1(edtHouseRent.getStringTrimmed());
        formSendingDataModel.setUtilitybill(edtUtility.getStringTrimmed());
        formSendingDataModel.setChildeducation(edtExpEducation.getStringTrimmed());
        formSendingDataModel.setMedical(edtMedical.getStringTrimmed());
        formSendingDataModel.setCommitte(edtExpComitteeSavings.getStringTrimmed());
        formSendingDataModel.setLoaninstallment(edtLoaninstallment.getStringTrimmed());
        formSendingDataModel.setOther0(edtExpMisc.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectForm(formSendingDataModel, WebServiceConstants.METHOD_FANANCIALINFO,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                getBaseActivity().addDockableFragment(GeneralInfoFragment.newInstance(), false);
//                                UIHelper.showToast(getContext(), "Will be implemented in future");
                                UserTypeEnum userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
                                switch (userTypeEnum) {

                                    case CUSTOMER:
                                    case BO:
                                        getBaseActivity().addDockableFragment(GeneralInfoFragment.newInstance(false), false);
                                        break;
                                    case CMO:
                                        getBaseActivity().addDockableFragment(CollateralInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                        break;
                                    case VO:
                                        getBaseActivity().addDockableFragment(VerificationFormFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                        break;
                                    case CSO:
                                        getBaseActivity().addDockableFragment(DocumentUploadingFragment.newInstance(false, false, null), false);
                                        break;

//                                    36502-8449474-1
                                }
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void bindData(FormSendingDataModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
                enableInputs();
                break;
            case CSO:
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false")) {
                    enableInputs();
                } else {
                    if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Deprecated"))
                        enableInputs();
                    else
                        disableInputs();
                }
                break;
        }


        if (customerData != null) {
            if (customerData.getGrosssalary() != null)
                edtGrossSalary.setText(customerData.getGrosssalary());

            if (customerData.getNetsalary() != null)
                edtNetSalary.setText(customerData.getNetsalary());
            if (customerData.getNatureofincome() != null)
                edtSourceoOtherIncom.setText(customerData.getNatureofincome());

            if (customerData.getOthersource() != null)
                edtOthers.setText(customerData.getOthersource());

            if (customerData.getLoanAmountDesired() != null)
                edtLoanAmount.setText(customerData.getLoanAmountDesired());

            if (customerData.getSourceofloan() != null)
                edtSourceOLoan.setText(customerData.getSourceofloan());

            if (customerData.getTotalexp() != null)
                txtTotal.setText(customerData.getTotalexp());

            if (customerData.getHousehold() != null)
                edtHouseHold.setText(customerData.getHousehold());


            if (customerData.getHouserent() != null)
                edtHouseRent.setText(customerData.getHouserent());

            if (customerData.getUtilitybill() != null)
                edtUtility.setText(customerData.getUtilitybill());

            if (customerData.getChildeducation() != null)
                edtExpEducation.setText(customerData.getChildeducation());

            if (customerData.getMedical() != null)
                edtMedical.setText(customerData.getMedical());

            if (customerData.getCommitte() != null)
                edtExpComitteeSavings.setText(customerData.getCommitte());

            if (customerData.getLoaninstallment() != null)
                edtLoaninstallment.setText(customerData.getLoaninstallment());
            if (customerData.getOther0() != null)
                edtExpMisc.setText(customerData.getOther0());

            if (customerData.getOutstandingloan() != null) {
                spexLoanYesNo = (customerData.getOutstandingloan());
                spExistingLoan.setSelection(((ArrayAdapter) spExistingLoan.getAdapter()).getPosition(spexLoanYesNo));

            }
            if (spexLoanYesNo.equalsIgnoreCase("yes")) {
                edtExistingLoan.setVisibility(View.VISIBLE);
            }
        }
    }

    private void enableInputs() {
        edtExpComitteeSavings.setEnabled(true);
        edtExpEducation.setEnabled(true);
        edtExpMisc.setEnabled(true);
        edtGrossSalary.setEnabled(true);
        edtHouseHold.setEnabled(true);
        edtHouseRent.setEnabled(true);
        edtLoanAmount.setEnabled(true);
        edtLoaninstallment.setEnabled(true);
        edtMedical.setEnabled(true);
        edtExpMisc.setEnabled(true);
        edtExpEducation.setEnabled(true);
        edtExpComitteeSavings.setEnabled(true);
        edtNetSalary.setEnabled(true);
        edtOthers.setEnabled(true);
        edtSourceOLoan.setEnabled(true);
        edtSourceoOtherIncom.setEnabled(true);
        edtUtility.setEnabled(true);
        edtExistingLoan.setEnabled(true);
    }

    private void disableInputs() {
        edtExpComitteeSavings.setEnabled(false);
        edtExpEducation.setEnabled(false);
        edtExpMisc.setEnabled(false);
        edtGrossSalary.setEnabled(false);
        edtHouseHold.setEnabled(false);
        edtHouseRent.setEnabled(false);
        edtLoanAmount.setEnabled(false);
        edtLoaninstallment.setEnabled(false);
        edtMedical.setEnabled(false);
        edtExpMisc.setEnabled(false);
        edtExpEducation.setEnabled(false);
        edtExpComitteeSavings.setEnabled(false);
        edtNetSalary.setEnabled(false);
        edtOthers.setEnabled(false);
        edtSourceOLoan.setEnabled(false);
        edtSourceoOtherIncom.setEnabled(false);
        edtUtility.setEnabled(false);
        edtExistingLoan.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (sharedPreferenceManager.getCustomerData().getUserId() != null)
                id = sharedPreferenceManager.getCustomerData().getUserId();
            else id = sharedPreferenceManager.getCustomerData().getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_LAF3 + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message, "Error", getContext());
                                }
                            }
                        });

    }

    @OnClick({R.id.txtTotal, R.id.btn_next, R.id.btnCalculate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtTotal:
                result = expHouseHold + expRent + expBills + expEduc + expLoan + expSavings + expMedical + expMisc;
                txtTotal.setText(result + "");
                break;
            case R.id.btn_next:
                if (isFromSearch)
                    serviceCall();

                else {
                  /*  if (edtGrossSalary.testValidity() &&
                            edtNetSalary.testValidity() &&
                            edtOthers.testValidity() &&
                            edtSourceoOtherIncom.testValidity() &&
                            edtExistingLoan.testValidity() &&
                            edtSourceOLoan.testValidity() &&

                            edtLoanAmount.testValidity() &&
                            edtHouseHold.testValidity() &&
                            edtHouseRent.testValidity() &&
                            edtUtility.testValidity() &&
                            edtExpEducation.testValidity() &&
                            edtLoaninstallment.testValidity() &&
                            edtExpComitteeSavings.testValidity() &&
                            edtMedical.testValidity()
                    ) {*/
                        serviceCall();
//                    } else UIHelper.showToast(getContext(), "Please fill all required fields");
                }
                break;
            case R.id.btnCalculate:

                getBaseActivity().

                        addDockableFragment(CalculatorFragment.newInstance(true), false);
                break;
        }
    }
}