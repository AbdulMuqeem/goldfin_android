package com.goldfin.app.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;

import com.goldfin.R;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FinishThankyouFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.btnFinish)
    Button btnFinish;
    @BindView(R.id.btnBrowser)
    ImageView btnBrowser;
    @BindView(R.id.btnFb)
    ImageView btnFb;
    @BindView(R.id.btnTwiter)
    ImageView btnTwiter;
    @BindView(R.id.btnLinkedIn)
    ImageView btnLinkedIn;

    //
    public static FinishThankyouFragment newInstance() {
        Bundle args = new Bundle();
        FinishThankyouFragment fragment = new FinishThankyouFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_thankyou;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnFinish, R.id.btnBrowser, R.id.btnFb, R.id.btnTwiter, R.id.btnLinkedIn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnFinish:

                closeApp();

                break;
            case R.id.btnBrowser:
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.google.com/"));
                startActivity(viewIntent);
                break;
            case R.id.btnFb:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.facebook.com/"));
                startActivity(viewIntent);
                break;
            case R.id.btnTwiter:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.twitter.com/"));
                startActivity(viewIntent);
                break;
            case R.id.btnLinkedIn:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.linked.com/"));
                startActivity(viewIntent);
                break;
        }
    }

    public void closeApp() {
        UIHelper.showAlertDialog(getResources().getString(R.string.close_application), "Quit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getBaseActivity().finish();
                        dialogInterface.dismiss();
                    }
                }

                , getContext());


    }


}