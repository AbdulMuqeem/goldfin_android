package com.goldfin.app.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.AppConstants.MOBILE_MASK;


public class Forgot1Fragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtPhoneNumber)
    AnyEditTextView edtPhoneNumber;
    @BindView(R.id.btnSearch)
    AnyTextView btnSearch;

    public static Forgot1Fragment newInstance() {
        Bundle args = new Bundle();
        Forgot1Fragment fragment = new Forgot1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_forgotpass1;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtPhoneNumber.addTextChangedListener(new MaskFormatter(MOBILE_MASK, edtPhoneNumber, '-'));
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }


    @Override
    public void setListeners() {
        edtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 3) {
                    edtPhoneNumber.setText("+92");
                    edtPhoneNumber.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSearch)
    public void onViewClicked() {
        servicCallStatus();
    }


    public void servicCallStatus() {
        UserModel userModel = new UserModel();

        userModel.setPhone(edtPhoneNumber.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_GET_OTP,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(Forgot2Fragment.newInstance(edtPhoneNumber.getStringTrimmed()), false);

                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message,"Error",getContext());
                                }
                            }
                        });


    }
}