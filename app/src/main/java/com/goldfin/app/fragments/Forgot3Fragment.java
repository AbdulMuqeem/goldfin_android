package com.goldfin.app.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.helperclasses.validator.PasswordValidation;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;
import com.google.gson.internal.LinkedTreeMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.AppConstants.MOBILE_MASK;


public class Forgot3Fragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtMobileNum)
    AnyEditTextView edtMobileNum;
    @BindView(R.id.edtPasswordsignup)
    AnyEditTextView edtPassword1;
    @BindView(R.id.imgHide1)
    CheckBox imgHide1;
    @BindView(R.id.btnSearch)
    AnyTextView btnSearch;
//    @BindView(R.id.edtMobileNum)
//    AnyEditTextView edtMobileNum;
//    @BindView(R.id.edtNewPassword)
//    AnyEditTextView edtNewPassword;
//    @BindView(R.id.btnSearch)
//    AnyTextView btnSearch;

    public static Forgot3Fragment newInstance() {
        Bundle args = new Bundle();
        Forgot3Fragment fragment = new Forgot3Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_forgotpass3;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtMobileNum.addTextChangedListener(new MaskFormatter(MOBILE_MASK, edtMobileNum, '-'));
        edtPassword1.addValidator(new PasswordValidation());

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }


    @Override
    public void setListeners() {

        edtMobileNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 3) {
                    edtMobileNum.setText("+92");
                    edtMobileNum.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        imgHide1.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                edtPassword1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                edtPassword1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            edtPassword1.setSelection(edtPassword1.getText().length());
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSearch)
    public void onViewClicked() {


        if (edtPassword1.testValidity()
                && edtMobileNum.testValidity()) {

            servicCallStatus();
        }
    }


    public void servicCallStatus() {
        UserModel userModel = new UserModel();

        userModel.setPhone(edtMobileNum.getStringTrimmed());
        userModel.setPassword(edtPassword1.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_SET_NEW_PASSWORD,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                emptyBackStack();
                                UIHelper.showShortToastInCenter(getBaseActivity(), ((LinkedTreeMap) webResponse.result).get("message").toString());
                                getBaseActivity().replaceDockableFragment(SignInUpFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message, "Error", getContext());
                                }
                            }
                        });


    }
}