package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.adapters.PhoneAdapter;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.fragments.dialogs.PhoneContactDialog;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.helperclasses.validator.RunTimePermissions1;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.ContactsModel;
import com.goldfin.app.models.wrappers.WebResponse;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class GeneralInfoFragment extends BaseFragment implements OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.imgContact)
    ImageView imgContact;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.rvPhone)
    RecyclerView rvPhone;
    PhoneAdapter phoneAdapter;
    ArrayList<ContactsModel> arrayList;
    ArrayList<String> arrName;
    ArrayList<String> arrPhone;
    boolean isBusinessUser;
    @BindView(R.id.checkbox1)
    CheckBox checkbox1;
    @BindView(R.id.checkbox2)
    CheckBox checkbox2;
    @BindView(R.id.contContact)
    RelativeLayout contContact;
    private boolean isYes;

    //
    public static GeneralInfoFragment newInstance(boolean isBusinessUser) {
        Bundle args = new Bundle();
        GeneralInfoFragment fragment = new GeneralInfoFragment();
        fragment.setArguments(args);
        fragment.isBusinessUser = isBusinessUser;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_general_info_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        arrName = new ArrayList<>();
        arrPhone = new ArrayList<>();
        phoneAdapter = new PhoneAdapter(getBaseActivity(), arrayList, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvPhone.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvPhone.getItemAnimator()).setSupportsChangeAnimations(false);
        rvPhone.setAdapter(phoneAdapter);
        phoneAdapter.notifyDataSetChanged();


    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isYes = true;
                    checkbox2.setChecked(false);
                }
            }
        });
        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    checkbox1.setChecked(false);
            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {


        ContactsModel contactsModel = new ContactsModel();
        contactsModel.setId(getCurrentUser().getId());
        contactsModel.setName(arrName);
        contactsModel.setPhone(arrPhone);
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectContact(contactsModel, WebServiceConstants.METHOD_ADDTOINVITE,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(DocumentUploadingFragment.newInstance(false, isBusinessUser, null), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @OnClick({R.id.contContact, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contContact:

                if (!RunTimePermissions1.isAllPhonePermissionGiven(getContext(), getBaseActivity(),
                        true, getResources().getText(R.string.app_name).toString())) {
                    return;
                }

                if (isYes)
                    if (arrayList.size() < 5)
                        genericDialog();
                    else
                        UIHelper.showAlertDialog("You can only add upto 5 contacts", "Alert", getBaseActivity());
                else UIHelper.showAlertDialog("Please check yes or no", "Alert", getBaseActivity());

                break;
            case R.id.btn_next:
                for (int i = 0; i < arrayList.size(); i++) {
                    arrName.add(arrayList.get(i).getName());
                    arrPhone.add(arrayList.get(i).getPhoneNumber());
                }
                serviceCall();
                break;
        }
    }

    private void genericDialog() {
        PhoneContactDialog genericPopupDialog = PhoneContactDialog.newInstance(getBaseActivity(), new OnItemClickListener() {
            @Override
            public void onItemClick(int position, Object object) {
                ContactsModel contactsModel = (ContactsModel) object;
                arrayList.add(new ContactsModel(contactsModel.getName(), contactsModel.getPhoneNumber()));
                phoneAdapter.notifyDataSetChanged();
                bindView();
            }
        });

        genericPopupDialog.setCancelable(true);
        genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
    }

    @Override
    public void onItemClick(int position, Object object) {
        if (object instanceof ContactsModel) {

        }

    }
}