package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.goldfin.R;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class HomeFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.contRegisteration)
    RoundKornerLinearLayout contRegisteration;
    @BindView(R.id.contCalculate)
    RoundKornerLinearLayout contCalculate;
    @BindView(R.id.txtLink)
    AnyTextView txtLink;


    //
    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
            titleBar.setVisibility(View.VISIBLE);
            titleBar.showBackButton(getBaseActivity());


    }

    @Override
    public void setListeners() {


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.contRegisteration, R.id.contCalculate, R.id.txtLink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contRegisteration:
                getBaseActivity().addDockableFragment(SignInUpFragment.newInstance(), false);
                break;
            case R.id.contCalculate:
                getBaseActivity().addDockableFragment(CalculatorFragment.newInstance(false), false);
                break;
            case R.id.txtLink:
                getBaseActivity().addDockableFragment(FaqsFragment.newInstance(), false);
                break;
        }
    }
}