package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class LoanApplicationStep1Fragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.startup_tv_heading1)
    TextView startupTvHeading1;
    @BindView(R.id.contEmployee)
    LinearLayout contEmployee;
    @BindView(R.id.contBusiness)
    LinearLayout contBusiness;
    @BindView(R.id.contSmallBusiness)
    LinearLayout contSmallBusiness;
    @BindView(R.id.contAgriculture)
    LinearLayout contAgriculture;

    //
    public static LoanApplicationStep1Fragment newInstance() {
        Bundle args = new Bundle();
        LoanApplicationStep1Fragment fragment = new LoanApplicationStep1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_loan_app_form_one;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contAgriculture.setVisibility(View.GONE);
        contSmallBusiness.setVisibility(View.GONE);
        if (getCurrentUser() != null) {
            if (getCurrentUser().getType().equalsIgnoreCase("customer") &&
                    getCurrentUser().getProfession() != null) {
                if (getCurrentUser().getProfession().equalsIgnoreCase("Business")) {
                    contEmployee.setEnabled(false);
                    contBusiness.setEnabled(true);
                } else {
                    contEmployee.setEnabled(true);
                    contBusiness.setEnabled(false);
                }
            }
        } else {
            contEmployee.setEnabled(true);
            contBusiness.setEnabled(true);
        }

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.contEmployee, R.id.contBusiness, R.id.contSmallBusiness, R.id.contAgriculture})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contEmployee:
                getBaseActivity().addDockableFragment(LoanApplicationStep2Fragment.newInstance(false), false);

                break;
            case R.id.contBusiness:
                contAgriculture.setVisibility(View.VISIBLE);
                contSmallBusiness.setVisibility(View.VISIBLE);
                break;
            case R.id.contSmallBusiness:
            case R.id.contAgriculture:
                getBaseActivity().addDockableFragment(LoanApplicationStep2Fragment.newInstance(true), false);

//                UIHelper.showToast(getContext(), "Will be implemented in future");
                break;
        }
    }
}