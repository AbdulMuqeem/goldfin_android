package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.goldfin.R;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class LoanApplicationStep2Fragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.startup_tv_heading1)
    TextView startupTvHeading1;
    @BindView(R.id.btnBtnPersonalInfo)
    RelativeLayout btnBtnPersonalInfo;
    @BindView(R.id.loancalc_fb_next)
    FloatingActionButton loancalcFbNext;
    private boolean isFromBusiness;

    //
    public static LoanApplicationStep2Fragment newInstance(boolean isFromBusiness) {
        Bundle args = new Bundle();
        LoanApplicationStep2Fragment fragment = new LoanApplicationStep2Fragment();
        fragment.setArguments(args);
        fragment.isFromBusiness = isFromBusiness;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_loan_app_form_two;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.btnBtnPersonalInfo, R.id.contProessional, R.id.contFinancial})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnBtnPersonalInfo:
                getBaseActivity().addDockableFragment(PersonalInfoFragment.newInstance(isFromBusiness, false, null), false);
                break;
            case R.id.contProessional:
                if (getCurrentUser() != null && getCurrentUser().getType().equalsIgnoreCase("customer")
                        && getCurrentUser().getPersonalInformation().equalsIgnoreCase("true")) {
                    if (isFromBusiness)
                        getBaseActivity().addDockableFragment(BusinessInfoFragment.newInstance(false, null), false);
                    else
                        getBaseActivity().addDockableFragment(ProfessionalInfoFragment.newInstance(false, null), false);
                } else
                    UIHelper.showAlertDialog("Please fill personal information", "Alert", getBaseActivity());
                break;
            case R.id.contFinancial:
                if (getCurrentUser() != null && getCurrentUser().getType().equalsIgnoreCase("customer")
                        && getCurrentUser().getProfessionalInformation().equalsIgnoreCase("true")) {
                    if (isFromBusiness)
                        getBaseActivity().addDockableFragment(BusinessFinancialInfoFragment.newInstance(false, null), false);
                    else
                        getBaseActivity().addDockableFragment(FinancialInfoFragment.newInstance(false, null), false);

                } else
                    UIHelper.showAlertDialog("Please fill professional information", "Alert", getBaseActivity());
                break;
        }
    }

}