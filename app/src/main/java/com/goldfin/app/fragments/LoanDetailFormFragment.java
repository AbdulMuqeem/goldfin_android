package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class LoanDetailFormFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtLoanAccNo)
    AnyEditTextView edtLoanAccNo;
    @BindView(R.id.edtMobileNum)
    AnyEditTextView edtMobileNum;
    @BindView(R.id.edtPaket1)
    AnyEditTextView edtPaket1;
    @BindView(R.id.edtPaket2)
    AnyEditTextView edtPaket2;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.edtLoanAmount)
    AnyEditTextView edtLoanAmount;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.edtMarketRate)
    AnyEditTextView edtMarketRate;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.edtTotalMarkup)
    AnyEditTextView edtTotalMarkup;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.edtProcessingFee)
    AnyEditTextView edtProcessingFee;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.edtPeriod)
    AnyEditTextView edtPeriod;
    @BindView(R.id.tv6)
    TextView tv6;
    @BindView(R.id.edtLoanDistuForm)
    AnyEditTextView edtLoanDistuForm;
    @BindView(R.id.tv7)
    TextView tv7;
    @BindView(R.id.edtGoldRate)
    AnyEditTextView edtGoldRate;
    @BindView(R.id.tv8)
    TextView tv8;
    @BindView(R.id.edtCollateral)
    AnyEditTextView edtCollateral;
    @BindView(R.id.tv9)
    TextView tv9;
    @BindView(R.id.tv10)
    TextView tv10;
    @BindView(R.id.edtCurrentCoverage)
    AnyEditTextView edtCurrentCoverage;
    @BindView(R.id.edtMaturityDate)
    AnyEditTextView edtMaturityDate;
    @BindView(R.id.tv11)
    TextView tv11;
    @BindView(R.id.edtShroffName)
    AnyEditTextView edtShroffName;
    @BindView(R.id.tv12)
    TextView tv12;
    @BindView(R.id.edtName)
    AnyEditTextView edtName;
    @BindView(R.id.tv13)
    TextView tv13;
    @BindView(R.id.edtUserCode)
    AnyEditTextView edtUserCode;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.btn_next)
    Button btnNext;

    //
    public static LoanDetailFormFragment newInstance() {
        Bundle args = new Bundle();
        LoanDetailFormFragment fragment = new LoanDetailFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_loan_detail_summary;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        CMOModel cmoModel = new CMOModel();

        cmoModel.setCustomerId("");
        cmoModel.setCMOId(getCurrentUser().getId());
        cmoModel.setFormId("");
        cmoModel.setShroffName(edtShroffName.getStringTrimmed());
        cmoModel.setLoanAccountNumber(edtLoanAccNo.getStringTrimmed());
        cmoModel.setMobileAccountNumber(edtMobileNum.getStringTrimmed());
        cmoModel.setSecuredPacket1Number(edtPaket1.getStringTrimmed());
        cmoModel.setSecuredPacket2Number(edtPaket2.getStringTrimmed());
        cmoModel.setLoanAmount(edtLoanAmount.getStringTrimmed());
        cmoModel.setMarkupRate(edtMarketRate.getStringTrimmed());

        cmoModel.setTotalMarkup(edtTotalMarkup.getStringTrimmed());
        cmoModel.setProcessingFee(edtProcessingFee.getStringTrimmed());
        cmoModel.setPeriod(edtPeriod.getStringTrimmed());

        cmoModel.setGoldRate(edtGoldRate.getStringTrimmed());
        cmoModel.setCollateralValue(edtCollateral.getStringTrimmed());
        cmoModel.setLoanMaturityDate(edtMaturityDate.getStringTrimmed());

        cmoModel.setLoanDisbursementDate(edtLoanDistuForm.getStringTrimmed());
        cmoModel.setCurrentCoverage(edtCurrentCoverage.getStringTrimmed());


        cmoModel.setUserCode(edtUserCode.getStringTrimmed());
        cmoModel.setName(edtName.getStringTrimmed());
        cmoModel.setDate(txtDate.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectCMO(cmoModel, WebServiceConstants.METHOD_LOANDETAILSUMMARY,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(LoanStatusFragment.newInstance(), false);
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }



    @OnClick({R.id.txtDate, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                break;
            case R.id.btn_next:
                serviceCall();
                break;
        }
    }
}