package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.fragments.dialogs.GenericPopupDialog;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.LaonStatusModel;
import com.goldfin.app.models.wrappers.WebResponse;

import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class LoanStatusFragment extends BaseFragment {


    Unbinder unbinder;

    //
    public static LoanStatusFragment newInstance() {
        Bundle args = new Bundle();
        LoanStatusFragment fragment = new LoanStatusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_loan_status;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        servicCallStatus();
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void servicCallStatus() {

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_LOAN_STATUS + getCurrentUser().getId() + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                LaonStatusModel formStatusModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), LaonStatusModel.class);
                                GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), formStatusModel.getLoanStatus(),
                                        formStatusModel.getMessage(), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getBaseActivity().addDockableFragment(FinishThankyouFragment.newInstance(), false);

                                            }
                                        });
                                genericPopupDialog.setCancelable(false);
                                genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);


                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        servicCallStatus();
    }
}