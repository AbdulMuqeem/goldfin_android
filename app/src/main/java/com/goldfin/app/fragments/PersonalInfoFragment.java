package com.goldfin.app.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.DateHelper;
import com.goldfin.app.helperclasses.GooglePlaceHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.libraries.location.GpsTracker;
import com.goldfin.app.managers.DateManager;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class PersonalInfoFragment extends BaseFragment
        implements GooglePlaceHelper.GooglePlaceDataInterface {


    Unbinder unbinder;
    @BindView(R.id.spLoan)
    Spinner spLoan;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.edtFatherName)
    AnyEditTextView edtFatherName;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.edtCurrentAdd)
    AnyTextView edtCurrentAdd;
    @BindView(R.id.spResidentialAddress)
    Spinner spResidentialAddress;
    @BindView(R.id.edtYears)
    AnyEditTextView edtYears;
    @BindView(R.id.spGender)
    Spinner spGender;
    @BindView(R.id.spMaritialStatus)
    Spinner spMaritialStatus;
    @BindView(R.id.spEducation)
    Spinner spEducation;
    @BindView(R.id.edtLoanAmount)
    AnyEditTextView edtLoanAmount;
    GooglePlaceHelper googlePlaceHelper;
    @BindView(R.id.edtPermanentEmployerAdress)
    AnyEditTextView edtPermanentEmployerAdress;
    private GoogleMap mMap;
    private UserTypeEnum userTypeEnum;
    SupportMapFragment mapFragment;
    private String city = "", martialStatus = "", purpose = "", residentStatus = "", gender = "", education = "";
    private boolean isFromBusiness, isFromSearch;
    private String currAddress = "";
    private double longitude, latitude;
    private UserAppointmentsModel userAppointmentsModel;

    public static PersonalInfoFragment newInstance(boolean isFromBusiness, boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        fragment.setArguments(args);
        fragment.isFromBusiness = isFromBusiness;
        fragment.userAppointmentsModel = userAppointmentsModel;
        fragment.isFromSearch = isFromSearch;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_personal_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googlePlaceHelper = new GooglePlaceHelper(getBaseActivity(), 0, this, PersonalInfoFragment.this);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFromSearch) {
            serviceCallGetData();
        } else if (getCurrentUser() != null && getCurrentUser().getPersonalInformation().equalsIgnoreCase("true")) {
            serviceCallGetData();
        } else {
            txtDate.setText(DateHelper.getCurrentDate());
        }
        setMap();
        try {
            getLocation();
        } catch (IOException e) {
        }


    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                education = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                gender = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spResidentialAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                residentStatus = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spMaritialStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                martialStatus = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                purpose = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.txtDate, R.id.btn_submit, R.id.edtCurrentAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                setDate();
                break;

            case R.id.btn_submit:
                if (isFromSearch) serviceCall();
                else {
                    if (!purpose.equalsIgnoreCase("") && !city.equalsIgnoreCase("") &&
                            edtFatherName.testValidity() && edtYears.testValidity() &&
                            edtPermanentEmployerAdress.testValidity() &&
                            !residentStatus.equalsIgnoreCase("") && edtYears.testValidity() &&
                            !gender.equalsIgnoreCase("") &&
                            !martialStatus.equalsIgnoreCase("") && !education.equalsIgnoreCase("")) {

                        serviceCall();

                    } else UIHelper.showToast(getContext(), "Please fill all required fields");
                }
                break;
            case R.id.edtCurrentAdd:
                callPlaceAutocompleteActivityIntent();
                break;
        }
    }


    private void serviceCall() {
        FormSendingDataModel formSendingDataModel = new FormSendingDataModel();
        formSendingDataModel.setId(getCurrentUser().getId());
        formSendingDataModel.setPurpose(purpose);
        formSendingDataModel.setCity(city);
        formSendingDataModel.setFname(edtFatherName.getStringTrimmed());
        formSendingDataModel.setPermanentaddress(edtPermanentEmployerAdress.getStringTrimmed());
        formSendingDataModel.setDob(txtDate.getStringTrimmed());
        formSendingDataModel.setCurrentaddress(edtCurrentAdd.getStringTrimmed());
        formSendingDataModel.setResidentialstatus(residentStatus);
        formSendingDataModel.setYears(edtYears.getStringTrimmed());
        formSendingDataModel.setGender(gender);
        formSendingDataModel.setMaritalstatus(martialStatus);
        formSendingDataModel.setEducation(education);
        formSendingDataModel.setLoanAmountDesired(edtLoanAmount.getStringTrimmed());

        String methodName;
        if (isFromBusiness) {
            methodName = WebServiceConstants.METHOD_BUSINESS_PERSONALINFO;
        } else methodName = WebServiceConstants.METHOD_PERSONALINFO;

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectForm(formSendingDataModel, methodName,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                if (isFromBusiness)
                                    getBaseActivity().addDockableFragment(BusinessInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                else
                                    getBaseActivity().addDockableFragment(ProfessionalInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void setDate() {
        DateManager.showDatePicker(getContext(), txtDate, AppConstants.ACCESS_LOG_DATE_SHOW, (datePicker, i, i1, i2) -> {
            final Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, i);
            myCalendar.set(Calendar.MONTH, i1);
            myCalendar.set(Calendar.DAY_OF_MONTH, i2);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            String myFormat = AppConstants.ACCESS_LOG_DATE_SEND; // In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        }, false, null);
    }

    private void bindData(FormSendingDataModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
                enableInputs();
                break;
            case CSO:
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false")) {
                    enableInputs();
                } else {
                    if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Deprecated"))
                        enableInputs();
                    else
                        disableInputs();
                }

                break;
        }


        if (customerData != null) {
            if (customerData.getLoanAmountDesired() != null)
                edtLoanAmount.setText(customerData.getLoanAmountDesired());

            if (customerData.getPurpose() != null) {
                purpose = (customerData.getPurpose());
                spLoan.setSelection(((ArrayAdapter) spLoan.getAdapter()).getPosition(purpose));
            }

            if (customerData.getCity() != null) {
                city = (customerData.getCity());
                spCity.setSelection(((ArrayAdapter) spCity.getAdapter()).getPosition(city));
            }

            if (customerData.getFname() != null)
                edtFatherName.setText(customerData.getFname());

            if (customerData.getDob() != null)
                txtDate.setText(customerData.getDob());

            if (customerData.getCurrentaddress() != null)
                edtCurrentAdd.setText(customerData.getCurrentaddress());

            if (customerData.getPermanentaddress() != null)
                edtPermanentEmployerAdress.setText(customerData.getPermanentaddress());

            if (customerData.getResidentialstatus() != null) {
                residentStatus = (customerData.getResidentialstatus());
                spResidentialAddress.setSelection(((ArrayAdapter) spResidentialAddress.getAdapter()).getPosition(residentStatus));
            }

            if (customerData.getYears() != null) ;
            edtYears.setText(customerData.getYears());

            if (customerData.getGender() != null) {
                gender = (customerData.getGender());
                spGender.setSelection(((ArrayAdapter) spGender.getAdapter()).getPosition(gender));
            }


            if (customerData.getMaritalstatus() != null) {
                martialStatus = (customerData.getMaritalstatus());
                spMaritialStatus.setSelection(((ArrayAdapter) spMaritialStatus.getAdapter()).getPosition(martialStatus));
            }
//
            if (customerData.getEducation() != null) {
                education = (customerData.getEducation());
                spEducation.setSelection(((ArrayAdapter) spEducation.getAdapter()).getPosition(education));
            }
        }
    }

    private void enableInputs() {
        edtCurrentAdd.setEnabled(true);
        edtFatherName.setEnabled(true);
        edtLoanAmount.setEnabled(true);
        edtYears.setEnabled(true);
        txtDate.setEnabled(true);
        spEducation.setEnabled(true);
        spCity.setEnabled(true);
        spGender.setEnabled(true);
        spLoan.setEnabled(true);
        spMaritialStatus.setEnabled(true);
        spResidentialAddress.setEnabled(true);
        edtPermanentEmployerAdress.setEnabled(true);
    }

    private void disableInputs() {
        edtCurrentAdd.setEnabled(false);
        edtFatherName.setEnabled(false);
        edtLoanAmount.setEnabled(false);
        edtYears.setEnabled(false);
        txtDate.setEnabled(false);
        spEducation.setEnabled(false);
        spCity.setEnabled(false);
        spGender.setEnabled(false);
        spLoan.setEnabled(false);
        spMaritialStatus.setEnabled(false);
        spResidentialAddress.setEnabled(false);
        edtPermanentEmployerAdress.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (userAppointmentsModel.getUserId() != null)
                id = userAppointmentsModel.getUserId();
            else id = userAppointmentsModel.getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_LAF1 + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    /*

    MAP
    *
    * */

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5656;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                String locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                setMap();
                edtCurrentAdd.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getBaseActivity(), "AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getBaseActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(/*Location location*/double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getBaseActivity(), Locale.getDefault());
        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        if (addresses != null && addresses.get(0) != null)
            currAddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        edtCurrentAdd.setText(currAddress);
    }


    public void getLocation() throws IOException {
        GpsTracker gpsTracker = new GpsTracker(getBaseActivity());
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            testCurr(latitude, longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    public void onPlaceActivityResult(double longitude, double latitude, String locationName) {

    }

    @Override
    public void onError(String error) {

    }

    private void setMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                googleMap.getUiSettings().setZoomGesturesEnabled(true);


                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .title(edtCurrentAdd.getStringTrimmed())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
                        longitude), 10));


            }
        });
    }

}