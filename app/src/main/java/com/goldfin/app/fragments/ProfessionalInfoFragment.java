package com.goldfin.app.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.DateHelper;
import com.goldfin.app.helperclasses.GooglePlaceHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.libraries.location.GpsTracker;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.DateManager;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.goldfin.app.constatnts.AppConstants.MOBILE_MASK;


public class ProfessionalInfoFragment extends BaseFragment implements GooglePlaceHelper.GooglePlaceDataInterface {


    Unbinder unbinder;
    @BindView(R.id.spProession)
    Spinner spProession;
    @BindView(R.id.edtDesignation)
    AnyEditTextView edtDesignation;
    @BindView(R.id.spJobStatus)
    Spinner spJobStatus;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.edtWExp)
    AnyEditTextView edtWExp;
    @BindView(R.id.edtEmployerName)
    AnyEditTextView edtEmployerName;
    @BindView(R.id.txtEmployerAdress)
    AnyTextView txtEmployerAdress;
    @BindView(R.id.edtEmployerPhone)
    AnyEditTextView edtEmployerPhone;
    @BindView(R.id.edtWebsite)
    AnyEditTextView edtWebsite;
    @BindView(R.id.edtLandline)
    AnyEditTextView edtLandline;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.edtPermanentEmployerAdress)
    AnyEditTextView edtPermanentEmployerAdress;
    private String jobStatus = "", profession = "";
    private double longitude, latitude;
    private String currAddress;
    GooglePlaceHelper googlePlaceHelper;
    private UserTypeEnum userTypeEnum;
    private boolean isFromSearch;
    private UserAppointmentsModel userAppointmentsModel;

    public static ProfessionalInfoFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        ProfessionalInfoFragment fragment = new ProfessionalInfoFragment();
        fragment.setArguments(args);
        fragment.isFromSearch = isFromSearch;
        fragment.userAppointmentsModel = userAppointmentsModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_professional_info_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googlePlaceHelper = new GooglePlaceHelper(getBaseActivity(), 0, this, ProfessionalInfoFragment.this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFromSearch) serviceCallGetData();
        else if (getCurrentUser() != null && getCurrentUser().getProfessionalInformation().equalsIgnoreCase("true")) {
            serviceCallGetData();
        } else txtDate.setText(DateHelper.getCurrentDate());

        setMap();
//        try {
//            getLocation();
//        } catch (IOException e) {
//        }

        edtEmployerPhone.addTextChangedListener(new MaskFormatter(MOBILE_MASK, edtEmployerPhone, '-'));

//        edtEmployerPhone
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
        spJobStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                jobStatus = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spProession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                profession = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        edtEmployerPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 3) {
                    edtEmployerPhone.setText("+92");
                    edtEmployerPhone.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        FormSendingDataModel formSendingDataModel = new FormSendingDataModel();
        formSendingDataModel.setId(getCurrentUser().getId());

        formSendingDataModel.setProfession(profession);
        formSendingDataModel.setJobstatus(jobStatus);
        formSendingDataModel.setDesignation(edtDesignation.getStringTrimmed());
        formSendingDataModel.setPermanentaddress(edtPermanentEmployerAdress.getStringTrimmed());
        formSendingDataModel.setEmpname(edtEmployerName.getStringTrimmed());
        formSendingDataModel.setEmpaddress(txtEmployerAdress.getStringTrimmed());
        formSendingDataModel.setPhone(edtEmployerPhone.getStringTrimmed());
        formSendingDataModel.setTotalExperince(edtWExp.getStringTrimmed());
        formSendingDataModel.setDateofjoining(txtDate.getStringTrimmed());
        formSendingDataModel.setWebsite(edtWebsite.getStringTrimmed());
        formSendingDataModel.setAddressapp(edtPermanentEmployerAdress.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectForm(formSendingDataModel, WebServiceConstants.METHOD_PROESSIONALINFO,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(FinancialInfoFragment.newInstance(isFromSearch, userAppointmentsModel), false);
                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void setDate() {
        DateManager.showDatePicker(getContext(), txtDate, AppConstants.ACCESS_LOG_DATE_SHOW, (datePicker, i, i1, i2) -> {
            final Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, i);
            myCalendar.set(Calendar.MONTH, i1);
            myCalendar.set(Calendar.DAY_OF_MONTH, i2);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            String myFormat = AppConstants.ACCESS_LOG_DATE_SEND; // In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        }, false, null);
    }


    @OnClick({R.id.txtDate, R.id.btnNext, R.id.txtEmployerAdress})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtDate:
                setDate();
                break;
            case R.id.txtEmployerAdress:
                callPlaceAutocompleteActivityIntent();
                break;
            case R.id.btnNext:
                if (isFromSearch) serviceCall();
                else {
                    if ((!profession.equalsIgnoreCase("") &&
                            edtDesignation.testValidity() &&
                            !jobStatus.equalsIgnoreCase("") &&
                            edtWExp.testValidity() && edtEmployerName.testValidity() && edtEmployerPhone.testValidity() &&
                            !txtEmployerAdress.getStringTrimmed().equalsIgnoreCase("") &&
                            edtWebsite.testValidity() &&
                            edtPermanentEmployerAdress.testValidity() &&
                            edtEmployerPhone.testValidity() && edtWExp.testValidity())) {

                        serviceCall();
                    } else UIHelper.showToast(getContext(), "Please fill all required fields");
                }
                break;
        }
    }

    private void bindData(FormSendingDataModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
                break;
            case CSO:
                if (userAppointmentsModel.getBo() != null && userAppointmentsModel.getBo().equalsIgnoreCase("false")) {
                    enableInputs();
                } else {
                    if (userAppointmentsModel.getLoanStatus().equalsIgnoreCase("Deprecated"))
                        enableInputs();
                    else
                        disableInputs();
                }
                break;
        }


        if (customerData != null) {
            if (customerData.getPhone() != null)
                edtEmployerPhone.setText(customerData.getPhone());

            if (customerData.getProfession() != null) {
                profession = (customerData.getProfession());
                spProession.setSelection(((ArrayAdapter) spProession.getAdapter()).getPosition(profession));
            }


            if (customerData.getJobstatus() != null) {
                jobStatus = (customerData.getJobstatus());
                spJobStatus.setSelection(((ArrayAdapter) spJobStatus.getAdapter()).getPosition(jobStatus));
            }

            if (customerData.getDateofjoining() != null)
                txtDate.setText(customerData.getDateofjoining());
            if (customerData.getAddressapp() != null)
                edtPermanentEmployerAdress.setText(customerData.getAddressapp());


            if (customerData.getEmpname() != null)
                edtEmployerName.setText(customerData.getEmpname());


            if (customerData.getDesignation() != null)
                edtDesignation.setText(customerData.getDesignation());

            if (customerData.getLandline() != null)
                edtLandline.setText(customerData.getLandline());

            if (customerData.getPermanentaddress() != null)
                edtPermanentEmployerAdress.setText(customerData.getPermanentaddress());
            if (customerData.getEmpaddress() != null)
                txtEmployerAdress.setText(customerData.getEmpaddress());

            if (customerData.getWebsite() != null)
                edtWebsite.setText(customerData.getWebsite());

            if (customerData.getTotalExperince() != null)
                edtWExp.setText(customerData.getTotalExperince());

        }
    }

    private void enableInputs() {
        edtEmployerPhone.setEnabled(true);
        edtDesignation.setEnabled(true);
        edtEmployerName.setEnabled(true);
        edtLandline.setEnabled(true);
        txtDate.setEnabled(true);
        edtWebsite.setEnabled(true);
        edtWExp.setEnabled(true);
        spJobStatus.setEnabled(true);
        spProession.setEnabled(true);
        edtPermanentEmployerAdress.setEnabled(true);
    }

    private void disableInputs() {
        edtEmployerPhone.setEnabled(false);
        edtDesignation.setEnabled(false);
        edtEmployerName.setEnabled(false);
        edtLandline.setEnabled(false);
        txtDate.setEnabled(false);
        edtWebsite.setEnabled(false);
        edtWExp.setEnabled(false);
        spJobStatus.setEnabled(false);
        spProession.setEnabled(false);
        edtPermanentEmployerAdress.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (isFromSearch) {
            if (sharedPreferenceManager.getCustomerData().getUserId() != null)
                id = sharedPreferenceManager.getCustomerData().getUserId();
            else id = sharedPreferenceManager.getCustomerData().getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_LAF2 + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }



      /*

    MAP
    *
    * */

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5656;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                String locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                setMap();
                txtEmployerAdress.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getBaseActivity(), "AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getBaseActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(/*Location location*/double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getBaseActivity(), Locale.getDefault());
        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        currAddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        txtEmployerAdress.setText(currAddress);

//        latitude = location.getLatitude();
//        longitude = location.getLongitude();
    }


    public void getLocation() throws IOException {
        GpsTracker gpsTracker = new GpsTracker(getBaseActivity());
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            testCurr(latitude, longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    public void onPlaceActivityResult(double longitude, double latitude, String locationName) {

    }

    @Override
    public void onError(String error) {

    }

    SupportMapFragment mapFragment;

    private void setMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                googleMap.getUiSettings().setZoomGesturesEnabled(true);


                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .title(txtEmployerAdress.getStringTrimmed())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
                        longitude), 10));


            }
        });
    }
}