package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.fragments.dialogs.NewUserPopupDialog;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.helperclasses.validator.CnicValidation;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.AppConstants.CNIC_MASK;


public class SearchFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtCNIC)
    AnyEditTextView edtCNIC;
    @BindView(R.id.btnSearch)
    AnyTextView btnSearch;
    @BindView(R.id.btnData)
    AnyTextView btnData;
    private UserTypeEnum userTypeEnum;

    public static SearchFragment newInstance() {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_search;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtCNIC.addTextChangedListener(new MaskFormatter(CNIC_MASK, edtCNIC, '-'));
        edtCNIC.addValidator(new CnicValidation());

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {

            case CUSTOMER:
            case CMO:
                btnData.setVisibility(View.GONE);
                break;
            case VO:
            case CSO:
            case BO:
                btnData.setVisibility(View.VISIBLE);
                break;
        }

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void servicCallStatus() {

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_FILLED_DATA_CNIC + edtCNIC.getStringTrimmed() + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UserAppointmentsModel userAppointmentsModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserAppointmentsModel.class);
                                sharedPreferenceManager.putObject("KEY_CUSTOMER_DATA", userAppointmentsModel);

                                if (userAppointmentsModel.getUserProfession() != null && userAppointmentsModel.getUserProfession().equalsIgnoreCase("Salaried")) {
                                    getBaseActivity().addDockableFragment(PersonalInfoFragment.newInstance(false, true, userAppointmentsModel), false);
                                } else {
                                    getBaseActivity().addDockableFragment(PersonalInfoFragment.newInstance(true, true, userAppointmentsModel), false);
                                }


//
                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    if (getCurrentUser().getType().equalsIgnoreCase("cmo")) {
                                        if (message.equalsIgnoreCase("No User Found with enter CNIC.")) {
                                            NewUserPopupDialog genericPopupDialog = NewUserPopupDialog.newInstance(getBaseActivity(), "", "", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    getBaseActivity().addDockableFragment(CreateNewUserFragment.newInstance(), false);

                                                }
                                            });
                                            genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                                        } else {
                                            UIHelper.showAlertDialog(message, "Error", getContext());
                                        }

                                    } else {
                                        UIHelper.showAlertDialog(message, "Error", getContext());
                                    }
                                }
                            }
                        });


    }

    @OnClick({R.id.btnSearch, R.id.btnData})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                if (edtCNIC.testValidity())
                    servicCallStatus();
                break;
            case R.id.btnData:
                getBaseActivity().addDockableFragment(UserAppointmentFragment.newInstance(), false);
                break;
        }
    }
}