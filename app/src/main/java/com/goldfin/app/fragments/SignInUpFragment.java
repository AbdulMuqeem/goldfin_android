package com.goldfin.app.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.helperclasses.validator.CnicValidation;
import com.goldfin.app.helperclasses.validator.MobileNumberValidation;
import com.goldfin.app.helperclasses.validator.PasswordValidation;
import com.goldfin.app.libraries.maskformatter.MaskFormatter;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.goldfin.app.widget.AnyTextView;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.goldfin.app.constatnts.AppConstants.CNIC_MASK;
import static com.goldfin.app.constatnts.AppConstants.MOBILE_MASK;


public class SignInUpFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.btnSignIn)
    LinearLayout btnSignIn;
    @BindView(R.id.btnSignUp)
    LinearLayout btnSignUp;
    @BindView(R.id.edtUserName)
    AnyEditTextView edtUserName;
    @BindView(R.id.edtPassword)
    AnyEditTextView edtPassword;
    @BindView(R.id.imgHide)
    CheckBox imgHide;
    @BindView(R.id.txtSignIn)
    AnyTextView txtSignIn;
    @BindView(R.id.contParentSignIn)
    RoundKornerLinearLayout contParentSignIn;
    @BindView(R.id.edtFullName)
    AnyEditTextView edtFullName;
    @BindView(R.id.edtCNIC)
    AnyEditTextView edtCNIC;
    @BindView(R.id.edtMobileNum)
    AnyEditTextView edtMobileNum;
    @BindView(R.id.edtEmail)
    AnyEditTextView edtEmail;
    @BindView(R.id.edtUserNameSignUp)
    AnyEditTextView edtUserName2;
    @BindView(R.id.edtPasswordsignup)
    AnyEditTextView edtPassword1;
    @BindView(R.id.imgHide1)
    CheckBox imgHide1;
    @BindView(R.id.txtSignUp)
    AnyTextView txtSignUp;
    @BindView(R.id.txtForgotPass)
    AnyTextView txtForgotPass;
    @BindView(R.id.contParentSignUp)
    RoundKornerLinearLayout contParentSignUp;
    @BindView(R.id.cbAcceptTerms)
    CheckBox cbAcceptTerms;
    @BindView(R.id.txtAccept)
    AnyTextView txtAccept;

    private UserTypeEnum userTypeEnum;
    private boolean termsCondition;

    //
    public static SignInUpFragment newInstance() {
        Bundle args = new Bundle();
        SignInUpFragment fragment = new SignInUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_signin_signup;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtPassword1.addValidator(new PasswordValidation());
        edtCNIC.addValidator(new CnicValidation());
        edtMobileNum.addValidator(new MobileNumberValidation());
        contParentSignIn.setVisibility(View.VISIBLE);
        contParentSignUp.setVisibility(View.GONE);
        btnSignIn.setBackgroundColor(getResources().getColor(R.color.selectedgray));
        btnSignUp.setBackgroundColor(getResources().getColor(R.color.unselected));
        edtCNIC.addTextChangedListener(new MaskFormatter(CNIC_MASK, edtCNIC, '-'));
        edtMobileNum.addTextChangedListener(new MaskFormatter(MOBILE_MASK, edtMobileNum, '-'));

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());

    }

    @Override
    public void setListeners() {

        imgHide.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            edtPassword.setSelection(edtPassword.getText().length());
        });
        imgHide1.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                edtPassword1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                edtPassword1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            edtPassword1.setSelection(edtPassword1.getText().length());
        });

        cbAcceptTerms.setOnCheckedChangeListener((compoundButton, b) -> {
//            if (b) {
                termsCondition = b;
//            } else {
//            }
        });


        edtMobileNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 3) {
                    edtMobileNum.setText("+92");
                    edtMobileNum.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnSignIn, R.id.btnSignUp, R.id.txtForgotPass, R.id.txtSignIn, R.id.txtSignUp, R.id.txtAccept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                contParentSignIn.setVisibility(View.VISIBLE);
                contParentSignUp.setVisibility(View.GONE);
                btnSignIn.setBackgroundColor(getResources().getColor(R.color.selectedgray));
                btnSignUp.setBackgroundColor(getResources().getColor(R.color.unselected));
                txtForgotPass.setVisibility(View.VISIBLE);

                break;
            case R.id.btnSignUp:
                contParentSignUp.setVisibility(View.VISIBLE);
                btnSignUp.setBackgroundColor(getResources().getColor(R.color.selectedgray));
                btnSignIn.setBackgroundColor(getResources().getColor(R.color.unselected));
                contParentSignIn.setVisibility(View.GONE);
                txtForgotPass.setVisibility(View.GONE);

                break;
            case R.id.txtForgotPass:
                getBaseActivity().addDockableFragment(Forgot1Fragment.newInstance(), false);

                break;
            case R.id.txtSignIn:
                if (edtUserName.testValidity() && edtPassword.testValidity())
                    serviceCallSignIn();
                break;
            case R.id.txtAccept:
                getBaseActivity().addDockableFragment(TandCFragment.newInstance(), false);
                break;
            case R.id.txtSignUp:
//                if (termsCondition) {
                if (edtFullName.testValidity()
                        && edtUserName2.testValidity()
                        && edtCNIC.testValidity()
                        && edtPassword1.testValidity()
                        && edtMobileNum.testValidity()
                        && edtMobileNum.testValidity()
                        && edtEmail.testValidity()) {
                    if (termsCondition) {
                        serviceCallSignUp();
                    } else UIHelper.showAlertDialog("Please agree on Terms and Condition", "Alert", getContext());
                }

//                else
//                    UIHelper.showToast(getContext(), "Please fill all data");


        break;
    }

}

    private void serviceCallSignUp() {

        UserModel userModel = new UserModel();
        userModel.setUserName(edtUserName2.getStringTrimmed());
        userModel.setFullname(edtFullName.getStringTrimmed());
        userModel.setPassword(edtPassword1.getStringTrimmed());
        userModel.setCnic(edtCNIC.getStringTrimmed());
        userModel.setPhone(edtMobileNum.getStringTrimmed());
        userModel.setEmail(edtEmail.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_SIGNUP,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                FormSendingDataModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormSendingDataModel.class);
                                String userID = formSendingDataModel.getId();
                                getBaseActivity().replaceDockableFragment(SignUpOTPFragment.newInstance(userID), false);

                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message, "Error", getContext());
                                }
                            }
                        });
    }

    private void serviceCallSignIn() {
        UserModel userModel = new UserModel();

        userModel.setPassword(edtPassword.getStringTrimmed());
        userModel.setCnic(edtUserName.getStringTrimmed());
        userModel.setUserName(edtUserName.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_LOGIN,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);
                                sharedPreferenceManager.putObject("KEY_CURRENT_USER_MODEL", userModel);
                                userTypeEnum = UserTypeEnum.fromCanonicalForm(userModel.getType());
                                switch (userTypeEnum) {

                                    case CUSTOMER:
                                        emptyBackStack();
                                        if (userModel.getFormDone().equalsIgnoreCase("true")) {
                                            getBaseActivity().replaceDockableFragment(LoanStatusFragment.newInstance(), false);

                                        } else {

                                            getBaseActivity().replaceDockableFragment(VisitPlanFragment.newInstance(), false);
                                        }
                                        break;
                                    case CMO:
                                    case CSO:
                                    case VO:
                                    case BO:
//                                        emptyBackStack();
                                        getBaseActivity().replaceDockableFragment(SearchFragment.newInstance(), false);

                                        break;
                                }


                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message, "Error", getContext());
                                }
                            }
                        });


    }

}