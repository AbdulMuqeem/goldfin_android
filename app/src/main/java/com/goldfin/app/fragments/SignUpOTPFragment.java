package com.goldfin.app.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.KeyboardHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyTextView;
import com.goldfin.app.widget.PinEntryEditText;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SignUpOTPFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.txtPinCode)
    PinEntryEditText txtPinCode;
    @BindView(R.id.btnSearch)
    AnyTextView btnSearch;
    @BindView(R.id.mTextField)
    AnyTextView mTextField;
    @BindView(R.id.txtResentCode)
    AnyTextView txtResentCode;
    private CountDownTimer countDownTimer;
    private String phoneNumber;

    public static SignUpOTPFragment newInstance(String phoneNumber) {
        Bundle args = new Bundle();
        SignUpOTPFragment fragment = new SignUpOTPFragment();
        fragment.phoneNumber = phoneNumber;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_otp_signup;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        edtCNIC.addTextChangedListener(new MaskFormatter(CNIC_MASK, edtCNIC, '-'));
//        edtCNIC.addValidator(new CnicValidation());

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mTextField != null) {
                    mTextField.setText("" + millisUntilFinished / 1000);

                    String hms = String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                    mTextField.setText(hms);//set text
                }
            }

            public void onFinish() {
                if (txtResentCode != null){
                    txtResentCode.setTextColor(getResources().getColor(R.color.c_white));
                txtResentCode.setEnabled(true);}


            }
        }.start();

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        KeyboardHelper.showSoftKeyboard(getContext(), txtPinCode);
    }

    @Override
    public void setListeners() {


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void servicCallStatus() {
        UserModel userModel = new UserModel();

        userModel.setUserId(phoneNumber);
        userModel.setVerifyCode(txtPinCode.getText().toString());
        KeyboardHelper.hideSoftKeyboard(getContext(), txtPinCode);
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_ENTER_SIGNUP_OTP_CODE,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().addDockableFragment(ThankyouFragment.newInstance(), false);


                            }

                            @Override
                            public void onError(Object object) {
                                if (object instanceof String) {
                                    String message = (String) object.toString();
                                    UIHelper.showAlertDialog(message, "Error", getContext());
                                }
                            }
                        });


    }

    @OnClick({R.id.btnSearch, R.id.txtResentCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                servicCallStatus();
                break;
            case R.id.txtResentCode:
                countDownTimer.start();
                UserModel userModel = new UserModel();
                userModel.setPhone(phoneNumber);

                new WebServices(getBaseActivity(),
                        "", true, WebServiceConstants.BASE_URL, true)
                        .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_GET_OTP,
//
                                new WebServices.IRequestWebResponseAnyObjectCallBack() {
                                    @Override
                                    public void requestDataResponse(WebResponse<Object> webResponse) {

                                    }

                                    @Override
                                    public void onError(Object object) {
                                    }
                                });
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}