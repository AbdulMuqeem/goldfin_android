package com.goldfin.app.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;


import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyTextView;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;


public class TandCFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.txtAbout)
    AnyTextView txtAbout;


    public static TandCFragment newInstance() {
        Bundle args = new Bundle();
        TandCFragment fragment = new TandCFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_about;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        txtAbout.setText(about);
        serviceCallGetData();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View v) {

    }

    private void serviceCallGetData() {
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_TC,
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                text1
                               String html =  ((LinkedTreeMap) ((ArrayList) webResponse.result).get(0)).get("html").toString();
//                                        webResponse.result.equals("text1");
                                txtAbout.setText(Html.fromHtml(html));
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

}
