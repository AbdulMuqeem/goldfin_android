package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.clans.fab.FloatingActionButton;
import com.goldfin.R;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.validator.RunTimePermissions1;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class TutorialFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    //
    public static TutorialFragment newInstance() {
        Bundle args = new Bundle();
        TutorialFragment fragment = new TutorialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_tutorial1;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        sharedPreferenceManager.setFirstTime(true);
        if (!RunTimePermissions1.isAllPhonePermissionGiven(getContext(), getBaseActivity(),
                true, getResources().getText(R.string.app_name).toString())) {
            return;
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);

    }


    @Override
    public void setListeners() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBaseActivity().addDockableFragment(TutorialNextFragment.newInstance(), false);
            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}