package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.goldfin.R;
import com.goldfin.app.adapters.recyleradapters.UserAppointmentsAdapter;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyTextView;
import com.google.common.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class UserAppointmentFragment extends BaseFragment implements OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.rvPhone)
    RecyclerView rvPhone;
    UserAppointmentsAdapter adapUserApoint;
    ArrayList<UserAppointmentsModel> arrayList;
    @BindView(R.id.faqs)
    AnyTextView faqs;
    private UserTypeEnum userTypeEnum;

    public static UserAppointmentFragment newInstance() {
        Bundle args = new Bundle();
        UserAppointmentFragment fragment = new UserAppointmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.fragment_faqs;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        adapUserApoint = new UserAppointmentsAdapter(getBaseActivity(), arrayList, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        faqs.setText("User Appointments");
        bindView();
        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case CSO:
                servicCallForm(WebServiceConstants.METHOD_GET_FORM_CMO_CSO);
                break;
            case VO:
                servicCallForm(WebServiceConstants.METHOD_GET_FORM_VO);
                break;
            case BO:
                servicCallForm(WebServiceConstants.METHOD_GET_FORM_BO);
                break;
        }


    }

    private void servicCallForm(String methodGetFormBo) {
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + methodGetFormBo,
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<UserAppointmentsModel>>() {
                                }.getType();
                                ArrayList<UserAppointmentsModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                arrayList.clear();
                                arrayList.addAll(arrayList1);
                                adapUserApoint.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvPhone.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvPhone.getItemAnimator()).setSupportsChangeAnimations(false);
        rvPhone.setAdapter(adapUserApoint);
        adapUserApoint.notifyDataSetChanged();


    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onItemClick(int position, Object object) {
        if (object instanceof UserAppointmentsModel) {
            UserAppointmentsModel userAppointmentsModel = (UserAppointmentsModel) object;
            sharedPreferenceManager.putObject("KEY_CUSTOMER_DATA", userAppointmentsModel);
            getBaseActivity().addDockableFragment(DashboardBOVOCSOFragment.newInstance(userAppointmentsModel), false);
        }
    }
}