package com.goldfin.app.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;
import com.google.common.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;


public class VerificationFormFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.edtPersonalInfo)
    AnyEditTextView edtPersonalInfo;
    @BindView(R.id.edtProfessionalInfo)
    AnyEditTextView edtProfessionalInfo;
    @BindView(R.id.edtFinanciallInfo)
    AnyEditTextView edtFinanciallInfo;
    @BindView(R.id.edtNeigResiInfo)
    AnyEditTextView edtNeigResiInfo;
    @BindView(R.id.edtNeigBusinessInfo)
    AnyEditTextView edtNeigBusinessInfo;
    //    @BindView(R.id.spIfNeg)
//    Spinner spIfNeg;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.edtRemarkPersonalInfo)
    AnyEditTextView edtRemarkPersonalInfo;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.edtRemarkProfInfo)
    AnyEditTextView edtRemarkProfInfo;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.edtRemarkFinancial)
    AnyEditTextView edtRemarkFinancial;
    @BindView(R.id.btnCustomers)
    Button btnCustomers;
    @BindView(R.id.btnBusiness)
    Button btnBusiness;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.edtVerification)
    AnyEditTextView edtVerification;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.edtIfNeg)
    AnyEditTextView edtIfNeg;
    private String inNega = "";
    private UserTypeEnum userTypeEnum;
    private UserAppointmentsModel userAppointmentsModel;
    private boolean isFromSearch;
    @BindView(R.id.txtPack1)
    TextView txtPack1;
    @BindView(R.id.viewP1)
    View viewP1;
    @BindView(R.id.txtPack2)
    TextView txtPack2;
    @BindView(R.id.viewPack2)
    View viewPack2;
    String attachmentType = "";
    private File filePacket1;
    private File filePacket2;

    //
    public static VerificationFormFragment newInstance(boolean isFromSearch, UserAppointmentsModel userAppointmentsModel) {
        Bundle args = new Bundle();
        VerificationFormFragment fragment = new VerificationFormFragment();
        fragment.setArguments(args);
        fragment.userAppointmentsModel = userAppointmentsModel;
        fragment.isFromSearch = isFromSearch;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_verification_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getCurrentUser().getType().equalsIgnoreCase("vo")) {
            btnNext.setText("Submit");
            isFromSearch = false;
            serviceCallGetData();
        }
        if (isFromSearch) {
            btnNext.setText("Back");
            if (userAppointmentsModel != null && userAppointmentsModel.getVoDone().equalsIgnoreCase("true")) {
                serviceCallGetData();
            }
        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
//        spIfNeg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                inNega = (String) adapterView.getSelectedItem();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {

        String id;
        if (userAppointmentsModel.getUserId() != null)
            id = userAppointmentsModel.getUserId();
        else id = userAppointmentsModel.getForm_id();
        CMOModel cmoModel = new CMOModel();

        cmoModel.setCustomerId(id);
        cmoModel.setVOId(getCurrentUser().getId());
        cmoModel.setFormId(sharedPreferenceManager.getCustomerData().getForm_id());

        cmoModel.setPersonalInformation(edtPersonalInfo.getStringTrimmed());
        cmoModel.setProfessionalInformation(edtProfessionalInfo.getStringTrimmed());
        cmoModel.setFinancialInformation(edtFinanciallInfo.getStringTrimmed());
        cmoModel.setNeighborcheckResidence(edtNeigResiInfo.getStringTrimmed());
        cmoModel.setNeighborcheckBusiness(edtNeigBusinessInfo.getStringTrimmed());
        cmoModel.setNegativeReason(edtIfNeg.getStringTrimmed());
        cmoModel.setRemarksFinancialInformation(edtRemarkFinancial.getStringTrimmed());
        cmoModel.setRemarksPersonalInformation(edtRemarkPersonalInfo.getStringTrimmed());
        cmoModel.setRemarksProfessionalInformation(edtRemarkProfInfo.getStringTrimmed());
        cmoModel.setVerificationResults(edtVerification.getStringTrimmed());


        if (filePacket1 != null)
            cmoModel.setPicture1(filePacket1.getPath());
        else {
            UIHelper.showToast(getContext(), "Please attach picture");
            return;
        }
        if (filePacket2 != null)
            cmoModel.setPicture2(filePacket2.getPath());
        else {
            UIHelper.showToast(getContext(), "Please attach picture");
            return;
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectCMO(cmoModel, WebServiceConstants.METHOD_VOFORM,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                emptyBackStack();
                                getBaseActivity().addDockableFragment(SearchFragment.newInstance(), false);
                                UIHelper.showToast(getContext(), "Form Submitted");

                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @OnClick({R.id.btnCustomers, R.id.btnBusiness, R.id.btn_next, R.id.txtPack1, R.id.txtPack2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCustomers:
                break;
            case R.id.btnBusiness:
                break;
            case R.id.btn_next:
                if (isFromSearch) popBackStack();
                else serviceCall();
                break;
            case R.id.txtPack1:
                attachmentType = AppConstants.ATTACHMENT_PICTURE_1;
                cropImagePicker();
                break;

            case R.id.txtPack2:
                attachmentType = AppConstants.ATTACHMENT_PICTURE_2;
                cropImagePicker();
                break;
        }
    }


    private void cropImagePicker() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMinCropWindowSize(192, 192)
                .setMinCropResultSize(192, 192)
                .setMultiTouchEnabled(false)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                // FIXME: 15-Jul-17 Fix Quality if required
                .setRequestedSize(640, 640, CropImageView.RequestSizeOptions.RESIZE_FIT)
                .setOutputCompressQuality(80)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                switch (attachmentType) {

                    case AppConstants.ATTACHMENT_PICTURE_1:
                        filePacket1 = new File(result.getUri().getPath());
                        break;
                    case AppConstants.ATTACHMENT_PICTURE_2:
                        filePacket2 = new File(result.getUri().getPath());
                        break;


                    default:
                        UIHelper.showShortToastInCenter(getContext(), "Attachment type undefined");

                }

                setImageAfterResult(result.getUri().toString());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }


    private void setAndUploadImage(String uploadFilePath) throws IOException {
        switch (attachmentType) {
            case AppConstants.ATTACHMENT_PICTURE_1:
                viewP1.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));
                break;
            case AppConstants.ATTACHMENT_PICTURE_2:
                viewPack2.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

                break;


        }
    }

    private void setImageAfterResult(final String uploadFilePath) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    setAndUploadImage(uploadFilePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /*getServiceCall*/

    private void bindData(CMOModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case BO:
            case CSO:
                disableInputs();
                break;
            case VO:
                enableInputs();
                break;
        }


        if (customerData.getRemarksFinancialInformation() != null)
            edtRemarkFinancial.setText(customerData.getRemarksFinancialInformation());
        if (customerData.getRemarksPersonalInformation() != null)
            edtRemarkPersonalInfo.setText(customerData.getRemarksPersonalInformation());
        if (customerData.getRemarksProfessionalInformation() != null)
            edtRemarkProfInfo.setText(customerData.getRemarksProfessionalInformation());
        if (customerData.getVerificationResults() != null)
            edtVerification.setText(customerData.getVerificationResults());


        if (customerData.getPersonalInformation() != null) {
            edtPersonalInfo.setText(customerData.getPersonalInformation());
        }
        if (customerData.getNegativeReason() != null) {
            edtIfNeg.setText(customerData.getNegativeReason());
        }

        if (customerData.getFinancialInformation() != null) {
            edtFinanciallInfo.setText(customerData.getFinancialInformation());
        }
        if (customerData.getNeighborcheckResidence() != null) {
            edtNeigResiInfo.setText(customerData.getNeighborcheckResidence());
        }
        if (customerData.getNeighborcheckBusiness() != null) {
            edtNeigBusinessInfo.setText(customerData.getNeighborcheckBusiness());
        }
        if (customerData.getProfessionalInformation() != null) {
            edtProfessionalInfo.setText(customerData.getProfessionalInformation());
        }
        if (customerData.getPicture1() != null) {
            viewP1.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

        }
        if (customerData.getPicture2() != null) {
            viewPack2.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 10f));

        }
    }

    private void enableInputs() {
        edtRemarkFinancial.setEnabled(true);
        edtRemarkPersonalInfo.setEnabled(true);
        edtRemarkProfInfo.setEnabled(true);
        edtVerification.setEnabled(true);

        edtFinanciallInfo.setEnabled(true);
        edtPersonalInfo.setEnabled(true);
        edtProfessionalInfo.setEnabled(true);
        edtNeigBusinessInfo.setEnabled(true);
        edtNeigResiInfo.setEnabled(true);

    }

    private void disableInputs() {
        edtRemarkFinancial.setEnabled(false);
        edtRemarkPersonalInfo.setEnabled(false);
        edtRemarkProfInfo.setEnabled(false);
        edtVerification.setEnabled(false);
        edtFinanciallInfo.setEnabled(false);
        edtPersonalInfo.setEnabled(false);
        edtProfessionalInfo.setEnabled(false);
        edtNeigBusinessInfo.setEnabled(false);
        edtNeigResiInfo.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (userAppointmentsModel.getUserId() != null)
            id = userAppointmentsModel.getUserId();
        else id = userAppointmentsModel.getForm_id();
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL +
                WebServiceConstants.METHOD_GET_VO_FORM + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<CMOModel>>() {
                                }.getType();
                                ArrayList<CMOModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                if (arrayList1 != null && arrayList1.size() > 0) {
                                    CMOModel formSendingDataModel = arrayList1.get(0);
                                    bindData(formSendingDataModel);
                                }
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }
}