/*
package com.goldfin.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.goldfin.R;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.enums.UserTypeEnum;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyEditTextView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class VerificationFormFragment1 extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.spPersonalInfo)
    Spinner spPersonalInfo;
    @BindView(R.id.spProfInfo)
    Spinner spProfInfo;
    @BindView(R.id.spFinanInfo)
    Spinner spFinanInfo;
    @BindView(R.id.spNeigResid)
    Spinner spNeigResid;
    @BindView(R.id.spNeigBusin)
    Spinner spNeigBusin;
    @BindView(R.id.spIfNeg)
    Spinner spIfNeg;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.edtRemarkPersonalInfo)
    AnyEditTextView edtRemarkPersonalInfo;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.edtRemarkProfInfo)
    AnyEditTextView edtRemarkProfInfo;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.edtRemarkFinancial)
    AnyEditTextView edtRemarkFinancial;
    @BindView(R.id.btnCustomers)
    Button btnCustomers;
    @BindView(R.id.btnBusiness)
    Button btnBusiness;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.edtVerification)
    AnyEditTextView edtVerification;
    @BindView(R.id.btn_next)
    Button btnNext;
    private String neigBusiness = "", neigResi = "", financial = "", personalInfo = "", professionalInfo = "", inNega = "";
    private UserTypeEnum userTypeEnum;

    //
    public static VerificationFormFragment1 newInstance() {
        Bundle args = new Bundle();
        VerificationFormFragment1 fragment = new VerificationFormFragment1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {

        return R.layout.activity_verification_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        if (getCurrentUser().getCmo().equalsIgnoreCase("true")) {
//            serviceCallGetData();
//        }
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {
        spFinanInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                financial = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spIfNeg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inNega = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spNeigBusin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                neigBusiness = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spNeigResid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                neigResi = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spPersonalInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                personalInfo = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spProfInfo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                professionalInfo = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        CMOModel cmoModel = new CMOModel();

        cmoModel.setCustomerId("");
        cmoModel.setCMOId(getCurrentUser().getId());

        cmoModel.setPersonalInformation(personalInfo);
        cmoModel.setProfessionalInformation(professionalInfo);
        cmoModel.setFinancialInformation(financial);
        cmoModel.setNeighborcheckResidence(neigResi);
        cmoModel.setNeighborcheckBusiness(neigBusiness);
        cmoModel.setNegativeReason(neigResi);
        cmoModel.setRemarksFinancialInformation(edtRemarkFinancial.getStringTrimmed());
        cmoModel.setRemarksPersonalInformation(edtRemarkPersonalInfo.getStringTrimmed());
        cmoModel.setRemarksProfessionalInformation(edtRemarkProfInfo.getStringTrimmed());
        cmoModel.setVerificationResults(edtVerification.getStringTrimmed());

        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL, true)
                .webServiceRequestAPIAnyObjectCMO(cmoModel, WebServiceConstants.METHOD_VOFORM,
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UserTypeEnum userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
                                switch (userTypeEnum) {

                                    case CMO:
                                    case VO:
                                    case CSO:
                                        getBaseActivity().addDockableFragment(FinishThankyouFragment.newInstance(), false);
                                        break;
                                    case BO:
                                        getBaseActivity().addDockableFragment(LoanDetailFormFragment.newInstance(), false);

                                        break;
                                }


//                                servicCallFormSatus();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @OnClick({R.id.btnCustomers, R.id.btnBusiness, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCustomers:
                break;
            case R.id.btnBusiness:
                break;
            case R.id.btn_next:
                serviceCall();
                break;
        }
    }



    */
/*getServiceCall*//*


    private void bindData(CMOModel customerData) {

        userTypeEnum = UserTypeEnum.fromCanonicalForm(getCurrentUser().getType());
        switch (userTypeEnum) {
            case CUSTOMER:
            case CMO:
            case VO:
                disableInputs();
                break;
            case BO:
            case CSO:
                enableInputs();
                break;
        }


        if (customerData.getRemarksFinancialInformation() != null)
            edtRemarkFinancial.setText(customerData.getRemarksFinancialInformation());
        if (customerData.getRemarksPersonalInformation() != null)
            edtRemarkPersonalInfo.setText(customerData.getRemarksPersonalInformation());
        if (customerData.getRemarksProfessionalInformation() != null)
            edtRemarkProfInfo.setText(customerData.getRemarksProfessionalInformation());
        if (customerData.getVerificationResults() != null)
            edtVerification.setText(customerData.getVerificationResults());


        if (customerData.getPersonalInformation() != null) {
            personalInfo = (customerData.getPersonalInformation());
            spPersonalInfo.setSelection(((ArrayAdapter) spPersonalInfo.getAdapter()).getPosition(personalInfo));
        }

        if (customerData.getFinancialInformation() != null) {
            financial = (customerData.getFinancialInformation());
            spFinanInfo.setSelection(((ArrayAdapter) spFinanInfo.getAdapter()).getPosition(financial));
        }
        if (customerData.getNeighborcheckResidence() != null) {
            neigResi = (customerData.getNeighborcheckResidence());
            spNeigResid.setSelection(((ArrayAdapter) spNeigResid.getAdapter()).getPosition(neigResi));
        }
        if (customerData.getNeighborcheckBusiness() != null) {
            neigBusiness = (customerData.getNeighborcheckBusiness());
            spNeigBusin.setSelection(((ArrayAdapter) spNeigBusin.getAdapter()).getPosition(neigBusiness));
        }
        if (customerData.getProfessionalInformation() != null) {
            professionalInfo = (customerData.getProfessionalInformation());
            spProfInfo.setSelection(((ArrayAdapter) spProfInfo.getAdapter()).getPosition(professionalInfo));
        }
    }

    private void enableInputs() {
        edtRemarkFinancial.setEnabled(true);
        edtRemarkPersonalInfo.setEnabled(true);
        edtRemarkProfInfo.setEnabled(true);
        edtVerification.setEnabled(true);

        spProfInfo.setEnabled(true);
        spNeigResid.setEnabled(true);
        spNeigBusin.setEnabled(true);
        spPersonalInfo.setEnabled(true);
        spFinanInfo.setEnabled(true);

    }

    private void disableInputs() {
        edtRemarkFinancial.setEnabled(false);
        edtRemarkPersonalInfo.setEnabled(false);
        edtRemarkProfInfo.setEnabled(false);
        edtVerification.setEnabled(false);
        spProfInfo.setEnabled(false);
        spNeigResid.setEnabled(false);
        spNeigBusin.setEnabled(false);
        spPersonalInfo.setEnabled(false);
        spFinanInfo.setEnabled(false);
    }

    private void serviceCallGetData() {
        String id;
        if (sharedPreferenceManager.getCustomerData()!= null) {
            id = sharedPreferenceManager.getCustomerData().getForm_id();
        } else {
            id = getCurrentUser().getId();
        }
        new WebServices(getBaseActivity(),
                "", true, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_CMO_FORM + id + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                CMOModel formSendingDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), CMOModel.class);

                                bindData(formSendingDataModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }
}*/
