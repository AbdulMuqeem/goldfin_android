package com.goldfin.app.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TimePicker;

import com.github.clans.fab.FloatingActionButton;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.goldfin.R;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.abstracts.BaseFragment;
import com.goldfin.app.helperclasses.DateHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.managers.DateManager;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.wrappers.WebResponse;
import com.goldfin.app.widget.AnyTextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class VisitPlanFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.txtDate)
    AnyTextView txtDate;
    @BindView(R.id.contDate)
    RoundKornerLinearLayout contDate;
    @BindView(R.id.txtTime)
    AnyTextView txtTime;
    @BindView(R.id.conTime)
    RoundKornerLinearLayout conTime;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private String selectedStartDate;
    private long selectedStartDateMillis;


    //
    public static VisitPlanFragment newInstance() {
        Bundle args = new Bundle();
        VisitPlanFragment fragment = new VisitPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_visitplan;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getCurrentUser() != null && getCurrentUser().getPlanVisitData() != null && getCurrentUser().getPlanVisitData().getDate() != null &&
                sharedPreferenceManager.getCurrentUser().getPlanVisitData().getTime() != null) {
            txtDate.setText(getCurrentUser().getPlanVisitData().getDate());
            txtTime.setText(getCurrentUser().getPlanVisitData().getTime());
        } else {
            txtTime.setText(DateHelper.getCurrentTime());
            txtDate.setText(DateHelper.getCurrentDate());
        }

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
//        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {
        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {


                                       if (getCurrentUser()!=null&&getCurrentUser().getPlanVisitData() != null && getCurrentUser().getPlanVisitData().getDate() != null && txtDate.getStringTrimmed().equalsIgnoreCase(getCurrentUser().getPlanVisitData().getDate()) &&
                                               getCurrentUser().getPlanVisitData().getTime() != null && txtTime.getStringTrimmed().equalsIgnoreCase(getCurrentUser().getPlanVisitData().getTime())) {
                                           getBaseActivity().addDockableFragment(LoanApplicationStep1Fragment.newInstance(), false);

                                       } else {
                                           UserModel userModel = new UserModel();
                                           userModel.setId(getCurrentUser().getId());
                                           userModel.setTime(txtTime.getStringTrimmed());
                                           userModel.setDate(txtDate.getStringTrimmed());


                                           new WebServices(getBaseActivity(),
                                                   "", true, WebServiceConstants.BASE_URL, true)
                                                   .webServiceRequestAPIAnyObjectG(userModel, WebServiceConstants.METHOD_VISITPLAN,
//
                                                           new WebServices.IRequestWebResponseAnyObjectCallBack() {
                                                               @Override
                                                               public void requestDataResponse(WebResponse<Object> webResponse) {
                                                                   getBaseActivity().addDockableFragment(LoanApplicationStep1Fragment.newInstance(), false);

                                                               }

                                                               @Override
                                                               public void onError(Object object) {
                                                               }
                                                           });
                                       }
                                   }
                               }
        );

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.contDate, R.id.conTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contDate:
                setDate();
                break;
            case R.id.conTime:
                timePicker();
                break;
        }
    }

    private void timePicker() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getBaseActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm = "";

                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, minute);

                if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                    am_pm = "AM";
                else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                    am_pm = "PM";

                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";

                txtTime.setText(strHrsToShow + ":" + selectedMinute + " " + am_pm);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private void setDate() {
        DateManager.showDatePicker(getContext(), txtDate, AppConstants.ACCESS_LOG_DATE_SHOW, (datePicker, i, i1, i2) -> {
            final Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, i);
            myCalendar.set(Calendar.MONTH, i1);
            myCalendar.set(Calendar.DAY_OF_MONTH, i2);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            String myFormat = AppConstants.ACCESS_LOG_DATE_SEND; // In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            selectedStartDate = sdf.format(myCalendar.getTime());
            selectedStartDateMillis = myCalendar.getTimeInMillis();
        }, true, null);
    }


}