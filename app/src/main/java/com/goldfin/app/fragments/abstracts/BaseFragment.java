package com.goldfin.app.fragments.abstracts;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.gdacciaro.iOSDialog.iOSDialogBuilder;

import com.goldfin.BaseApplication;

import com.goldfin.R;
import com.goldfin.app.activities.BaseActivity;
import com.goldfin.app.activities.MainActivity;
import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.fragments.SearchFragment;
import com.goldfin.app.helperclasses.ui.helper.KeyboardHelper;
import com.goldfin.app.helperclasses.ui.helper.TitleBar;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.goldfin.app.managers.SharedPreferenceManager;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.managers.retrofit.WebServices;
import com.goldfin.app.models.FormStatusModel;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.wrappers.WebResponse;

import io.reactivex.disposables.Disposable;

import static com.goldfin.app.constatnts.AppConstants.KEY_FORM_STATUS_MODEL;

public abstract class BaseFragment extends Fragment implements
        View.OnClickListener, AdapterView.OnItemClickListener/*, OnNewPacketReceivedListener */ {

    protected View view;
    public SharedPreferenceManager sharedPreferenceManager;
    public String TAG = "Logging Tag";
    public boolean onCreated = false;
    Disposable subscription;


    /**
     * This is an abstract class, we should inherit our fragment from this class
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(getContext());
        onCreated = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBaseActivity().getTitleBar().resetViews();

    }

    public UserModel getCurrentUser() {
        return sharedPreferenceManager.getCurrentUser();
    }


    // Use  UIHelper.showSpinnerDialog
    @Deprecated
    public void setSpinner(ArrayAdapter adaptSpinner, final TextView textView, final Spinner spinner) {
        if (adaptSpinner == null || spinner == null)
            return;
        //selected item will look like a spinner set from XML
//        simple_list_item_single_choice
        adaptSpinner.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adaptSpinner);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = spinner.getItemAtPosition(position).toString();
                if (textView != null)
                    textView.setText(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    protected abstract int getFragmentLayout();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    public void emptyBackStack() {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popBackStack() {
        if (getFragmentManager() == null) {
            return;
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void popStackTill(int stackNumber) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = stackNumber; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popStackTillReverse(int stackNumber) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < stackNumber; i++) {
            fm.popBackStack();
        }
    }

    public abstract void setTitlebar(TitleBar titleBar);


    public abstract void setListeners();

    @Override
    public void onResume() {
        super.onResume();
        onCreated = true;
        setListeners();

        if (getBaseActivity() != null) {
            setTitlebar(getBaseActivity().getTitleBar());
        }
//
        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
    }

    @Override
    public void onPause() {

        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
        super.onPause();
    }


    public void notifyToAll(int event, Object data) {
        BaseApplication.getPublishSubject().onNext(new Pair<>(event, data));
    }

//    protected void subscribeToNewPacket(final OnNewPacketReceivedListener newPacketReceivedListener) {
//        subscription = BaseApplication.getPublishSubject()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Pair>() {
//                    @Override
//                    public void accept(@NonNull Pair pair) throws Exception {
//                        Log.e("abc", "on accept");
//                        newPacketReceivedListener.onNewPacket((int) pair.first, pair.second);
//                    }
//                });
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        sharedPreferenceManager.clearDB();
        Log.e("abc", "onDestroyView");
        if (subscription != null)
            subscription.dispose();
    }


    public void showNextBuildToast() {
        UIHelper.showToast(getContext(), "This feature is in progress");
    }

    public static void logoutClick(final BaseFragment baseFragment) {
        Context context = baseFragment.getContext();

        new iOSDialogBuilder(context)
                .setTitle(context.getString(R.string.logout))
                .setSubtitle(context.getString(R.string.areYouSureToLogout))
                .setBoldPositiveLabel(false)
                .setCancelable(false)
                .setPositiveListener(context.getString(R.string.yes), dialog -> {
                    dialog.dismiss();
//                    baseFragment.sharedPreferenceManager.clearDB();
                    baseFragment.getBaseActivity().clearAllActivitiesExceptThis(MainActivity.class);
                })
                .setNegativeListener(context.getString(R.string.no), dialog -> dialog.dismiss())
                .build().show();


    }


    public void putOneTimeToken(String token) {
        sharedPreferenceManager.putValue(AppConstants.KEY_ONE_TIME_TOKEN, token);
    }

    public void servicCallFormSatus() {

        new WebServices(getBaseActivity(),
                "", false, WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_FORM_STATUS + getCurrentUser().getId() + "/",
                true)
                .webServiceRequestAPIAnyObjectGet(
//
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                FormStatusModel formStatusModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), FormStatusModel.class);
                                sharedPreferenceManager.putObject(KEY_FORM_STATUS_MODEL, formStatusModel);

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }
}

