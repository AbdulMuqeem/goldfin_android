package com.goldfin.app.fragments.dialogs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.goldfin.R;
import com.goldfin.app.activities.BaseActivity;
import com.goldfin.app.widget.AnyTextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by  on 21-Feb-17.
 */

public class GenericPopupDialog extends DialogFragment {


    Unbinder unbinder;
    @BindView(R.id.txtHeader)
    AnyTextView txtHeader;
    @BindView(R.id.btnCross)
    AnyTextView btnCross;
    @BindView(R.id.edtDynmaicText)
    AnyTextView edtDynmaicText;
    @BindView(R.id.btnOk)
    Button btnOk;
    private BaseActivity baseActivity;
    private String message;
    private String title;
    private View.OnClickListener onClickListener;

    public static GenericPopupDialog newInstance(BaseActivity baseActivity, String title, String message, View.OnClickListener onClickListener) {
        GenericPopupDialog frag = new GenericPopupDialog();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.baseActivity = baseActivity;
        frag.onClickListener = onClickListener;
        frag.title = title;
        frag.message = message;

        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popup_generic, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtHeader.setText(title);
        edtDynmaicText.setText(message);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnCross, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCross:
                dismiss();
                break;
            case R.id.btnOk:
                onClickListener.onClick(this.btnOk);
                dismiss();
                break;
        }
    }
}

