package com.goldfin.app.fragments.dialogs;


import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goldfin.R;
import com.goldfin.app.activities.BaseActivity;
import com.goldfin.app.adapters.PhoneAdapter;
import com.goldfin.app.callbacks.OnItemClickListener;
import com.goldfin.app.models.ContactsModel;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by  on 21-Feb-17.
 */

public class PhoneContactDialog extends DialogFragment implements OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.rvPhone)
    RecyclerView rvPhone;
    private BaseActivity baseActivity;
    private OnItemClickListener onClickListener;
    private String title;
    private String body;
    PhoneAdapter phoneAdapter;
    ArrayList<ContactsModel> arrayList;

    public static PhoneContactDialog newInstance(BaseActivity baseActivity, OnItemClickListener onItemClickListener) {
        PhoneContactDialog frag = new PhoneContactDialog();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.baseActivity = baseActivity;
//        frag.title = title;
//        frag.body = body;
        frag.onClickListener = onItemClickListener;

        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
        arrayList = new ArrayList<>();
        phoneAdapter = new PhoneAdapter(baseActivity, arrayList, this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popup_phone, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        readContactList();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void readContactList() {
        ContentResolver cr = baseActivity.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
//                        arrayList.clear();
                        arrayList.add(new ContactsModel(name, phoneNo));
//                        arrayList.addAll(arrayList);
                        phoneAdapter.notifyDataSetChanged();
//                        Log.i(TAG, "Name: " + name);
//                        Log.i(TAG, "Phone Number: " + phoneNo);
                    }
                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }


        if (arrayList.size() < 0) {
            return;
        } else bindView();
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(baseActivity);
        rvPhone.setLayoutManager(mLayoutManager);
        ((DefaultItemAnimator) rvPhone.getItemAnimator()).setSupportsChangeAnimations(false);
        rvPhone.setAdapter(phoneAdapter);
        phoneAdapter.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, Object object) {
        if (object instanceof ContactsModel) {

            ContactsModel contactsModel = (ContactsModel) object;
            onClickListener.onItemClick(position, contactsModel);
            dismiss();
        }

    }

}

