package com.goldfin.app.helperclasses;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.goldfin.R;

import java.util.Map;


import ru.bullyboo.encoder.Encoder;
import ru.bullyboo.encoder.methods.AES;


/**
 * Created by aqsa.sarwar on 6/28/2019.
 */

public class ImageLoaderHelper {
//    public static void loadImageWithConstantHeadersWithoutAnimation(Context context, ImageView imageView, String path) {
//        ImageLoader.getInstance().displayImage(ImageLoaderHelper.getUserImageURL(path),
//                imageView,
//                ImageLoaderHelper.getOptionsSimple(WebServiceConstants
//                        .getHeaders(SharedPreferenceManager.getInstance(context).getString(Encoder.BuilderAES()
//                                .method(AES.Method.AES_CBC_PKCS5PADDING)
//                                .message(AppConstants.KEY_TOKEN)
//                                .key("qetuoadgjlzcbm") // not necessary
//                                .keySize(AES.Key.SIZE_128) // not necessary
//                                .decrypt()))));
//    }
//
//    public static void loadImageWithConstantHeaders(Context context, ImageView imageView, String path) {
//        ImageLoader.getInstance().displayImage(ImageLoaderHelper.getUserImageURL(path),
//                imageView,
//                ImageLoaderHelper.getOptionsWithAnimation(WebServiceConstants
//                        .getHeaders(SharedPreferenceManager.getInstance(context).getString(Encoder.BuilderAES()
//                                .method(AES.Method.AES_CBC_PKCS5PADDING)
//                                .message(AppConstants.KEY_TOKEN)
//                                .key("qetuoadgjlzcbm") // not necessary
//                                .keySize(AES.Key.SIZE_128) // not necessary
//                                .decrypt()))));
//    }

    public static String getImageURL(String path, String requestMethod) {
        return Encoder.BuilderAES()
                .method(AES.Method.AES_CBC_PKCS5PADDING)
                .message("GETIMAGE_BASE_URL")
                .key("qetuoadgjlzcbm") // not necessary
                .keySize(AES.Key.SIZE_128) // not necessary
                .decrypt() + path + "&requestmethod=" + requestMethod;
    }

    private static String getUserImageURL(String path) {
        return Encoder.BuilderAES()
                .method(AES.Method.AES_CBC_PKCS5PADDING)
                .message("GETIMAGE_BASE_URL")
                .key("qetuoadgjlzcbm") // not necessary
                .keySize(AES.Key.SIZE_128) // not necessary
                .decrypt()

                + path + "&requestmethod="

                + Encoder.BuilderAES()
                .method(AES.Method.AES_CBC_PKCS5PADDING)
                .message("METHOD_USER_GET_USER_IMAGE")
                .key("qetuoadgjlzcbm") // not necessary
                .keySize(AES.Key.SIZE_128) // not necessary
                .decrypt();
    }

    public static DisplayImageOptions getOptionsSimple() {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .build();
    }


    public static DisplayImageOptions getOptionsSimple(Map<String, String> headers) {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .extraForDownloader(headers)
                .build();
    }

    public static DisplayImageOptions getOptionsWithAnimation() {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(500)).build();
    }


    public static DisplayImageOptions getOptionsWithAnimation(Map<String, String> headers) {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .extraForDownloader(headers)
                .imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(500)).build();
    }
}
