package com.goldfin.app.helperclasses.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.goldfin.R;


/**
 * Created by  on 02-Mar-17.
 */

public class TitleBar extends RelativeLayout {


    ImageView btnLeft1;
    LinearLayout containerTitlebar1;

    final Handler handler = new Handler();


    public TitleBar(Context context) {
        super(context);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }


    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.titlebar_main, this);
        bindViews();
    }

    private void bindViews() {
        btnLeft1 = findViewById(R.id.btnLeft1);
        containerTitlebar1 = findViewById(R.id.containerTitlebar1);

    }

    public void resetViews() {
        containerTitlebar1.setVisibility(VISIBLE);
        btnLeft1.setVisibility(INVISIBLE);

    }


    public void showBackButton(final Activity mActivity) {
        this.btnLeft1.setVisibility(VISIBLE);
        containerTitlebar1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null) {
//                    mActivity.getSupportFragmentManager().popBackStack();
                    mActivity.onBackPressed();
                }

            }
        });
    }


}
