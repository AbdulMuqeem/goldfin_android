package com.goldfin.app.helperclasses.validator;

import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;
import com.goldfin.app.widget.AnyEditTextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PasswordValidation extends Validator {

    private EditText edtConfirmPassword;

    public PasswordValidation() {
        super("Password must be greater than 7 Characters,should contain at least one digit, special char and upper case");
    }

    public PasswordValidation(EditText edtConfirmPassword) {
        super("Password not match");
        this.edtConfirmPassword = edtConfirmPassword;
    }

    @Override
    public boolean isValid(EditText et) {
        if (edtConfirmPassword != null) {
            return et.getText().toString().equals(edtConfirmPassword.getText().toString());
        } else {
            return isValidPassword(((AnyEditTextView) et).getStringTrimmed());
        }
    }

    /*
    *
    *
    *
    *   (/^
        (?=.*\d)                //should contain at least one digit
        (?=.*[@$%&#])           //should contain at least one special char
        (?=.*[A-Z])             //should contain at least one upper case
        [a-zA-Z0-9]{4,}         //should contain at least 8 from the mentioned characters
        $/)
    *
    *
    * */
    public boolean isValidPassword(final String password) {


        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

}
