package com.goldfin.app.helperclasses.validator;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import com.goldfin.app.activities.BaseActivity;

import androidx.core.app.ActivityCompat;

public class RunTimePermissions1 {


    // Storage Permissions variables
    private static final int REQUEST_CODE = 6361;
    private static String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CONTACTS
    };


    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
//        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int wifiPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_WIFI_STATE);
        int cameraPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int internetPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);
        int locationPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        int readContactPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        int phoneContactPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
        int readPhonePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
//
        if (
//                writePermission != PackageManager.PERMISSION_GRANTED
//                || readPermission != PackageManager.PERMISSION_GRANTED
                internetPermission != PackageManager.PERMISSION_GRANTED
                        || wifiPermission != PackageManager.PERMISSION_GRANTED
                        || cameraPermission != PackageManager.PERMISSION_GRANTED
                        || locationPermission != PackageManager.PERMISSION_GRANTED
                        || coarseLocation != PackageManager.PERMISSION_GRANTED
                        || readContactPermission != PackageManager.PERMISSION_GRANTED
                        || phoneContactPermission != PackageManager.PERMISSION_GRANTED
                        || readPhonePermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    REQUEST_CODE
            );
        }
    }


    public static boolean isAllPhonePermissionGiven(Context context, BaseActivity activity, boolean showAlert, String alertMessage) {

//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        if (showAlert) {

//            PermissionDialogFragment permissionDialogFragment = PermissionDialogFragment.newInstance();
//            permissionDialogFragment.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    permissionDialogFragment.dismiss();
            verifyStoragePermissions(activity);
//                }
//            });
//            permissionDialogFragment.show(activity.getSupportFragmentManager(), PermissionDialogFragment.class.getSimpleName());
//

        }


        return false;
    }
}
