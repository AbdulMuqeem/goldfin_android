package com.goldfin.app.libraries.searchdialog.core;

/**
 * Created by MADNESS on 5/14/2017.
 */

public interface OnPerformFilterListener {
    void doBeforeFiltering();

    void doAfterFiltering();
}
