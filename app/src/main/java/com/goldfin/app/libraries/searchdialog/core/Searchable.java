package com.goldfin.app.libraries.searchdialog.core;

public interface Searchable {
    String getTitle();

    String getContent();

    String getSearchTime();
}
