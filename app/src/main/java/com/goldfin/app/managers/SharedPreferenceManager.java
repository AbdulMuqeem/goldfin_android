package com.goldfin.app.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.goldfin.app.fragments.UserAppointmentFragment;
import com.goldfin.app.models.receivingmodel.UserAppointmentsModel;
import com.google.gson.Gson;

import com.goldfin.app.constatnts.AppConstants;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;

/**
 * Class that can be extended to make available simple preference
 * setter/getters.
 *
 * Should be extended to provide Facades.
 *
 */
public class SharedPreferenceManager {
    private static SharedPreferences pref;
    private static SharedPreferenceManager factory;

    public static SharedPreferenceManager getInstance(Context context) {
        if (pref == null)
            pref = context.getSharedPreferences("mypref", Context.MODE_PRIVATE);

        if (factory == null)
            factory = new SharedPreferenceManager();

        return factory;
    }

    public static void clearDB() {
        pref.edit().clear().commit();
    }

    public static void clearKey(String key) {
        pref.edit().remove(key).commit();
    }


    public void putValue(String key, Object value) {
        if (value instanceof Boolean)
            pref.edit().putBoolean(key, (Boolean) value).commit();
        else if (value instanceof String)
            pref.edit().putString(key, (String) value).commit();
        else if (value instanceof Integer)
            pref.edit().putInt(key, (int) value).commit();
        else if (value instanceof Long)
            pref.edit().putLong(key, (long) value).commit();

//        else
//            pref.edit().putString(key, (String) value).apply();
    }

    public int getInt(String key) {
        return pref.getInt(key, -1);
    }

    public long getLong(String key) {
        return pref.getLong(key, -1);
    }

    public String getString(String key) {
        return pref.getString(key, "");
    }

    public boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public void putObject(String key, Object object) {
        if (object == null || object.equals("")) {
            pref.edit().putString(key, (String) object).commit();
            return;
        }

        pref.edit().putString(key, new Gson().toJson(object)).commit();
    }

    public void removeObject(String key) {
        pref.edit().remove(key).commit();
    }

    public <T> T getObject(String key, Class<T> a) {
        String json = pref.getString(key, null);
        if (json == null) {
            return null;
        } else {
            try {
                return new Gson().fromJson(json, a);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object stored with key "
                        + key + " is instance of other class");
            }
        }
    }


    public UserModel getCurrentUser() {
        return getObject(AppConstants.KEY_CURRENT_USER_MODEL, UserModel.class);
    }


    public UserAppointmentsModel getCustomerData() {
        return getObject(AppConstants.KEY_CUSTOMER_DATA, UserAppointmentsModel.class);
    }

    public boolean isFirstTime() {
        return getBoolean(AppConstants.IS_FIRST_TIME);
    }

//    public void setFirstTime(boolean firstTime) {
//        putValue(AppConstants.IS_FIRST_TIME, firstTime);
//    }

}
