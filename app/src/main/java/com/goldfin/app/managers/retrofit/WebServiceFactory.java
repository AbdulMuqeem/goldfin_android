package com.goldfin.app.managers.retrofit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.goldfin.app.constatnts.WebServiceConstants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by  on 09-Mar-17.
 */

public class WebServiceFactory {

    private static Retrofit retrofit;
    private static Retrofit retrofitAuthenticateUser;
    private static Retrofit retrofitPACSViewer;
    private static Retrofit retrofitPACSToken;
    private static boolean isReferesh = false;


    /***
     *      SINGLETON Design Pattern
     */
    public static WebServiceProxy getInstance(final String _token, String base_url, boolean enableReferesh) {

//     webServiceProxy = null;
        isReferesh = enableReferesh;

        if (retrofit == null || isReferesh) {
            isReferesh = false;

//            Gson gson = new GsonBuilder()
//                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                    .create();


            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            // set your desired log level
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(80, TimeUnit.SECONDS);
            httpClient.readTimeout(81, TimeUnit.SECONDS);


            // add your other interceptors …
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder().addHeader("_token", _token + "");
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });


            // add logging as last interceptor
//            httpClient.addNetworkInterceptor(interceptor).addInterceptor(interceptor);  // <-- this is the important line!
            httpClient.addInterceptor(interceptor);  // <-- this is the important line!
            retrofit = new Retrofit.Builder()
                    .baseUrl(/*BaseApplication.unwrapValueText(*/base_url)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

//            WebServiceFactory.retrofit = retrofit.create(WebServiceProxy.class);
        }

        return retrofit.create(WebServiceProxy.class);
    }



    public static WebServiceProxy getInstance(final String newToken, boolean enableReferesh) {

//     webServiceProxy = null;


         isReferesh = enableReferesh;



            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            // set your desired log level
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(80, TimeUnit.SECONDS);
            httpClient.readTimeout(81, TimeUnit.SECONDS);


            // add your other interceptors …
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder().addHeader("_token", newToken + "");
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });


            // add logging as last interceptor
//            httpClient.addNetworkInterceptor(interceptor).addInterceptor(interceptor);  // <-- this is the important line!
            httpClient.addInterceptor(interceptor);  // <-- this is the important line!
            retrofit = new Retrofit.Builder()
                    .baseUrl(/*BaseApplication.unwrapValueText(*/WebServiceConstants.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

//            WebServiceFactory.retrofit = retrofit.create(WebServiceProxy.class);


        return retrofit.create(WebServiceProxy.class);
    }


//    public static WebServiceProxy getInstanceAuthenicateUser() {
//
//        if (retrofitAuthenticateUser == null) {
//
//
//            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//            // set your desired log level
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//            httpClient.connectTimeout(120, TimeUnit.SECONDS);
//            httpClient.readTimeout(121, TimeUnit.SECONDS);
//
//
////             add your other interceptors …
//            httpClient.addInterceptor(new Interceptor() {
//
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request original = chain.request();
//                    Request.Builder requestBuilder = original.newBuilder();
////                    requestBuilder.addHeader("_token", _token + "");
//
//                    // Request customization: add request headers
//
//                    Request request = requestBuilder.build();
//                    return chain.proceed(request);
//                }
//            });
//
//            // add logging as last interceptor
////            httpClient.addNetworkInterceptor(interceptor).addInterceptor(interceptor);  // <-- this is the important line!
//            httpClient.addInterceptor(interceptor);  // <-- this is the important line!
//            retrofitAuthenticateUser = new Retrofit.Builder()
//                    .baseUrl(BaseApplication.unwrapValueText(WebServiceConstants.BASE_URL_LOGIN))
//                    .addConverterFactory(GsonConverterFactory.create(GsonFactory.getSimpleGson()))
//                    .client(httpClient.build())
//                    .build();
//        }
//
//        return retrofitAuthenticateUser.create(WebServiceProxy.class);
//    }




}