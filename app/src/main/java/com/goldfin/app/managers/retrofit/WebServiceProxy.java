package com.goldfin.app.managers.retrofit;


import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.models.ContactsModel;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by  on 09-Mar-17.
 */

public interface WebServiceProxy {


    //     @Multipart
//    @POST("./")
//    Call<WebResponse<Object>> webServiceRequestAPIForWebResponseAnyObject(
//             @Part(WebServiceConstants.PARAMS_REQUEST_METHOD) RequestBody requestMethod,
//             @Part(WebServiceConstants.PARAMS_REQUEST_DATA) RequestBody requestData
//     );
    @POST("./")
    Call<WebResponse<Object>> webServiceRequestAPIForWebResponseAnyObject(
            @Part(WebServiceConstants.PARAMS_REQUEST_METHOD) RequestBody requestMethod,
            @Part(WebServiceConstants.PARAMS_REQUEST_DATA) RequestBody requestData
    );


    @POST(WebServiceConstants.METHOD_PERSONALINFO)
    Call<WebResponse<Object>> postPersonalInfo(
            @Body FormSendingDataModel personalInfoSendingModel
    );

    @POST(WebServiceConstants.METHOD_PROESSIONALINFO)
    Call<WebResponse<Object>> postProessionalIno(
            @Body FormSendingDataModel personalInfoSendingModel
    );

    @POST(WebServiceConstants.METHOD_FANANCIALINFO)
    Call<WebResponse<Object>> postFinancialsalaried(
            @Body FormSendingDataModel personalInfoSendingModel
    );


    @POST(WebServiceConstants.METHOD_BUSINESS_PERSONALINFO)
    Call<WebResponse<Object>> postBusinessPersonalInfo(
            @Body FormSendingDataModel personalBusinessInfoSendingModel
    );

    @POST(WebServiceConstants.METHOD_BUSINESS_PROESSIONALINFO)
    Call<WebResponse<Object>> postBusinInfo(
            @Body FormSendingDataModel personalBusinessInfoSendingModel
    );

    @POST(WebServiceConstants.METHOD_BUSINESS_FANANCIALINFO)
    Call<WebResponse<Object>> postBusFinanc(
            @Body FormSendingDataModel sendingDataModel
    );


    @POST(WebServiceConstants.METHOD_LOGIN)
    Call<WebResponse<Object>> login(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_SIGNUP)
    Call<WebResponse<Object>> signup(
            @Body UserModel userModel

    );

    @GET("./")
    Call<WebResponse<Object>> getCallGeneric(
    );

    @POST(WebServiceConstants.METHOD_VISITPLAN)
    Call<WebResponse<Object>> getVisitPlan(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_ADDTOINVITE)
    Call<WebResponse<Object>> postContactDetail(
            @Body ContactsModel contactsModel
    );


    @Multipart
    @POST(WebServiceConstants.METHOD_CMO)
    Call<WebResponse<Object>> postCMO(
            @Part MultipartBody.Part picture1,
            @Part MultipartBody.Part picture2,
            @Part("customerId") RequestBody customerId,
            @Part("CMOId") RequestBody CMOId,
            @Part("formId") RequestBody formId,
            @Part("shroffName") RequestBody shroffName,
            @Part("shroffCertificateNumber") RequestBody shroffCertificateNumber,
            @Part("natureOfGold") RequestBody natureOfGold,

            @Part("grossWeight") RequestBody grossWeight,
            @Part("netWeight") RequestBody netWeight,
            @Part("securedPacket1Number") RequestBody securedPacket1Number,
            @Part("securedPacket2Number") RequestBody securedPacket2Number,
            @Part("barCodeNumber") RequestBody barCodeNumber,

            @Part("userCode") RequestBody userCode,
            @Part("name") RequestBody name,
            @Part("date") RequestBody date,
            @Part("detailsGold") RequestBody detailsGold
    );

    @Multipart
    @POST(WebServiceConstants.METHOD_VOFORM)
    Call<WebResponse<Object>> postVOFORM(
            @Part MultipartBody.Part picture1,
            @Part MultipartBody.Part picture2,
            @Part("customerId") RequestBody customerId,
            @Part("VOId") RequestBody VOId,
            @Part("formId") RequestBody formId,
            @Part("personalInformation") RequestBody personalInformation,
            @Part("professionalInformation") RequestBody professionalInformation,
            @Part("financialInformation") RequestBody financialInformation,
            @Part("neighborcheck_residence") RequestBody neighborcheck_residence,
            @Part("neighborcheck_business") RequestBody neighborcheck_business,
            @Part("negative_reason") RequestBody negative_reason,
            @Part("remarks_personalInformation") RequestBody remarks_personalInformation,
            @Part("remarks_financialInformation") RequestBody remarks_financialInformation,
            @Part("remarks_professionalInformation") RequestBody remarks_professionalInformation,
            @Part("verificationResults") RequestBody verificationResults
    );

    @POST(WebServiceConstants.METHOD_LOANDETAILSUMMARY)
    Call<WebResponse<Object>> postDSLoan(
            @Body CMOModel cmoModel
    );


    @Multipart
    @POST(WebServiceConstants.METHOD_ADD_DOCUMENTS)
    Call<WebResponse<Object>> postDocumentDetail(
            @Part MultipartBody.Part cnicFront,
            @Part MultipartBody.Part cnicBack,
            @Part MultipartBody.Part residenceUtilityBills,
            @Part MultipartBody.Part currentSalarySlip,
            @Part MultipartBody.Part jobCertificate,
            @Part("_id") RequestBody id
    );


    @Multipart
    @POST(WebServiceConstants.METHOD_ADD_DOCUMENTS_BUSINESS)
    Call<WebResponse<Object>> postDocumentDetailBusiness(
            @Part MultipartBody.Part cnicFront,
            @Part MultipartBody.Part cnicBack,
            @Part MultipartBody.Part residenceUtilityBills,
            @Part MultipartBody.Part residenceBusinessBills,
            @Part MultipartBody.Part proofOfBusiness,
            @Part MultipartBody.Part proofOfBusinessIncome,
            @Part MultipartBody.Part SECP,
            @Part MultipartBody.Part NTN,
            @Part("_id") RequestBody id
    );


    @POST(WebServiceConstants.METHOD_GET_OTP)
    Call<WebResponse<Object>> getOTP(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_ENTER_OTP_CODE)
    Call<WebResponse<Object>> verifyOTP(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_ENTER_SIGNUP_OTP_CODE)
    Call<WebResponse<Object>> verifySignUpOTP(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_SET_NEW_PASSWORD)
    Call<WebResponse<Object>> resetNewPass(
            @Body UserModel userModel
    );

    @POST(WebServiceConstants.METHOD_CMO_ADD_USER)
    Call<WebResponse<Object>> addUserByCMO(
            @Body CMOModel cmoModel
    );

}

