package com.goldfin.app.managers.retrofit;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.goldfin.app.constatnts.WebServiceConstants;
import com.goldfin.app.helperclasses.Helper;
import com.goldfin.app.helperclasses.ui.helper.UIHelper;
import com.kaopiz.kprogresshud.KProgressHUD;

import com.goldfin.app.models.ContactsModel;
import com.goldfin.app.models.User.UserModel;
import com.goldfin.app.models.sendingmodels.AttachedDocumentModel;
import com.goldfin.app.models.sendingmodels.CMOModel;
import com.goldfin.app.models.sendingmodels.FormSendingDataModel;
import com.goldfin.app.models.wrappers.WebResponse;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;


/**
 * Created by hamzakhan on 6/30/2017.
 */

public class WebServices {
    private WebServiceProxy apiService;
    private KProgressHUD mDialog;
    private Context mContext;
    private static String bearerToken = "";

    public static String getBearerToken() {
        return bearerToken;
    }

    public static void setBearerToken(String bearerToken) {
        WebServices.bearerToken = bearerToken;
    }


    public WebServices(Activity activity, String token, boolean isShowDialog, String base_url, boolean enableReferesh) {
        apiService = WebServiceFactory.getInstance(token, base_url, enableReferesh);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }


    // For New Token
    public WebServices(Activity activity, String newToken, boolean enableReferesh, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(newToken, enableReferesh);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }


    /**
     * This API is for Any type of Object result but under our WebResponse wrapper
     * <p>
     * "ResponseMessage": "",
     * "ResponseCode": 200,   // You will get 200 is there is any data or 204 is no data is found
     * "ResponseType": "UserManager.GetUserPreferenceUsingMapper",
     * "ConsultationHistoryModel": []
     *
     * @param requestMethod
     * @param requestData
     * @param callBack
     * @return
     */

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObject(String requestMethod, String requestData, final IRequestWebResponseAnyObjectCallBack callBack) {
        RequestBody bodyRequestMethod = getRequestBody(MultipartBody.FORM, requestMethod);
        RequestBody bodyRequestData = getRequestBody(MultipartBody.FORM, requestData);
        Call<WebResponse<Object>> webResponseCall = apiService.webServiceRequestAPIForWebResponseAnyObject(bodyRequestMethod, bodyRequestData);

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }


    @NonNull
    private RequestBody getRequestBody(MediaType form, String trim) {
        return RequestBody.create(
                form, trim);
    }


    private void dismissDialog() {
        mDialog.dismiss();
    }


    public interface IRequestWebResponseAnyObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);

        void onError(Object object);
    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectG(UserModel requestData, String methodName, final IRequestWebResponseAnyObjectCallBack callBack) {
//        RequestBody bodyRequestData = getRequestBody(JsonObject.class, requestData);

        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_SIGNUP)) {

            webResponseCall = apiService.signup(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_LOGIN)) {
            webResponseCall = apiService.login(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_VISITPLAN)) {
            webResponseCall = apiService.getVisitPlan(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_GET_OTP)) {
            webResponseCall = apiService.getOTP(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ENTER_OTP_CODE)) {
            webResponseCall = apiService.verifyOTP(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_SET_NEW_PASSWORD)) {
            webResponseCall = apiService.resetNewPass(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ENTER_SIGNUP_OTP_CODE)) {
            webResponseCall = apiService.verifySignUpOTP(requestData);
        }

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectGet(final IRequestWebResponseAnyObjectCallBack callBack) {

        Call<WebResponse<Object>> webResponseCall = null;
        webResponseCall = apiService.getCallGeneric();

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectForm(FormSendingDataModel requestData, String methodName, final IRequestWebResponseAnyObjectCallBack callBack) {
//        RequestBody bodyRequestData = getRequestBody(JsonObject.class, requestData);

        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_PERSONALINFO)) {
            webResponseCall = apiService.postPersonalInfo(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_PROESSIONALINFO)) {
            webResponseCall = apiService.postProessionalIno(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_FANANCIALINFO)) {
            webResponseCall = apiService.postFinancialsalaried(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_BUSINESS_PERSONALINFO)) {
            webResponseCall = apiService.postBusinessPersonalInfo(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_BUSINESS_PROESSIONALINFO)) {
            webResponseCall = apiService.postBusinInfo(requestData);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_BUSINESS_FANANCIALINFO)) {
            webResponseCall = apiService.postBusFinanc(requestData);
        }

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null || response.body().noRecordFound()) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            if (!response.isSuccessful() && !response.body().isSuccess()) {
                                UIHelper.showShortToastInCenter(mContext, "Server Error");
                            } else {
                                String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                                UIHelper.showToast(mContext, message);
                                callBack.onError(message);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectContact(ContactsModel requestData, String methodName, final IRequestWebResponseAnyObjectCallBack callBack) {
//        RequestBody bodyRequestData = getRequestBody(JsonObject.class, requestData);

        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ADDTOINVITE))
            webResponseCall = apiService.postContactDetail(requestData);

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectDocument(AttachedDocumentModel model, String methodName, final IRequestWebResponseAnyObjectCallBack callBack) {

        Call<WebResponse<Object>> webResponseCall = null;

        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ADD_DOCUMENTS)) {
            webResponseCall = apiService.postDocumentDetail(

                    MultipartBody.Part.createFormData("cnicFront", model.getCnicFront(), RequestBody.create(MultipartBody.FORM, model.getCnicFront())),
                    MultipartBody.Part.createFormData("cnicBack", model.getCnicBack(), RequestBody.create(MultipartBody.FORM, model.getCnicBack())),
                    MultipartBody.Part.createFormData("residenceUtilityBills", model.getResidenceUtilityBills(), RequestBody.create(MultipartBody.FORM, model.getResidenceUtilityBills())),
                    MultipartBody.Part.createFormData("currentSalarySlip", model.getCurrentSalarySlip(), RequestBody.create(MultipartBody.FORM, model.getCurrentSalarySlip())),
                    MultipartBody.Part.createFormData("jobCertificate", model.getJobCertificate(), RequestBody.create(MultipartBody.FORM, model.getJobCertificate())),
                    getRequestBody(MultipartBody.FORM, model.getId()));
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ADD_DOCUMENTS_BUSINESS)) {
            webResponseCall = apiService.postDocumentDetailBusiness(

                    MultipartBody.Part.createFormData("cnicFront", model.getCnicFront(), RequestBody.create(MultipartBody.FORM, model.getCnicFront())),
                    MultipartBody.Part.createFormData("cnicBack", model.getCnicBack(), RequestBody.create(MultipartBody.FORM, model.getCnicBack())),
                    MultipartBody.Part.createFormData("residenceUtilityBills", model.getResidenceUtilityBills(), RequestBody.create(MultipartBody.FORM, model.getResidenceUtilityBills())),
                    MultipartBody.Part.createFormData("residenceBusinessBills", model.getResidenceBusinessBills(), RequestBody.create(MultipartBody.FORM, model.getResidenceBusinessBills())),
                    MultipartBody.Part.createFormData("proofOfBusiness", model.getProofOfBusiness(), RequestBody.create(MultipartBody.FORM, model.getProofOfBusiness())),
                    MultipartBody.Part.createFormData("proofOfBusinessIncome", model.getProofOfBusinessIncome(), RequestBody.create(MultipartBody.FORM, model.getProofOfBusinessIncome())),
                    MultipartBody.Part.createFormData("SECP", model.getsECP(), RequestBody.create(MultipartBody.FORM, model.getsECP())),
                    MultipartBody.Part.createFormData("NTN", model.getnTN(), RequestBody.create(MultipartBody.FORM, model.getnTN())),
                    getRequestBody(MultipartBody.FORM, model.getId()));
        }

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectCMO(CMOModel requestData, String methodName, final IRequestWebResponseAnyObjectCallBack callBack) {

        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CMO))
            webResponseCall = apiService.postCMO(
                    MultipartBody.Part.createFormData("picture1", requestData.getPicture1(), RequestBody.create(MultipartBody.FORM, requestData.getPicture1())),
                    MultipartBody.Part.createFormData("picture2", requestData.getPicture2(), RequestBody.create(MultipartBody.FORM, requestData.getPicture2())),
                    getRequestBody(MultipartBody.FORM, requestData.getCustomerId()),

                    getRequestBody(MultipartBody.FORM, requestData.getCMOId()),
                    getRequestBody(MultipartBody.FORM, requestData.getFormId()),
                    getRequestBody(MultipartBody.FORM, requestData.getShroffName()),
                    getRequestBody(MultipartBody.FORM, requestData.getShroffCertificateNumber()),
                    getRequestBody(MultipartBody.FORM, requestData.getNatureOfGold()),
                    getRequestBody(MultipartBody.FORM, requestData.getGrossWeight()),
                    getRequestBody(MultipartBody.FORM, requestData.getNetWeight()),
                    getRequestBody(MultipartBody.FORM, requestData.getSecuredPacket1Number()),
                    getRequestBody(MultipartBody.FORM, requestData.getSecuredPacket2Number()),

                    getRequestBody(MultipartBody.FORM, requestData.getBarCodeNumber()),
                    getRequestBody(MultipartBody.FORM, requestData.getUserCode()),
                    getRequestBody(MultipartBody.FORM, requestData.getName()),
                    getRequestBody(MultipartBody.FORM, requestData.getDate()),
                    getRequestBody(MultipartBody.FORM, requestData.getNatureOfGold())


            );
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_VOFORM))
            webResponseCall = apiService.postVOFORM(

                    MultipartBody.Part.createFormData("picture1", requestData.getPicture1(), RequestBody.create(MultipartBody.FORM, requestData.getPicture1())),
                    MultipartBody.Part.createFormData("picture2", requestData.getPicture2(), RequestBody.create(MultipartBody.FORM, requestData.getPicture2())),
                    getRequestBody(MultipartBody.FORM, requestData.getCustomerId()),

                    getRequestBody(MultipartBody.FORM, requestData.getVOId()),
                    getRequestBody(MultipartBody.FORM, requestData.getFormId()),

                    getRequestBody(MultipartBody.FORM, requestData.getPersonalInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getProfessionalInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getFinancialInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getNeighborcheckResidence()),
                    getRequestBody(MultipartBody.FORM, requestData.getNeighborcheckBusiness()),

                    getRequestBody(MultipartBody.FORM, requestData.getNegativeReason()),
                    getRequestBody(MultipartBody.FORM, requestData.getRemarksPersonalInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getFinancialInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getRemarksProfessionalInformation()),
                    getRequestBody(MultipartBody.FORM, requestData.getVerificationResults())

            );
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_LOANDETAILSUMMARY))
            webResponseCall = apiService.postDSLoan(requestData);
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CMO_ADD_USER))
            webResponseCall = apiService.addUserByCMO(requestData);
        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

}
