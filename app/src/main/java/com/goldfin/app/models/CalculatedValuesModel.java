package com.goldfin.app.models;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalculatedValuesModel implements Serializable {

    @SerializedName("tolaRequired")
    @Expose
    private String tolaRequired;
    @SerializedName("gramRequired")
    @Expose
    private String gramRequired;
    @SerializedName("requiredAmount")
    @Expose
    private String requiredAmount;

    public String getTolaRequired() {
        return tolaRequired;
    }

    public void setTolaRequired(String tolaRequired) {
        this.tolaRequired = tolaRequired;
    }

    public String getGramRequired() {
        return gramRequired;
    }

    public void setGramRequired(String gramRequired) {
        this.gramRequired = gramRequired;
    }

    public String getRequiredAmount() {
        return requiredAmount;
    }

    public void setRequiredAmount(String requiredAmount) {
        this.requiredAmount = requiredAmount;
    }
}