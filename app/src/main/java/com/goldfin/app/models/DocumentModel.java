package com.goldfin.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentModel {

    @SerializedName("cnicFront")
    @Expose
    private String cnicFront;
    @SerializedName("cnicBack")
    @Expose
    private String cnicBack;
    @SerializedName("residenceUtilityBills")
    @Expose
    private String residenceUtilityBills;
    @SerializedName("currentSalarySlip")
    @Expose
    private String currentSalarySlip;
    @SerializedName("jobCertificate")
    @Expose
    private String jobCertificate;
    @SerializedName("experienceLetter")
    @Expose
    private String experienceLetter;


    @SerializedName("residenceBusinessBills")
    @Expose
    private String residenceBusinessBills;
    @SerializedName("proofOfBusiness")
    @Expose
    private String proofOfBusiness;
    @SerializedName("proofOfBusinessIncome")
    @Expose
    private String proofOfBusinessIncome;
    @SerializedName("SECP")
    @Expose
    private String sECP;
    @SerializedName("NTN")
    @Expose
    private String nTN;

    public String getCnicFront() {
        return cnicFront;
    }

    public void setCnicFront(String cnicFront) {
        this.cnicFront = cnicFront;
    }

    public String getCnicBack() {
        return cnicBack;
    }

    public void setCnicBack(String cnicBack) {
        this.cnicBack = cnicBack;
    }

    public String getResidenceUtilityBills() {
        return residenceUtilityBills;
    }

    public void setResidenceUtilityBills(String residenceUtilityBills) {
        this.residenceUtilityBills = residenceUtilityBills;
    }

    public String getCurrentSalarySlip() {
        return currentSalarySlip;
    }

    public void setCurrentSalarySlip(String currentSalarySlip) {
        this.currentSalarySlip = currentSalarySlip;
    }

    public String getJobCertificate() {
        return jobCertificate;
    }

    public void setJobCertificate(String jobCertificate) {
        this.jobCertificate = jobCertificate;
    }

    public String getExperienceLetter() {
        return experienceLetter;
    }

    public void setExperienceLetter(String experienceLetter) {
        this.experienceLetter = experienceLetter;
    }

    public String getResidenceBusinessBills() {
        return residenceBusinessBills;
    }

    public void setResidenceBusinessBills(String residenceBusinessBills) {
        this.residenceBusinessBills = residenceBusinessBills;
    }

    public String getProofOfBusiness() {
        return proofOfBusiness;
    }

    public void setProofOfBusiness(String proofOfBusiness) {
        this.proofOfBusiness = proofOfBusiness;
    }

    public String getProofOfBusinessIncome() {
        return proofOfBusinessIncome;
    }

    public void setProofOfBusinessIncome(String proofOfBusinessIncome) {
        this.proofOfBusinessIncome = proofOfBusinessIncome;
    }

    public String getsECP() {
        return sECP;
    }

    public void setsECP(String sECP) {
        this.sECP = sECP;
    }

    public String getnTN() {
        return nTN;
    }

    public void setnTN(String nTN) {
        this.nTN = nTN;
    }
}