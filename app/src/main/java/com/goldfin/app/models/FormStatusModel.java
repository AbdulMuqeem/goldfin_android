package com.goldfin.app.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormStatusModel implements Serializable
{

@SerializedName("personalInformation")
@Expose
private String personalInformation;
@SerializedName("professionalInformation")
@Expose
private String professionalInformation;
@SerializedName("financialInformation")
@Expose
private String financialInformation;
@SerializedName("generalInformation")
@Expose
private String generalInformation;
@SerializedName("documentsUploading")
@Expose
private String documentsUploading;
private final static long serialVersionUID = -8814848025954668760L;

public String getPersonalInformation() {
return personalInformation;
}

public void setPersonalInformation(String personalInformation) {
this.personalInformation = personalInformation;
}

public String getProfessionalInformation() {
return professionalInformation;
}

public void setProfessionalInformation(String professionalInformation) {
this.professionalInformation = professionalInformation;
}

public String getFinancialInformation() {
return financialInformation;
}

public void setFinancialInformation(String financialInformation) {
this.financialInformation = financialInformation;
}

public String getGeneralInformation() {
return generalInformation;
}

public void setGeneralInformation(String generalInformation) {
this.generalInformation = generalInformation;
}

public String getDocumentsUploading() {
return documentsUploading;
}

public void setDocumentsUploading(String documentsUploading) {
this.documentsUploading = documentsUploading;
}

}