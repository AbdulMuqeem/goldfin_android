package com.goldfin.app.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LaonStatusModel implements Serializable
{

@SerializedName("loanStatus")
@Expose
private String loanStatus;
@SerializedName("message")
@Expose
private String message;
private final static long serialVersionUID = 8207292304956849896L;

public String getLoanStatus() {
return loanStatus;
}

public void setLoanStatus(String loanStatus) {
this.loanStatus = loanStatus;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}