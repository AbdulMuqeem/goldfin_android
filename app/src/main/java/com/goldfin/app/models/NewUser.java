package com.goldfin.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewUser {

@SerializedName("_id")
@Expose
private String id;
@SerializedName("formId")
@Expose
private String formId;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getFormId() {
return formId;
}

public void setFormId(String formId) {
this.formId = formId;
}

}