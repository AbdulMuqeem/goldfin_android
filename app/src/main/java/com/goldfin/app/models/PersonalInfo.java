package com.goldfin.app.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalInfo implements Serializable
{

@SerializedName("purpose")
@Expose
private String purpose;
@SerializedName("city")
@Expose
private String city;
@SerializedName("fname")
@Expose
private String fname;
@SerializedName("dob")
@Expose
private String dob;
@SerializedName("currentaddress")
@Expose
private String currentaddress;
@SerializedName("residentialstatus")
@Expose
private String residentialstatus;
@SerializedName("residingsince")
@Expose
private String residingsince;
@SerializedName("years")
@Expose
private String years;
@SerializedName("permanentaddress")
@Expose
private String permanentaddress;
@SerializedName("othermobilenum")
@Expose
private String othermobilenum;
@SerializedName("landline")
@Expose
private String landline;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("maritalstatus")
@Expose
private String maritalstatus;
@SerializedName("education")
@Expose
private String education;
@SerializedName("loanAmount")
@Expose
private String loanAmount;
@SerializedName("profession")
@Expose
private String profession;
@SerializedName("designation")
@Expose
private String designation;
@SerializedName("empname")
@Expose
private String empname;
@SerializedName("empaddress")
@Expose
private String empaddress;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("website")
@Expose
private String website;
@SerializedName("jobstatus")
@Expose
private String jobstatus;
@SerializedName("dateofjoining")
@Expose
private String dateofjoining;
@SerializedName("totalexp")
@Expose
private String totalexp;
@SerializedName("grosssalary")
@Expose
private String grosssalary;
@SerializedName("netsalary")
@Expose
private String netsalary;
@SerializedName("othersource")
@Expose
private String othersource;
@SerializedName("natureofincome")
@Expose
private String natureofincome;
@SerializedName("outstandingloan")
@Expose
private String outstandingloan;
@SerializedName("sourceofloan")
@Expose
private String sourceofloan;
@SerializedName("loanamount")
@Expose
private String loanamount;
@SerializedName("household")
@Expose
private String household;
@SerializedName("utilitybill")
@Expose
private String utilitybill;
@SerializedName("childeducation")
@Expose
private String childeducation;
@SerializedName("medical")
@Expose
private String medical;
@SerializedName("committe")
@Expose
private String committe;
@SerializedName("loaninstallment")
@Expose
private String loaninstallment;
@SerializedName("other0")
@Expose
private String other0;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("userId")
@Expose
private String userId;
@SerializedName("createdAt")
@Expose
private String createdAt;
@SerializedName("__v")
@Expose
private Integer v;
private final static long serialVersionUID = 1447863946184666163L;

public String getPurpose() {
return purpose;
}

public void setPurpose(String purpose) {
this.purpose = purpose;
}

public String getCity() {
return city;
}

public void setCity(String city) {
this.city = city;
}

public String getFname() {
return fname;
}

public void setFname(String fname) {
this.fname = fname;
}

public String getDob() {
return dob;
}

public void setDob(String dob) {
this.dob = dob;
}

public String getCurrentaddress() {
return currentaddress;
}

public void setCurrentaddress(String currentaddress) {
this.currentaddress = currentaddress;
}

public String getResidentialstatus() {
return residentialstatus;
}

public void setResidentialstatus(String residentialstatus) {
this.residentialstatus = residentialstatus;
}

public String getResidingsince() {
return residingsince;
}

public void setResidingsince(String residingsince) {
this.residingsince = residingsince;
}

public String getYears() {
return years;
}

public void setYears(String years) {
this.years = years;
}

public String getPermanentaddress() {
return permanentaddress;
}

public void setPermanentaddress(String permanentaddress) {
this.permanentaddress = permanentaddress;
}

public String getOthermobilenum() {
return othermobilenum;
}

public void setOthermobilenum(String othermobilenum) {
this.othermobilenum = othermobilenum;
}

public String getLandline() {
return landline;
}

public void setLandline(String landline) {
this.landline = landline;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getMaritalstatus() {
return maritalstatus;
}

public void setMaritalstatus(String maritalstatus) {
this.maritalstatus = maritalstatus;
}

public String getEducation() {
return education;
}

public void setEducation(String education) {
this.education = education;
}

public String getLoanAmount() {
return loanAmount;
}

public void setLoanAmount(String loanAmount) {
this.loanAmount = loanAmount;
}

public String getProfession() {
return profession;
}

public void setProfession(String profession) {
this.profession = profession;
}

public String getDesignation() {
return designation;
}

public void setDesignation(String designation) {
this.designation = designation;
}

public String getEmpname() {
return empname;
}

public void setEmpname(String empname) {
this.empname = empname;
}

public String getEmpaddress() {
return empaddress;
}

public void setEmpaddress(String empaddress) {
this.empaddress = empaddress;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getWebsite() {
return website;
}

public void setWebsite(String website) {
this.website = website;
}

public String getJobstatus() {
return jobstatus;
}

public void setJobstatus(String jobstatus) {
this.jobstatus = jobstatus;
}

public String getDateofjoining() {
return dateofjoining;
}

public void setDateofjoining(String dateofjoining) {
this.dateofjoining = dateofjoining;
}

public String getTotalexp() {
return totalexp;
}

public void setTotalexp(String totalexp) {
this.totalexp = totalexp;
}

public String getGrosssalary() {
return grosssalary;
}

public void setGrosssalary(String grosssalary) {
this.grosssalary = grosssalary;
}

public String getNetsalary() {
return netsalary;
}

public void setNetsalary(String netsalary) {
this.netsalary = netsalary;
}

public String getOthersource() {
return othersource;
}

public void setOthersource(String othersource) {
this.othersource = othersource;
}

public String getNatureofincome() {
return natureofincome;
}

public void setNatureofincome(String natureofincome) {
this.natureofincome = natureofincome;
}

public String getOutstandingloan() {
return outstandingloan;
}

public void setOutstandingloan(String outstandingloan) {
this.outstandingloan = outstandingloan;
}

public String getSourceofloan() {
return sourceofloan;
}

public void setSourceofloan(String sourceofloan) {
this.sourceofloan = sourceofloan;
}

public String getLoanamount() {
return loanamount;
}

public void setLoanamount(String loanamount) {
this.loanamount = loanamount;
}

public String getHousehold() {
return household;
}

public void setHousehold(String household) {
this.household = household;
}

public String getUtilitybill() {
return utilitybill;
}

public void setUtilitybill(String utilitybill) {
this.utilitybill = utilitybill;
}

public String getChildeducation() {
return childeducation;
}

public void setChildeducation(String childeducation) {
this.childeducation = childeducation;
}

public String getMedical() {
return medical;
}

public void setMedical(String medical) {
this.medical = medical;
}

public String getCommitte() {
return committe;
}

public void setCommitte(String committe) {
this.committe = committe;
}

public String getLoaninstallment() {
return loaninstallment;
}

public void setLoaninstallment(String loaninstallment) {
this.loaninstallment = loaninstallment;
}

public String getOther0() {
return other0;
}

public void setOther0(String other0) {
this.other0 = other0;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public Integer getV() {
return v;
}

public void setV(Integer v) {
this.v = v;
}

}