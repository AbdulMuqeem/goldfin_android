package com.goldfin.app.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanVisitData implements Serializable
{

@SerializedName("date")
@Expose
private String date;
@SerializedName("time")
@Expose
private String time;
private final static long serialVersionUID = -3909902011274685698L;

public String getDate() {
return date;
}

public void setDate(String date) {
this.date = date;
}

public String getTime() {
return time;
}

public void setTime(String time) {
this.time = time;
}

}