package com.goldfin.app.models.User;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.goldfin.app.managers.retrofit.GsonFactory;
import com.goldfin.app.models.PlanVisitData;

public class UserModel implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("formDone")
    @Expose
    private String formDone;
    @SerializedName("verifyCode")
    @Expose
    private String verifyCode;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @SerializedName("personalInformation")
    @Expose
    private String personalInformation;
    @SerializedName("professionalInformation")
    @Expose
    private String professionalInformation;
    @SerializedName("financialInformation")
    @Expose
    private String financialInformation;
    @SerializedName("generalInformation")
    @Expose
    private String generalInformation;
    @SerializedName("documentsUploading")
    @Expose
    private String documentsUploading;
    @SerializedName("loanStatus")
    @Expose
    private String loanStatus;
    @SerializedName("loanmessage")
    @Expose
    private String loanmessage;
    @SerializedName("lafform")
    @Expose
    private String lafform;
    @SerializedName("planvisit")
    @Expose
    private String planvisit;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("cnic")
    @Expose
    private String cnic;
    @SerializedName("accountStatus")
    @Expose
    private String accountStatus;
    @SerializedName("phoneVerification")
    @Expose
    private String phoneVerification;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    @SerializedName("planVisitData")
    @Expose
    private PlanVisitData planVisitData;
    private final static long serialVersionUID = 5414178778702144771L;

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public String getProfessionalInformation() {
        return professionalInformation;
    }

    public void setProfessionalInformation(String professionalInformation) {
        this.professionalInformation = professionalInformation;
    }

    public String getFinancialInformation() {
        return financialInformation;
    }

    public void setFinancialInformation(String financialInformation) {
        this.financialInformation = financialInformation;
    }

    public String getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }

    public String getDocumentsUploading() {
        return documentsUploading;
    }

    public void setDocumentsUploading(String documentsUploading) {
        this.documentsUploading = documentsUploading;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanmessage() {
        return loanmessage;
    }

    public void setLoanmessage(String loanmessage) {
        this.loanmessage = loanmessage;
    }

    public String getLafform() {
        return lafform;
    }

    public void setLafform(String lafform) {
        this.lafform = lafform;
    }

    public String getPlanvisit() {
        return planvisit;
    }

    public void setPlanvisit(String planvisit) {
        this.planvisit = planvisit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPhoneVerification() {
        return phoneVerification;
    }

    public void setPhoneVerification(String phoneVerification) {
        this.phoneVerification = phoneVerification;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public PlanVisitData getPlanVisitData() {
        return planVisitData;
    }

    public void setPlanVisitData(PlanVisitData planVisitData) {
        this.planVisitData = planVisitData;
    }

    public String getFormDone() {
        return formDone;
    }

    public void setFormDone(String formDone) {
        this.formDone = formDone;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    @SerializedName("desiredLoanAmount")
    @Expose
    private String desiredLoanAmount;
    @SerializedName("currentaddress")
    @Expose
    private String currentaddress;
    @SerializedName("cmo")
    @Expose
    private String cmo;
    @SerializedName("vo")
    @Expose
    private String vo;
    @SerializedName("cmoDone")
    @Expose
    private String cmoDone;
    @SerializedName("voDone")
    @Expose
    private String voDone;
    @SerializedName("bo")
    @Expose
    private String bo;
    @SerializedName("verifiedCode")
    @Expose
    private String verifiedCode;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDesiredLoanAmount() {
        return desiredLoanAmount;
    }

    public void setDesiredLoanAmount(String desiredLoanAmount) {
        this.desiredLoanAmount = desiredLoanAmount;
    }

    public String getCurrentaddress() {
        return currentaddress;
    }

    public void setCurrentaddress(String currentaddress) {
        this.currentaddress = currentaddress;
    }

    public String getCmo() {
        return cmo;
    }

    public void setCmo(String cmo) {
        this.cmo = cmo;
    }

    public String getVo() {
        return vo;
    }

    public void setVo(String vo) {
        this.vo = vo;
    }

    public String getCmoDone() {
        return cmoDone;
    }

    public void setCmoDone(String cmoDone) {
        this.cmoDone = cmoDone;
    }

    public String getVoDone() {
        return voDone;
    }

    public void setVoDone(String voDone) {
        this.voDone = voDone;
    }

    public String getBo() {
        return bo;
    }

    public void setBo(String bo) {
        this.bo = bo;
    }

    public String getVerifiedCode() {
        return verifiedCode;
    }

    public void setVerifiedCode(String verifiedCode) {
        this.verifiedCode = verifiedCode;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }
}