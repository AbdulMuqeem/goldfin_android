package com.goldfin.app.models.receivingmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAppointmentsModel {

    @SerializedName("userProfession")
    @Expose
    private String userProfession;
    @SerializedName("personalInformation")
    @Expose
    private String personalInformation;
    @SerializedName("professionalInformation")
    @Expose
    private String professionalInformation;
    @SerializedName("financialInformation")
    @Expose
    private String financialInformation;
    @SerializedName("generalInformation")
    @Expose
    private String generalInformation;
    @SerializedName("documentsUploading")
    @Expose
    private String documentsUploading;
    @SerializedName("loanStatus")
    @Expose
    private String loanStatus;
    @SerializedName("loanmessage")
    @Expose
    private String loanmessage;
    @SerializedName("lafform")
    @Expose
    private String lafform;
    @SerializedName("planvisit")
    @Expose
    private String planvisit;
    @SerializedName("cmo")
    @Expose
    private String cmo;
    @SerializedName("vo")
    @Expose
    private String vo;
    @SerializedName("cmoDone")
    @Expose
    private String cmoDone;
    @SerializedName("voDone")
    @Expose
    private String voDone;
    @SerializedName("formDone")
    @Expose
    private String formDone;
    @SerializedName("verifiedCode")
    @Expose
    private String verifiedCode;
    @SerializedName("bo")
    @Expose
    private String bo;
    @SerializedName("_id")
    @Expose
    private String form_id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("cnic")
    @Expose
    private String cnic;
    @SerializedName("accountStatus")
    @Expose
    private String accountStatus;
    @SerializedName("phoneVerification")
    @Expose
    private String phoneVerification;
    @SerializedName("verificationCode")
    @Expose
    private String verificationCode;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("profession")
    @Expose
    private String profession;


    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("currentaddress")
    @Expose
    private String currentaddress;
    @SerializedName("residentialstatus")
    @Expose
    private String residentialstatus;
    @SerializedName("residingsince")
    @Expose
    private String residingsince;
    @SerializedName("years")
    @Expose
    private Object years;
    @SerializedName("permanentaddress")
    @Expose
    private String permanentaddress;
    @SerializedName("othermobilenum")
    @Expose
    private String othermobilenum;
    @SerializedName("landline")
    @Expose
    private String landline;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("maritalstatus")
    @Expose
    private String maritalstatus;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("desiredLoanAmount")
    @Expose
    private String desiredLoanAmount;
    @SerializedName("outstandingloanamount")
    @Expose
    private Object outstandingloanamount;
    @SerializedName("outstandingloan")
    @Expose
    private String outstandingloan;
    @SerializedName("sourceofloan")
    @Expose
    private String sourceofloan;

    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("empname")
    @Expose
    private String empname;
    @SerializedName("empaddress")
    @Expose
    private String empaddress;

    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("jobstatus")
    @Expose
    private String jobstatus;
    @SerializedName("dateofjoining")
    @Expose
    private String dateofjoining;
    @SerializedName("totalexp")
    @Expose
    private String totalexp;
    @SerializedName("grosssalary")
    @Expose
    private String grosssalary;
    @SerializedName("netsalary")
    @Expose
    private String netsalary;
    @SerializedName("othersource")
    @Expose
    private String othersource;
    @SerializedName("natureofincome")
    @Expose
    private String natureofincome;
    @SerializedName("household")
    @Expose
    private String household;
    @SerializedName("houserent")
    @Expose
    private String houserent;
    @SerializedName("utilitybill")
    @Expose
    private String utilitybill;
    @SerializedName("childeducation")
    @Expose
    private String childeducation;
    @SerializedName("medical")
    @Expose
    private String medical;
    @SerializedName("committe")
    @Expose
    private String committe;
    @SerializedName("utilitybills")
    @Expose
    private String utilitybills;
    @SerializedName("loaninstallment")
    @Expose
    private String loaninstallment;
    @SerializedName("other0")
    @Expose
    private String other0;
    @SerializedName("totalexpense")
    @Expose
    private Object totalexpense;

    @SerializedName("loanAmount")
    @Expose
    private String loanAmount;
    @SerializedName("loanamount")
    @Expose
    private String loanamount;
    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("cnicFront")
    @Expose
    private String cnicFront;

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public String getProfessionalInformation() {
        return professionalInformation;
    }

    public void setProfessionalInformation(String professionalInformation) {
        this.professionalInformation = professionalInformation;
    }

    public String getFinancialInformation() {
        return financialInformation;
    }

    public void setFinancialInformation(String financialInformation) {
        this.financialInformation = financialInformation;
    }

    public String getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }

    public String getDocumentsUploading() {
        return documentsUploading;
    }

    public void setDocumentsUploading(String documentsUploading) {
        this.documentsUploading = documentsUploading;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanmessage() {
        return loanmessage;
    }

    public void setLoanmessage(String loanmessage) {
        this.loanmessage = loanmessage;
    }

    public String getLafform() {
        return lafform;
    }

    public void setLafform(String lafform) {
        this.lafform = lafform;
    }

    public String getPlanvisit() {
        return planvisit;
    }

    public void setPlanvisit(String planvisit) {
        this.planvisit = planvisit;
    }

    public String getCmo() {
        return cmo;
    }

    public void setCmo(String cmo) {
        this.cmo = cmo;
    }

    public String getVo() {
        return vo;
    }

    public void setVo(String vo) {
        this.vo = vo;
    }

    public String getCmoDone() {
        return cmoDone;
    }

    public void setCmoDone(String cmoDone) {
        this.cmoDone = cmoDone;
    }

    public String getVoDone() {
        return voDone;
    }

    public void setVoDone(String voDone) {
        this.voDone = voDone;
    }

    public String getFormDone() {
        return formDone;
    }

    public void setFormDone(String formDone) {
        this.formDone = formDone;
    }

    public String getVerifiedCode() {
        return verifiedCode;
    }

    public void setVerifiedCode(String verifiedCode) {
        this.verifiedCode = verifiedCode;
    }

    public String getBo() {
        return bo;
    }

    public void setBo(String bo) {
        this.bo = bo;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPhoneVerification() {
        return phoneVerification;
    }

    public void setPhoneVerification(String phoneVerification) {
        this.phoneVerification = phoneVerification;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCurrentaddress() {
        return currentaddress;
    }

    public void setCurrentaddress(String currentaddress) {
        this.currentaddress = currentaddress;
    }

    public String getResidentialstatus() {
        return residentialstatus;
    }

    public void setResidentialstatus(String residentialstatus) {
        this.residentialstatus = residentialstatus;
    }

    public String getResidingsince() {
        return residingsince;
    }

    public void setResidingsince(String residingsince) {
        this.residingsince = residingsince;
    }

    public Object getYears() {
        return years;
    }

    public void setYears(Object years) {
        this.years = years;
    }

    public String getPermanentaddress() {
        return permanentaddress;
    }

    public void setPermanentaddress(String permanentaddress) {
        this.permanentaddress = permanentaddress;
    }

    public String getOthermobilenum() {
        return othermobilenum;
    }

    public void setOthermobilenum(String othermobilenum) {
        this.othermobilenum = othermobilenum;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalstatus() {
        return maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        this.maritalstatus = maritalstatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDesiredLoanAmount() {
        return desiredLoanAmount;
    }

    public void setDesiredLoanAmount(String desiredLoanAmount) {
        this.desiredLoanAmount = desiredLoanAmount;
    }

    public Object getOutstandingloanamount() {
        return outstandingloanamount;
    }

    public void setOutstandingloanamount(Object outstandingloanamount) {
        this.outstandingloanamount = outstandingloanamount;
    }

    public String getOutstandingloan() {
        return outstandingloan;
    }

    public void setOutstandingloan(String outstandingloan) {
        this.outstandingloan = outstandingloan;
    }

    public String getSourceofloan() {
        return sourceofloan;
    }

    public void setSourceofloan(String sourceofloan) {
        this.sourceofloan = sourceofloan;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getEmpaddress() {
        return empaddress;
    }

    public void setEmpaddress(String empaddress) {
        this.empaddress = empaddress;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getJobstatus() {
        return jobstatus;
    }

    public void setJobstatus(String jobstatus) {
        this.jobstatus = jobstatus;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getTotalexp() {
        return totalexp;
    }

    public void setTotalexp(String totalexp) {
        this.totalexp = totalexp;
    }

    public String getGrosssalary() {
        return grosssalary;
    }

    public void setGrosssalary(String grosssalary) {
        this.grosssalary = grosssalary;
    }

    public String getNetsalary() {
        return netsalary;
    }

    public void setNetsalary(String netsalary) {
        this.netsalary = netsalary;
    }

    public String getOthersource() {
        return othersource;
    }

    public void setOthersource(String othersource) {
        this.othersource = othersource;
    }

    public String getNatureofincome() {
        return natureofincome;
    }

    public void setNatureofincome(String natureofincome) {
        this.natureofincome = natureofincome;
    }

    public String getHousehold() {
        return household;
    }

    public void setHousehold(String household) {
        this.household = household;
    }

    public String getHouserent() {
        return houserent;
    }

    public void setHouserent(String houserent) {
        this.houserent = houserent;
    }

    public String getUtilitybill() {
        return utilitybill;
    }

    public void setUtilitybill(String utilitybill) {
        this.utilitybill = utilitybill;
    }

    public String getChildeducation() {
        return childeducation;
    }

    public void setChildeducation(String childeducation) {
        this.childeducation = childeducation;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getCommitte() {
        return committe;
    }

    public void setCommitte(String committe) {
        this.committe = committe;
    }

    public String getUtilitybills() {
        return utilitybills;
    }

    public void setUtilitybills(String utilitybills) {
        this.utilitybills = utilitybills;
    }

    public String getLoaninstallment() {
        return loaninstallment;
    }

    public void setLoaninstallment(String loaninstallment) {
        this.loaninstallment = loaninstallment;
    }

    public String getOther0() {
        return other0;
    }

    public void setOther0(String other0) {
        this.other0 = other0;
    }

    public Object getTotalexpense() {
        return totalexpense;
    }

    public void setTotalexpense(Object totalexpense) {
        this.totalexpense = totalexpense;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getLoanamount() {
        return loanamount;
    }

    public void setLoanamount(String loanamount) {
        this.loanamount = loanamount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCnicFront() {
        return cnicFront;
    }

    public void setCnicFront(String cnicFront) {
        this.cnicFront = cnicFront;
    }

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }


    @SerializedName("bussinessname")
    @Expose
    private String bussinessname;
    @SerializedName("businessStatus")
    @Expose
    private String businessStatus;
    @SerializedName("bussinessaddress")
    @Expose
    private String bussinessaddress;
    @SerializedName("bussinesslandline")
    @Expose
    private String bussinesslandline;
    @SerializedName("bussinesswebsite")
    @Expose
    private String bussinesswebsite;
    @SerializedName("bussinessOwnstatus")
    @Expose
    private Object bussinessOwnstatus;
    @SerializedName("natureofbusiness")
    @Expose
    private String natureofbusiness;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("establishsince")
    @Expose
    private String establishsince;
    @SerializedName("secp")
    @Expose
    private String secp;
    @SerializedName("dateofreg")
    @Expose
    private String dateofreg;
    @SerializedName("ntn")
    @Expose
    private String ntn;
    @SerializedName("nbrofemp")
    @Expose
    private String nbrofemp;
    @SerializedName("working")
    @Expose
    private Object working;
    @SerializedName("ownland")
    @Expose
    private String ownland;
    @SerializedName("rentedland")
    @Expose
    private String rentedland;
    @SerializedName("cultivating")
    @Expose
    private String cultivating;
    @SerializedName("maincrops")
    @Expose
    private String maincrops;
    @SerializedName("animals")
    @Expose
    private String animals;
    @SerializedName("averagemonthbusinessexp")
    @Expose
    private String averagemonthbusinessexp;
    @SerializedName("avgmonthturnover")
    @Expose
    private String avgmonthturnover;
    @SerializedName("monthlyincome")
    @Expose
    private String monthlyincome;
    @SerializedName("otherincome")
    @Expose
    private String otherincome;
    @SerializedName("monthlyaccounts")
    @Expose
    private String monthlyaccounts;
    @SerializedName("household1")
    @Expose
    private String household1;
    @SerializedName("houserent1")
    @Expose
    private String houserent1;
    @SerializedName("utilitybills1")
    @Expose
    private String utilitybills1;
    @SerializedName("childeducation1")
    @Expose
    private String childeducation1;
    @SerializedName("medical1")
    @Expose
    private String medical1;
    @SerializedName("committee1")
    @Expose
    private String committee1;
    @SerializedName("loaninstallment1")
    @Expose
    private String loaninstallment1;
    @SerializedName("other3")
    @Expose
    private String other3;

    public String getBussinessname() {
        return bussinessname;
    }

    public void setBussinessname(String bussinessname) {
        this.bussinessname = bussinessname;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getBussinessaddress() {
        return bussinessaddress;
    }

    public void setBussinessaddress(String bussinessaddress) {
        this.bussinessaddress = bussinessaddress;
    }

    public String getBussinesslandline() {
        return bussinesslandline;
    }

    public void setBussinesslandline(String bussinesslandline) {
        this.bussinesslandline = bussinesslandline;
    }

    public String getBussinesswebsite() {
        return bussinesswebsite;
    }

    public void setBussinesswebsite(String bussinesswebsite) {
        this.bussinesswebsite = bussinesswebsite;
    }

    public Object getBussinessOwnstatus() {
        return bussinessOwnstatus;
    }

    public void setBussinessOwnstatus(Object bussinessOwnstatus) {
        this.bussinessOwnstatus = bussinessOwnstatus;
    }

    public String getNatureofbusiness() {
        return natureofbusiness;
    }

    public void setNatureofbusiness(String natureofbusiness) {
        this.natureofbusiness = natureofbusiness;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getEstablishsince() {
        return establishsince;
    }

    public void setEstablishsince(String establishsince) {
        this.establishsince = establishsince;
    }

    public String getSecp() {
        return secp;
    }

    public void setSecp(String secp) {
        this.secp = secp;
    }

    public String getDateofreg() {
        return dateofreg;
    }

    public void setDateofreg(String dateofreg) {
        this.dateofreg = dateofreg;
    }

    public String getNtn() {
        return ntn;
    }

    public void setNtn(String ntn) {
        this.ntn = ntn;
    }

    public String getNbrofemp() {
        return nbrofemp;
    }

    public void setNbrofemp(String nbrofemp) {
        this.nbrofemp = nbrofemp;
    }

    public Object getWorking() {
        return working;
    }

    public void setWorking(Object working) {
        this.working = working;
    }

    public String getOwnland() {
        return ownland;
    }

    public void setOwnland(String ownland) {
        this.ownland = ownland;
    }

    public String getRentedland() {
        return rentedland;
    }

    public void setRentedland(String rentedland) {
        this.rentedland = rentedland;
    }

    public String getCultivating() {
        return cultivating;
    }

    public void setCultivating(String cultivating) {
        this.cultivating = cultivating;
    }

    public String getMaincrops() {
        return maincrops;
    }

    public void setMaincrops(String maincrops) {
        this.maincrops = maincrops;
    }

    public String getAnimals() {
        return animals;
    }

    public void setAnimals(String animals) {
        this.animals = animals;
    }

    public String getAveragemonthbusinessexp() {
        return averagemonthbusinessexp;
    }

    public void setAveragemonthbusinessexp(String averagemonthbusinessexp) {
        this.averagemonthbusinessexp = averagemonthbusinessexp;
    }

    public String getAvgmonthturnover() {
        return avgmonthturnover;
    }

    public void setAvgmonthturnover(String avgmonthturnover) {
        this.avgmonthturnover = avgmonthturnover;
    }

    public String getMonthlyincome() {
        return monthlyincome;
    }

    public void setMonthlyincome(String monthlyincome) {
        this.monthlyincome = monthlyincome;
    }

    public String getOtherincome() {
        return otherincome;
    }

    public void setOtherincome(String otherincome) {
        this.otherincome = otherincome;
    }

    public String getMonthlyaccounts() {
        return monthlyaccounts;
    }

    public void setMonthlyaccounts(String monthlyaccounts) {
        this.monthlyaccounts = monthlyaccounts;
    }

    public String getHousehold1() {
        return household1;
    }

    public void setHousehold1(String household1) {
        this.household1 = household1;
    }

    public String getHouserent1() {
        return houserent1;
    }

    public void setHouserent1(String houserent1) {
        this.houserent1 = houserent1;
    }

    public String getUtilitybills1() {
        return utilitybills1;
    }

    public void setUtilitybills1(String utilitybills1) {
        this.utilitybills1 = utilitybills1;
    }

    public String getChildeducation1() {
        return childeducation1;
    }

    public void setChildeducation1(String childeducation1) {
        this.childeducation1 = childeducation1;
    }

    public String getMedical1() {
        return medical1;
    }

    public void setMedical1(String medical1) {
        this.medical1 = medical1;
    }

    public String getCommittee1() {
        return committee1;
    }

    public void setCommittee1(String committee1) {
        this.committee1 = committee1;
    }

    public String getLoaninstallment1() {
        return loaninstallment1;
    }

    public void setLoaninstallment1(String loaninstallment1) {
        this.loaninstallment1 = loaninstallment1;
    }

    public String getOther3() {
        return other3;
    }

    public void setOther3(String other3) {
        this.other3 = other3;
    }

    public String getUserProfession() {
        return userProfession;
    }

    public void setUserProfession(String userProfession) {
        this.userProfession = userProfession;
    }
}
