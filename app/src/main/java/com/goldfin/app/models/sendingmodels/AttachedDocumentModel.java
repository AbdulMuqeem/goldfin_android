package com.goldfin.app.models.sendingmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.goldfin.app.managers.retrofit.GsonFactory;

public class AttachedDocumentModel {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("cnicFront")
    @Expose
    private String cnicFront;
    @SerializedName("cnicBack")
    @Expose
    private String cnicBack;
    @SerializedName("residenceUtilityBills")
    @Expose
    private String residenceUtilityBills;
    @SerializedName("currentSalarySlip")
    @Expose
    private String currentSalarySlip;
    @SerializedName("jobCertificate")
    @Expose
    private String jobCertificate;


    @SerializedName("residenceBusinessBills")
    @Expose
    private String residenceBusinessBills;
    @SerializedName("proofOfBusiness")
    @Expose
    private String proofOfBusiness;
    @SerializedName("proofOfBusinessIncome")
    @Expose
    private String proofOfBusinessIncome;
    @SerializedName("SECP")
    @Expose
    private String sECP;
    @SerializedName("NTN")
    @Expose
    private String nTN;    @SerializedName("totalexpense")
    @Expose
    private String totalexpense;


    public String getCnicFront() {
        return cnicFront;
    }

    public void setCnicFront(String cnicFront) {
        this.cnicFront = cnicFront;
    }

    public String getCnicBack() {
        return cnicBack;
    }

    public void setCnicBack(String cnicBack) {
        this.cnicBack = cnicBack;
    }

    public String getResidenceUtilityBills() {
        return residenceUtilityBills;
    }

    public void setResidenceUtilityBills(String residenceUtilityBills) {
        this.residenceUtilityBills = residenceUtilityBills;
    }

    public String getCurrentSalarySlip() {
        return currentSalarySlip;
    }

    public void setCurrentSalarySlip(String currentSalarySlip) {
        this.currentSalarySlip = currentSalarySlip;
    }

    public String getJobCertificate() {
        return jobCertificate;
    }

    public void setJobCertificate(String jobCertificate) {
        this.jobCertificate = jobCertificate;
    }

    public String getResidenceBusinessBills() {
        return residenceBusinessBills;
    }

    public void setResidenceBusinessBills(String residenceBusinessBills) {
        this.residenceBusinessBills = residenceBusinessBills;
    }

    public String getProofOfBusiness() {
        return proofOfBusiness;
    }

    public void setProofOfBusiness(String proofOfBusiness) {
        this.proofOfBusiness = proofOfBusiness;
    }

    public String getProofOfBusinessIncome() {
        return proofOfBusinessIncome;
    }

    public void setProofOfBusinessIncome(String proofOfBusinessIncome) {
        this.proofOfBusinessIncome = proofOfBusinessIncome;
    }

    public String getsECP() {
        return sECP;
    }

    public void setsECP(String sECP) {
        this.sECP = sECP;
    }

    public String getnTN() {
        return nTN;
    }

    public void setnTN(String nTN) {
        this.nTN = nTN;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotalexpense() {
        return totalexpense;
    }

    public void setTotalexpense(String totalexpense) {
        this.totalexpense = totalexpense;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }

}
