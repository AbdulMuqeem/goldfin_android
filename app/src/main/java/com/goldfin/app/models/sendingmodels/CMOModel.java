package com.goldfin.app.models.sendingmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.goldfin.app.managers.retrofit.GsonFactory;

import java.util.ArrayList;
import java.util.List;

public class CMOModel {

    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("CMOId")
    @Expose
    private String cMOId;
    @SerializedName("formId")
    @Expose
    private String formId;
    @SerializedName("shroffName")
    @Expose
    private String shroffName;
    @SerializedName("shroffCertificateNumber")
    @Expose
    private String shroffCertificateNumber;
    @SerializedName("natureOfGold")
    @Expose
    private String natureOfGold;
    @SerializedName("grossWeight")
    @Expose
    private String grossWeight;
    @SerializedName("netWeight")
    @Expose
    private String netWeight;
    @SerializedName("marketValue")
    @Expose
    private String marketValue;
    @SerializedName("securedPacket1Number")
    @Expose
    private String securedPacket1Number;
    @SerializedName("grossWeightCMO")
    @Expose
    private String grossWeightCMO;
    @SerializedName("securedPacket2Number")
    @Expose
    private String securedPacket2Number;
    @SerializedName("picture1")
    @Expose
    private String picture1;
    @SerializedName("picture2")
    @Expose
    private String picture2;
    @SerializedName("barCodeNumber")
    @Expose
    private String barCodeNumber;
    @SerializedName("detailsGold")
    @Expose
    private ArrayList<String> detailsGold = null;


    @SerializedName("userCode")
    @Expose
    private String userCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;


    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("cnic")
    @Expose
    private String cnic;

    @SerializedName("address")
    @Expose
    private String address;

    public String getPicture1() {
        return picture1;
    }

    public void setPicture1(String picture1) {
        this.picture1 = picture1;
    }

    public String getPicture2() {
        return picture2;
    }

    public void setPicture2(String picture2) {
        this.picture2 = picture2;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCMOId() {
        return cMOId;
    }

    public void setCMOId(String cMOId) {
        this.cMOId = cMOId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getShroffName() {
        return shroffName;
    }

    public void setShroffName(String shroffName) {
        this.shroffName = shroffName;
    }

    public String getShroffCertificateNumber() {
        return shroffCertificateNumber;
    }

    public void setShroffCertificateNumber(String shroffCertificateNumber) {
        this.shroffCertificateNumber = shroffCertificateNumber;
    }

    public String getNatureOfGold() {
        return natureOfGold;
    }

    public void setNatureOfGold(String natureOfGold) {
        this.natureOfGold = natureOfGold;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }

    public String getSecuredPacket1Number() {
        return securedPacket1Number;
    }

    public void setSecuredPacket1Number(String securedPacket1Number) {
        this.securedPacket1Number = securedPacket1Number;
    }

    public String getGrossWeightCMO() {
        return grossWeightCMO;
    }

    public void setGrossWeightCMO(String grossWeightCMO) {
        this.grossWeightCMO = grossWeightCMO;
    }

    public String getSecuredPacket2Number() {
        return securedPacket2Number;
    }

    public void setSecuredPacket2Number(String securedPacket2Number) {
        this.securedPacket2Number = securedPacket2Number;
    }

    public String getBarCodeNumber() {
        return barCodeNumber;
    }

    public void setBarCodeNumber(String barCodeNumber) {
        this.barCodeNumber = barCodeNumber;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getDetailsGold() {
        return detailsGold;
    }

    public void setDetailsGold(ArrayList<String> detailsGold) {
        this.detailsGold = detailsGold;
    }

    @SerializedName("personalInformation")
    @Expose
    private String personalInformation;
    @SerializedName("VOId")
    @Expose
    private String VOId;
    @SerializedName("professionalInformation")
    @Expose
    private String professionalInformation;
    @SerializedName("financialInformation")
    @Expose
    private String financialInformation;
    @SerializedName("neighborcheck_residence")
    @Expose
    private String neighborcheckResidence;
    @SerializedName("neighborcheck_business")
    @Expose
    private String neighborcheckBusiness;
    @SerializedName("negative_reason")
    @Expose
    private String negativeReason;
    @SerializedName("remarks_personalInformation")
    @Expose
    private String remarksPersonalInformation;
    @SerializedName("remarks_professionalInformation")
    @Expose
    private String remarksProfessionalInformation;
    @SerializedName("remarks_financialInformation")
    @Expose
    private String remarksFinancialInformation;
    @SerializedName("verificationResults")
    @Expose
    private String verificationResults;

    public String getcMOId() {
        return cMOId;
    }

    public void setcMOId(String cMOId) {
        this.cMOId = cMOId;
    }

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public String getProfessionalInformation() {
        return professionalInformation;
    }

    public void setProfessionalInformation(String professionalInformation) {
        this.professionalInformation = professionalInformation;
    }

    public String getFinancialInformation() {
        return financialInformation;
    }

    public void setFinancialInformation(String financialInformation) {
        this.financialInformation = financialInformation;
    }

    public String getNeighborcheckResidence() {
        return neighborcheckResidence;
    }

    public void setNeighborcheckResidence(String neighborcheckResidence) {
        this.neighborcheckResidence = neighborcheckResidence;
    }

    public String getNeighborcheckBusiness() {
        return neighborcheckBusiness;
    }

    public void setNeighborcheckBusiness(String neighborcheckBusiness) {
        this.neighborcheckBusiness = neighborcheckBusiness;
    }

    public String getNegativeReason() {
        return negativeReason;
    }

    public void setNegativeReason(String negativeReason) {
        this.negativeReason = negativeReason;
    }

    public String getRemarksPersonalInformation() {
        return remarksPersonalInformation;
    }

    public void setRemarksPersonalInformation(String remarksPersonalInformation) {
        this.remarksPersonalInformation = remarksPersonalInformation;
    }

    public String getRemarksProfessionalInformation() {
        return remarksProfessionalInformation;
    }

    public void setRemarksProfessionalInformation(String remarksProfessionalInformation) {
        this.remarksProfessionalInformation = remarksProfessionalInformation;
    }

    public String getRemarksFinancialInformation() {
        return remarksFinancialInformation;
    }

    public void setRemarksFinancialInformation(String remarksFinancialInformation) {
        this.remarksFinancialInformation = remarksFinancialInformation;
    }

    public String getVerificationResults() {
        return verificationResults;
    }

    public void setVerificationResults(String verificationResults) {
        this.verificationResults = verificationResults;
    }

    public String getVOId() {
        return VOId;
    }

    public void setVOId(String VOId) {
        this.VOId = VOId;
    }

    @SerializedName("loanAccountNumber")
    @Expose
    private String loanAccountNumber;
    @SerializedName("mobileAccountNumber")
    @Expose
    private String mobileAccountNumber;
    @SerializedName("loanAmount")
    @Expose
    private String loanAmount;
    @SerializedName("markupRate")
    @Expose
    private String markupRate;
    @SerializedName("totalMarkup")
    @Expose
    private String totalMarkup;
    @SerializedName("processingFee")
    @Expose
    private String processingFee;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("goldRate")
    @Expose
    private String goldRate;
    @SerializedName("collateralValue")
    @Expose
    private String collateralValue;
    @SerializedName("currentCoverage")
    @Expose
    private String currentCoverage;
    @SerializedName("loanDisbursementDate")
    @Expose
    private String loanDisbursementDate;
    @SerializedName("loanMaturityDate")
    @Expose
    private String loanMaturityDate;

    public String getLoanAccountNumber() {
        return loanAccountNumber;
    }

    public void setLoanAccountNumber(String loanAccountNumber) {
        this.loanAccountNumber = loanAccountNumber;
    }

    public String getMobileAccountNumber() {
        return mobileAccountNumber;
    }

    public void setMobileAccountNumber(String mobileAccountNumber) {
        this.mobileAccountNumber = mobileAccountNumber;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getMarkupRate() {
        return markupRate;
    }

    public void setMarkupRate(String markupRate) {
        this.markupRate = markupRate;
    }

    public String getTotalMarkup() {
        return totalMarkup;
    }

    public void setTotalMarkup(String totalMarkup) {
        this.totalMarkup = totalMarkup;
    }

    public String getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(String processingFee) {
        this.processingFee = processingFee;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getGoldRate() {
        return goldRate;
    }

    public void setGoldRate(String goldRate) {
        this.goldRate = goldRate;
    }

    public String getCollateralValue() {
        return collateralValue;
    }

    public void setCollateralValue(String collateralValue) {
        this.collateralValue = collateralValue;
    }

    public String getCurrentCoverage() {
        return currentCoverage;
    }

    public void setCurrentCoverage(String currentCoverage) {
        this.currentCoverage = currentCoverage;
    }

    public String getLoanDisbursementDate() {
        return loanDisbursementDate;
    }

    public void setLoanDisbursementDate(String loanDisbursementDate) {
        this.loanDisbursementDate = loanDisbursementDate;
    }

    public String getLoanMaturityDate() {
        return loanMaturityDate;
    }

    public void setLoanMaturityDate(String loanMaturityDate) {
        this.loanMaturityDate = loanMaturityDate;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }


}