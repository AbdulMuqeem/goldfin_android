package com.goldfin.app.models.sendingmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.goldfin.app.managers.retrofit.GsonFactory;

public class FormSendingDataModel {

    @SerializedName("addressapp")
    @Expose
    private String addressapp;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("years")
    @Expose
    private String years;
    @SerializedName("landline")
    @Expose
    private String landline;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("desiredLoanAmount")
    @Expose
    private String loanAmountDesired;
    @SerializedName("othermobilenum")
    @Expose
    private String othermobilenum;
    @SerializedName("residentialstatus")
    @Expose
    private String residentialstatus;
    @SerializedName("permanentaddress")
    @Expose
    private String permanentaddress;
    @SerializedName("currentaddress")
    @Expose
    private String currentaddress;
    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("residingsince")
    @Expose
    private String residingsince;
    @SerializedName("maritalstatus")
    @Expose
    private String maritalstatus;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("gender")
    @Expose
    private String gender;

    //
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("empname")
    @Expose
    private String empname;
    @SerializedName("empaddress")
    @Expose
    private String empaddress;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("jobstatus")
    @Expose
    private String jobstatus;
    @SerializedName("dateofjoining")
    @Expose
    private String dateofjoining;

    @SerializedName("totalexpense")
    @Expose
    private String totalexpense;

    @SerializedName("totalexp")
    @Expose
    private String totalExperince;

    @SerializedName("grosssalary")
    @Expose
    private String grosssalary;
    @SerializedName("netsalary")
    @Expose
    private String netsalary;
    @SerializedName("othersource")
    @Expose
    private String othersource;
    @SerializedName("natureofincome")
    @Expose
    private String natureofincome;
    @SerializedName("outstandingloan")
    @Expose
    private String outstandingloan;
    @SerializedName("outstandingloanamount")
    @Expose
    private String outstandingloanamount;
    @SerializedName("sourceofloan")
    @Expose
    private String sourceofloan;
    @SerializedName("loanamount")
    @Expose
    private String loanamount;
    @SerializedName("household")
    @Expose
    private String household;
    @SerializedName("utilitybill")
    @Expose
    private String utilitybill;
    @SerializedName("childeducation")
    @Expose
    private String childeducation;
    @SerializedName("medical")
    @Expose
    private String medical;
    @SerializedName("committe")
    @Expose
    private String committe;
    @SerializedName("loaninstallment")
    @Expose
    private String loaninstallment;
    @SerializedName("other0")
    @Expose
    private String other0;


    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getLoanAmountDesired() {
        return loanAmountDesired;
    }

    public void setLoanAmountDesired(String loanAmountDesired) {
        this.loanAmountDesired = loanAmountDesired;
    }

    public String getOthermobilenum() {
        return othermobilenum;
    }

    public void setOthermobilenum(String othermobilenum) {
        this.othermobilenum = othermobilenum;
    }

    public String getResidentialstatus() {
        return residentialstatus;
    }

    public void setResidentialstatus(String residentialstatus) {
        this.residentialstatus = residentialstatus;
    }

    public String getPermanentaddress() {
        return permanentaddress;
    }

    public void setPermanentaddress(String permanentaddress) {
        this.permanentaddress = permanentaddress;
    }

    public String getCurrentaddress() {
        return currentaddress;
    }

    public void setCurrentaddress(String currentaddress) {
        this.currentaddress = currentaddress;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getResidingsince() {
        return residingsince;
    }

    public void setResidingsince(String residingsince) {
        this.residingsince = residingsince;
    }

    public String getMaritalstatus() {
        return maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        this.maritalstatus = maritalstatus;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getEmpaddress() {
        return empaddress;
    }

    public void setEmpaddress(String empaddress) {
        this.empaddress = empaddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getJobstatus() {
        return jobstatus;
    }

    public void setJobstatus(String jobstatus) {
        this.jobstatus = jobstatus;
    }

    public String getDateofjoining() {
        return dateofjoining;
    }

    public void setDateofjoining(String dateofjoining) {
        this.dateofjoining = dateofjoining;
    }

    public String getTotalexp() {
        return totalexpense;
    }

    public void setTotalexp(String totalexpense) {
        this.totalexpense = totalexpense;
    }

    public String getGrosssalary() {
        return grosssalary;
    }

    public void setGrosssalary(String grosssalary) {
        this.grosssalary = grosssalary;
    }

    public String getNetsalary() {
        return netsalary;
    }

    public void setNetsalary(String netsalary) {
        this.netsalary = netsalary;
    }

    public String getOthersource() {
        return othersource;
    }

    public void setOthersource(String othersource) {
        this.othersource = othersource;
    }

    public String getNatureofincome() {
        return natureofincome;
    }

    public void setNatureofincome(String natureofincome) {
        this.natureofincome = natureofincome;
    }

    public String getOutstandingloan() {
        return outstandingloan;
    }

    public void setOutstandingloan(String outstandingloan) {
        this.outstandingloan = outstandingloan;
    }

    public String getSourceofloan() {
        return sourceofloan;
    }

    public void setSourceofloan(String sourceofloan) {
        this.sourceofloan = sourceofloan;
    }

    public String getLoanamount() {
        return loanamount;
    }

    public void setLoanamount(String loanamount) {
        this.loanamount = loanamount;
    }

    public String getHousehold() {
        return household;
    }

    public void setHousehold(String household) {
        this.household = household;
    }

    public String getUtilitybill() {
        return utilitybill;
    }

    public void setUtilitybill(String utilitybill) {
        this.utilitybill = utilitybill;
    }

    public String getChildeducation() {
        return childeducation;
    }

    public void setChildeducation(String childeducation) {
        this.childeducation = childeducation;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getCommitte() {
        return committe;
    }

    public void setCommitte(String committe) {
        this.committe = committe;
    }

    public String getLoaninstallment() {
        return loaninstallment;
    }

    public void setLoaninstallment(String loaninstallment) {
        this.loaninstallment = loaninstallment;
    }

    public String getOther0() {
        return other0;
    }

    public void setOther0(String other0) {
        this.other0 = other0;
    }

    public String getAddressapp() {
        return addressapp;
    }

    public void setAddressapp(String addressapp) {
        this.addressapp = addressapp;
    }

    /*Business*/
    @SerializedName("bussinessname")
    @Expose
    private String bussinessname;
    @SerializedName("businessStatus")
    @Expose
    private String businessStatus;
    @SerializedName("bussinessaddress")
    @Expose
    private String bussinessaddress;
    @SerializedName("bussinesslandline")
    @Expose
    private String bussinesslandline;
    @SerializedName("bussinesswebsite")
    @Expose
    private String bussinesswebsite;
    @SerializedName("bussinessOwnstatus")
    @Expose
    private String bussinessOwnstatus;
    @SerializedName("natureofbusiness")
    @Expose
    private String natureofbusiness;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("secp")
    @Expose
    private String secp;
    @SerializedName("dateofreg")
    @Expose
    private String dateofreg;
    @SerializedName("ntn")
    @Expose
    private String ntn;
    @SerializedName("nbrofemp")
    @Expose
    private String nbrofemp;
    @SerializedName("working")
    @Expose
    private String working;
    @SerializedName("ownland")
    @Expose
    private String ownland;
    @SerializedName("rentedland")
    @Expose
    private String rentedland;
    @SerializedName("cultivating")
    @Expose
    private String cultivating;
    @SerializedName("maincrops")
    @Expose
    private String maincrops;
    @SerializedName("animals")
    @Expose
    private String animals;

    @SerializedName("averagemonthbusinessexp")
    @Expose
    private String averagemonthbusinessexp;
    @SerializedName("avgmonthturnover")
    @Expose
    private String avgmonthturnover;
    @SerializedName("monthlyincome")
    @Expose
    private String monthlyincome;
    @SerializedName("otherincome")
    @Expose
    private String otherincome;
    @SerializedName("monthlyaccounts")
    @Expose
    private String monthlyaccounts;
    @SerializedName("outstandingloan1")
    @Expose
    private String outstandingloan1;
    @SerializedName("sourceofloan1")
    @Expose
    private String sourceofloan1;
    @SerializedName("outstandingloan2")
    @Expose
    private String outstandingloan2;
    @SerializedName("sourceofloan2")
    @Expose
    private String sourceofloan2;
    @SerializedName("loanamount2")
    @Expose
    private String loanamount2;
    @SerializedName("household1")
    @Expose
    private String household1;
    @SerializedName("houserent1")
    @Expose
    private String houserent1;
    @SerializedName("houserent")
    @Expose
    private String houserent;
    @SerializedName("utilitybills1")
    @Expose
    private String utilitybills1;
    @SerializedName("childeducation1")
    @Expose
    private String childeducation1;
    @SerializedName("medical1")
    @Expose
    private String medical1;
    @SerializedName("committee1")
    @Expose
    private String committee1;
    @SerializedName("loaninstallment1")
    @Expose
    private String loaninstallment1;
    @SerializedName("other3")
    @Expose
    private String other3;

    @SerializedName("establishsince")
    @Expose
    private String establishsince;

    public String getBussinessname() {
        return bussinessname;
    }

    public void setBussinessname(String bussinessname) {
        this.bussinessname = bussinessname;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getBussinessaddress() {
        return bussinessaddress;
    }

    public void setBussinessaddress(String bussinessaddress) {
        this.bussinessaddress = bussinessaddress;
    }

    public String getBussinesslandline() {
        return bussinesslandline;
    }

    public void setBussinesslandline(String bussinesslandline) {
        this.bussinesslandline = bussinesslandline;
    }

    public String getBussinesswebsite() {
        return bussinesswebsite;
    }

    public void setBussinesswebsite(String bussinesswebsite) {
        this.bussinesswebsite = bussinesswebsite;
    }

    public String getBussinessOwnstatus() {
        return bussinessOwnstatus;
    }

    public void setBussinessOwnstatus(String bussinessOwnstatus) {
        this.bussinessOwnstatus = bussinessOwnstatus;
    }

    public String getNatureofbusiness() {
        return natureofbusiness;
    }

    public void setNatureofbusiness(String natureofbusiness) {
        this.natureofbusiness = natureofbusiness;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getSecp() {
        return secp;
    }

    public void setSecp(String secp) {
        this.secp = secp;
    }

    public String getDateofreg() {
        return dateofreg;
    }

    public void setDateofreg(String dateofreg) {
        this.dateofreg = dateofreg;
    }

    public String getNtn() {
        return ntn;
    }

    public void setNtn(String ntn) {
        this.ntn = ntn;
    }

    public String getNbrofemp() {
        return nbrofemp;
    }

    public void setNbrofemp(String nbrofemp) {
        this.nbrofemp = nbrofemp;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public String getOwnland() {
        return ownland;
    }

    public void setOwnland(String ownland) {
        this.ownland = ownland;
    }

    public String getRentedland() {
        return rentedland;
    }

    public void setRentedland(String rentedland) {
        this.rentedland = rentedland;
    }

    public String getCultivating() {
        return cultivating;
    }

    public void setCultivating(String cultivating) {
        this.cultivating = cultivating;
    }

    public String getMaincrops() {
        return maincrops;
    }

    public void setMaincrops(String maincrops) {
        this.maincrops = maincrops;
    }

    public String getAnimals() {
        return animals;
    }

    public void setAnimals(String animals) {
        this.animals = animals;
    }

    public String getAveragemonthbusinessexp() {
        return averagemonthbusinessexp;
    }

    public void setAveragemonthbusinessexp(String averagemonthbusinessexp) {
        this.averagemonthbusinessexp = averagemonthbusinessexp;
    }

    public String getAvgmonthturnover() {
        return avgmonthturnover;
    }

    public void setAvgmonthturnover(String avgmonthturnover) {
        this.avgmonthturnover = avgmonthturnover;
    }

    public String getMonthlyincome() {
        return monthlyincome;
    }

    public void setMonthlyincome(String monthlyincome) {
        this.monthlyincome = monthlyincome;
    }

    public String getOtherincome() {
        return otherincome;
    }

    public void setOtherincome(String otherincome) {
        this.otherincome = otherincome;
    }

    public String getMonthlyaccounts() {
        return monthlyaccounts;
    }

    public void setMonthlyaccounts(String monthlyaccounts) {
        this.monthlyaccounts = monthlyaccounts;
    }

    public String getOutstandingloan1() {
        return outstandingloan1;
    }

    public void setOutstandingloan1(String outstandingloan1) {
        this.outstandingloan1 = outstandingloan1;
    }

    public String getSourceofloan1() {
        return sourceofloan1;
    }

    public void setSourceofloan1(String sourceofloan1) {
        this.sourceofloan1 = sourceofloan1;
    }

    public String getOutstandingloan2() {
        return outstandingloan2;
    }

    public void setOutstandingloan2(String outstandingloan2) {
        this.outstandingloan2 = outstandingloan2;
    }

    public String getSourceofloan2() {
        return sourceofloan2;
    }

    public void setSourceofloan2(String sourceofloan2) {
        this.sourceofloan2 = sourceofloan2;
    }

    public String getLoanamount2() {
        return loanamount2;
    }

    public void setLoanamount2(String loanamount2) {
        this.loanamount2 = loanamount2;
    }

    public String getHousehold1() {
        return household1;
    }

    public void setHousehold1(String household1) {
        this.household1 = household1;
    }

    public String getHouserent1() {
        return houserent1;
    }

    public void setHouserent1(String houserent1) {
        this.houserent1 = houserent1;
    }

    public String getUtilitybills1() {
        return utilitybills1;
    }

    public void setUtilitybills1(String utilitybills1) {
        this.utilitybills1 = utilitybills1;
    }

    public String getChildeducation1() {
        return childeducation1;
    }

    public void setChildeducation1(String childeducation1) {
        this.childeducation1 = childeducation1;
    }

    public String getMedical1() {
        return medical1;
    }

    public void setMedical1(String medical1) {
        this.medical1 = medical1;
    }

    public String getCommittee1() {
        return committee1;
    }

    public void setCommittee1(String committee1) {
        this.committee1 = committee1;
    }

    public String getLoaninstallment1() {
        return loaninstallment1;
    }

    public void setLoaninstallment1(String loaninstallment1) {
        this.loaninstallment1 = loaninstallment1;
    }

    public String getOther3() {
        return other3;
    }

    public void setOther3(String other3) {
        this.other3 = other3;
    }

    public String getTotalExperince() {
        return totalExperince;
    }

    public void setTotalExperince(String totalExperince) {
        this.totalExperince = totalExperince;
    }

//    public String getTotalexpSal() {
//        return totalexpenseSal;
//    }

    public String getHouserent() {
        return houserent;
    }

    public void setHouserent(String houserent) {
        this.houserent = houserent;
    }

    //    public void setTotalexpSal(String totalexpSal) {
//        this.totalexpenseSal = totalexpSal;
//    }
    public String getEstablishsince() {
        return establishsince;
    }

    public void setEstablishsince(String establishsince) {
        this.establishsince = establishsince;
    }

    public String getOutstandingloanamount() {
        return outstandingloanamount;
    }

    public void setOutstandingloanamount(String outstandingloanamount) {
        this.outstandingloanamount = outstandingloanamount;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }

}
