package com.goldfin.app.models.sendingmodels;


import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.goldfin.app.managers.retrofit.GsonFactory;

public class ValidationFormModel implements Serializable {

    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("CMOId")
    @Expose
    private String cMOId;
    @SerializedName("formId")
    @Expose
    private String formId;
    @SerializedName("personalInformation")
    @Expose
    private String personalInformation;
    @SerializedName("professionalInformation")
    @Expose
    private String professionalInformation;
    @SerializedName("financialInformation")
    @Expose
    private String financialInformation;
    @SerializedName("neighborcheck_residence")
    @Expose
    private String neighborcheckResidence;
    @SerializedName("neighborcheck_business")
    @Expose
    private String neighborcheckBusiness;
    @SerializedName("negative_reason")
    @Expose
    private String negativeReason;
    @SerializedName("remarks_personalInformation")
    @Expose
    private String remarksPersonalInformation;
    @SerializedName("remarks_professionalInformation")
    @Expose
    private String remarksProfessionalInformation;
    @SerializedName("remarks_financialInformation")
    @Expose
    private String remarksFinancialInformation;
    private final static long serialVersionUID = 5180916876293350116L;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCMOId() {
        return cMOId;
    }

    public void setCMOId(String cMOId) {
        this.cMOId = cMOId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public String getProfessionalInformation() {
        return professionalInformation;
    }

    public void setProfessionalInformation(String professionalInformation) {
        this.professionalInformation = professionalInformation;
    }

    public String getFinancialInformation() {
        return financialInformation;
    }

    public void setFinancialInformation(String financialInformation) {
        this.financialInformation = financialInformation;
    }

    public String getNeighborcheckResidence() {
        return neighborcheckResidence;
    }

    public void setNeighborcheckResidence(String neighborcheckResidence) {
        this.neighborcheckResidence = neighborcheckResidence;
    }

    public String getNeighborcheckBusiness() {
        return neighborcheckBusiness;
    }

    public void setNeighborcheckBusiness(String neighborcheckBusiness) {
        this.neighborcheckBusiness = neighborcheckBusiness;
    }

    public String getNegativeReason() {
        return negativeReason;
    }

    public void setNegativeReason(String negativeReason) {
        this.negativeReason = negativeReason;
    }

    public String getRemarksPersonalInformation() {
        return remarksPersonalInformation;
    }

    public void setRemarksPersonalInformation(String remarksPersonalInformation) {
        this.remarksPersonalInformation = remarksPersonalInformation;
    }

    public String getRemarksProfessionalInformation() {
        return remarksProfessionalInformation;
    }

    public void setRemarksProfessionalInformation(String remarksProfessionalInformation) {
        this.remarksProfessionalInformation = remarksProfessionalInformation;
    }

    public String getRemarksFinancialInformation() {
        return remarksFinancialInformation;
    }

    public void setRemarksFinancialInformation(String remarksFinancialInformation) {
        this.remarksFinancialInformation = remarksFinancialInformation;
    }

    @Override
    public String toString() {
        return GsonFactory.getConfiguredGson().toJson(this);
    }
}