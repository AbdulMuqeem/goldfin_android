package com.goldfin.app.models.wrappers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.goldfin.app.models.extramodels.AddressModel;

/**
 * Created by  on 11-Mar-17.
 */

public class AddressWrapper {
    @SerializedName("Address")
    public ArrayList<AddressModel> addresses;

}
