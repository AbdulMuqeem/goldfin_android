package com.goldfin.app.models.wrappers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.goldfin.app.models.NotificationModel;

/**
 * Created by  on 11-Mar-17.
 */

public class NotificationWrapper {
    @SerializedName("Notifications")
    public ArrayList<NotificationModel> notifications;

}
