package com.goldfin.app.widget.recyclerviewenhanced;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}